<?php
namespace PFBC\Element;

class Posttype extends Select {
	public function __construct($label, $name, array $properties = null) {
		$options = array();
		global $wpdb;
		$posttypes = web_apper_posttype_get_results( "SELECT posttype_slug, posttype_name FROM {$wpdb->prefix}web_apper_posttypes" );
		foreach ( $posttypes as $posttype ) : // Foreach role
			$options[$posttype['posttype_slug']] = $posttype['posttype_name'] . ' (' . $posttype['posttype_slug'] . ')';
		endforeach;
		parent::__construct($label, $name, $options, $properties);
    }
}
