<?php
namespace PFBC\Element;

class Equation extends Select {
	public function __construct($label, $name, array $properties = null) {
		$options = array();
		global $wpdb;
		$equations = web_apper_equation_get_results( "SELECT equation_id, equation_name FROM {$wpdb->prefix}web_apper_equations WHERE core_item = 0;" );
		foreach ( $equations as $equation ) : // Foreach role
			$options[$equation['equation_id']] = $equation['equation_name'] . ' (' . $equation['equation_id'] . ')';
		endforeach;
		parent::__construct($label, $name, $options, $properties);
    }
}
