<?php
namespace PFBC\Element;

class Flow extends Select {
	public function __construct($label, $name, array $properties = null) {
		$options = array();
		global $wpdb;
		$flows = web_apper_flow_get_results( "SELECT flow_id, flow_name FROM {$wpdb->prefix}web_apper_flows WHERE core_item = 0;" );
		foreach ( $flows as $flow ) : // Foreach role
			$options[$flow['flow_id']] = $flow['flow_name'] . ' (' . $flow['flow_id'] . ')';
		endforeach;
		parent::__construct($label, $name, $options, $properties);
    }
}
