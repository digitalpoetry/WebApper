<?php
namespace PFBC\Element;

class Field extends Select {
	public function __construct($label, $name, array $properties = null) {
		$options = array();
		global $wpdb;
		$fields = web_apper_field_get_results( "SELECT field_id, field_name FROM {$wpdb->prefix}web_apper_fields WHERE core_item = 0;" );
		foreach ( $fields as $field ) : // Foreach role
			$options[$field['field_id']] = $field['field_name'] . ' (' . $field['field_id'] . ')';
		endforeach;
		parent::__construct($label, $name, $options, $properties);
    }
}
