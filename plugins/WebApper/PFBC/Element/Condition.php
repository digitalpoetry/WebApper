<?php
namespace PFBC\Element;

class Condition extends Select {
	public function __construct($label, $name, array $properties = null) {
		$options = array();
		global $wpdb;
		$conditions = web_apper_condition_get_results( "SELECT condition_id, condition_name FROM {$wpdb->prefix}web_apper_conditions WHERE core_item = 0;" );
		foreach ( $conditions as $condition ) : // Foreach role
			$options[$condition['condition_id']] = $condition['condition_name'] . ' (' . $condition['condition_id'] . ')';
		endforeach;
		parent::__construct($label, $name, $options, $properties);
    }
}
