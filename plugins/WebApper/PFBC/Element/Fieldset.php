<?php
namespace PFBC\Element;

class Fieldset extends Select {
	public function __construct($label, $name, array $properties = null) {
		$options = array();
		global $wpdb;
		$fieldsets = web_apper_fieldset_get_results( "SELECT fieldset_id, fieldset_name FROM {$wpdb->prefix}web_apper_fieldsets WHERE core_item = 0;" );
		foreach ( $fieldsets as $fieldset ) : // Foreach role
			$options[$fieldset['fieldset_id']] = $fieldset['fieldset_name'] . ' (' . $fieldset['fieldset_id'] . ')';
		endforeach;
		parent::__construct($label, $name, $options, $properties);
    }
}
