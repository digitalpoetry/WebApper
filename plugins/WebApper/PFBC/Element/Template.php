<?php
namespace PFBC\Element;
require_once dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) . "/PostJuice/Template/core.php";

class Template extends Select {
	public function __construct($label, $name, array $properties = null) {
		$options = array();
		$value = $properties['value'];
		global $wpdb;
		$templates = \web_apper_template_get_results( "SELECT ID, template_name FROM {$wpdb->prefix}web_apper_templates" );
		if ( !empty($templates) ) :
			foreach ( $templates as $template ) : // Foreach role
				$options[$template['ID']] = $template['template_name'];
			endforeach;
		endif;
		parent::__construct($label, $name, $options, $properties);
    }
}