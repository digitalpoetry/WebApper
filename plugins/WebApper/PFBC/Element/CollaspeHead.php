<?php
namespace PFBC\Element;

class CollaspeHead extends \PFBC\Element {

	public function __construct($value, $name = "", array $properties = null) {
		if(!is_array($properties))
			$properties = array();

		if(!empty($value))
			$properties["value"] = $value;

		parent::__construct("", $name, $properties);
	}

	public function render() {
		echo '
			<div class="accordion" id="' . $this->_attributes["id"] . '">
			<div class="accordion-group">
				<div class="accordion-heading">
					<input type="submit" value="Save" name="" id="submit-' . $this->_attributes["name"] . '" class="hide btn btn-primary" data-loading-text="Submitting...">
					<a id="' . $this->_attributes["name"] . '" class="accordion-toggle" data-toggle="collapse" data-parent="#' . $this->_attributes["name"] . '" href="#collapse' . $this->_attributes["name"] . '">
						' . $this->_attributes["value"] . '
					</a>
				</div>
				<div id="collapse' . $this->_attributes["name"] . '" class="accordion-body collapse">
					<div class="accordion-inner">
		';
    }
}
