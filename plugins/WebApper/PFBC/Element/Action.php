<?php
namespace PFBC\Element;

class Action extends Select {
	public function __construct($label, $name, array $properties = null) {
		$this->_attributes["class"] = "form-control " . $this->_attributes["class"];
		$options = array();
		$options = array();
		global $wpdb;
		$actions = web_apper_action_get_results( "SELECT action_id, action_name FROM {$wpdb->prefix}web_apper_actions WHERE core_item = 0;" );
		foreach ( $actions as $action ) : // Foreach role
			$options[$action['action_id']] = $action['action_name'] . ' (' . $action['action_id'] . ')';
		endforeach;
		parent::__construct($label, $name, $options, $properties);
    }
}
