<?php
namespace PFBC\Element;

class UploadImage extends \PFBC\Element {

	public function __construct($label, $name, array $properties = null) {
        parent::__construct($label, $name, $properties);
	}

    public function jQueryDocumentReady() {
        echo '';
    }

    public function render() {
		echo '
			<div id="attachContainer">
				<a id="pickfiles" href="javascript:;" class="btn">Browse for images</a>
			</div>
			<div id="console">Your browser doesn\'t have HTML5 support.</div>
			<ul id="' . $this->_attributes["name"] . 'Wrapper" class="filelist thumbnails">
		';
		if ( !empty($this->_attributes["value"]) ) :
			foreach( explode(',', $this->_attributes["value"]) as $attachment_id ) :
				$attachment = web_apper_get_attachment($attachment_id);
				echo '<li class="span2">
						<a href="javascript:void(0)" data-attachment-id="' . $attachment->ID . '" class="delete"><i class="icon-remove"></i></a>
						<img src="' . site_url('wp-content/uploads/WebApper/plupload') . '/' . $attachment->attachment_file_name . '" title="' . $attachment->attachment_file_name . '" alt="' . $attachment->attachment_file_name . '" class="img-rounded" />
					</li>
				';
			endforeach;
		endif;
		echo '
			</ul>
			<input type="hidden" id="' . $this->_attributes["name"] . '_attachment_ids" name="' . $this->_attributes["name"] . '" value="' . $this->_attributes["value"] . '">
			
			<script type="text/javascript">
				// Custom example logic
				var uploader = new plupload.Uploader({
					runtimes : "html5,html4",
					browse_button : "pickfiles", // you can pass in id...
					container: "attachContainer", // ... or DOM Element itself
					urlstream_upload: true,
					url: "' . site_url("/wp-content/plugins/WebApper/Attachment/ajax_handler.php") . '",
					filters : {
						max_file_size : "100mb",
						mime_types: [
							{title : "Image files", extensions : "jpg,gif,png"},
						]
					},
					// Flash settings
					flash_swf_url : "' . site_url("/wp-content/plugins/WebApper/assets/js/plupload/Moxie.swf") . '",
					// Silverlight settings
					silverlight_xap_url : "' . site_url("/wp-content/plugins/WebApper/assets/js/plupload/Moxie.xap") . '",
					init: {
						PostInit: function() {
							document.getElementById("console").innerHTML = "";
						},
						FilesAdded: function(up, files) {
							plupload.each(files, function(file) {
								document.getElementById("' . $this->_attributes["name"] . 'Wrapper").innerHTML += \'<div id="\' + file.id + \'">\' + file.name + \' (\' + plupload.formatSize(file.size) + \') <b></b></div>\';
							});
							uploader.start();
						},
						UploadProgress: function(up, file) {
							document.getElementById(file.id).getElementsByTagName("b")[0].innerHTML = "<span>" + file.percent + "%</span>";
						},
						Error: function(up, err) {
							document.getElementById("console").innerHTML += "\nError #" + err.code + ": " + err.message;
						},
						FileUploaded: function(up, file, info) {
			                var result = jQuery.parseJSON(info.response);  // Parse response
							var current_val = jQuery("#' . $this->_attributes["name"] . '_attachment_ids").val();
			                if ( current_val == "" ) {
								jQuery("#' . $this->_attributes["name"] . '_attachment_ids").val( result.id );
							} else {
								jQuery("#' . $this->_attributes["name"] . '_attachment_ids").val( current_val + "," + result.id );
							}
							jQuery("#" + file.id).remove();
							document.getElementById("' . $this->_attributes["name"] . 'Wrapper").innerHTML += 
							\'<li class="span2">\' + 
								\'<a href="javascript:void(0)" data-attachment-id="\' + result.id + \'" class="delete"><i class="icon-remove"></i></a>\' + 
								\'<img src="\' + result.url + \'" title="\' + result.data.attachment_file_name + \'" alt="\' + result.data.attachment_file_name + \'" class="img-rounded" />\' + 
							\'</li>\';

							var form = jQuery("#' . $this->_attributes["name"] . 'Wrapper").parents("form");
							setTimeout( function() {
								var data = {
									action: jQuery(\'[name="action"]\', form).val(),
									web_apper_action: jQuery(\'[name="web_apper_action"]\', form).val(),
									web_apper_nonce: jQuery(\'[name="web_apper_nonce"]\', form).val(),
									web_apper_item_ids: jQuery(\'[name="web_apper_item_ids"]\', form).val(),
									web_apper_include: "' . $this->_attributes["name"] . '",
									' . $this->_attributes["name"] . ': jQuery("#' . $this->_attributes["name"] . '_attachment_ids").val(),
								}
								jQuery.post("' . admin_url("admin-ajax.php") . '", data, function(response) {
									console.log( response );
									var result = jQuery.parseJSON(response);  // Parse response
									if ( result.success ) {  // If ajax returns a successful save
										//
									}
								});
							}, 1500 );
			            },
					}
				});
				uploader.init();
				
				jQuery(document).ready(function($) {
					// "Delete" button click handler
					$("#' . $this->_attributes["name"] . 'Wrapper > li .delete").live("click", function() {
						var that = this;
						var attach_id = $(this).data("attachment-id");
						var data = {
							action: "web_apper_attachment",
							web_apper_action: "delete_item",
							web_apper_nonce: "' . wp_create_nonce("AwesomeSauce!87") . '",
							web_apper_item_ids: attach_id
						}
						$.post("' . admin_url("admin-ajax.php") . '", data, function(response) {
							var result = jQuery.parseJSON(response);  // Parse response
							if ( result.success ) {  // If ajax returns a successful save
								$(that).parent().remove();
								var current_val = jQuery("#' . $this->_attributes["name"] . '_attachment_ids").val();
								var current_val = current_val.split(",");
								current_val.splice( current_val.indexOf(attach_id.toString()), 1);
								jQuery("#' . $this->_attributes["name"] . '_attachment_ids").val( current_val.toString() );
							}
						});
						var form = $(that).parents("form");
						setTimeout( function() {
							var data = {
								action: $(\'[name="action"]\', form).val(),
								web_apper_action: $(\'[name="web_apper_action"]\', form).val(),
								web_apper_nonce: $(\'[name="web_apper_nonce"]\', form).val(),
								web_apper_item_ids: $(\'[name="web_apper_item_ids"]\', form).val(),
								web_apper_include: "' . $this->_attributes["name"] . '",
								' . $this->_attributes["name"] . ': $("#' . $this->_attributes["name"] . '_attachment_ids").val(),
							}
							$.post("' . admin_url("admin-ajax.php") . '", data, function(response) {
								console.log( response );
								var result = jQuery.parseJSON(response);  // Parse response
								if ( result.success ) {  // If ajax returns a successful save
									//
								}
							});
						}, 1500 );
					});
				});
			</script>
		';
    }

    /*public function renderCSS() {
        echo '#', $this->_attributes["id"], ' { list-style-type: none; margin: 0; padding: 0; cursor: pointer; max-width: 400px; }';
        echo '#', $this->_attributes["id"], ' li { margin: 0.25em 0; padding: 0.5em; font-size: 1em; }';
    }*/
}
