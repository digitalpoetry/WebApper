<?php
namespace PFBC\Element;

class ModalHeading extends \PFBC\Element {
	public function __construct($value) {
		$properties = array("value" => $value);
		parent::__construct("", "", $properties);
	}

	public function render() {
		$id = $this->_form->getAttribute("id");
		echo '<h3 id="' . $id . 'ModalLabel">' . $this->_attributes["value"] . '</h3>';
	}
}
