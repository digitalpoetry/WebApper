<?php
namespace PFBC\Element;

class Url extends Textbox {
	protected $_attributes = array("type" => "url");

	public function render() {
		$this->_attributes["class"] = "form-control " . $this->_attributes["class"];
		$this->validation[] = new \PFBC\Validation\Url;
		parent::render();
	}
}
