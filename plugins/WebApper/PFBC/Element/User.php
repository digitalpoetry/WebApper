<?php
namespace PFBC\Element;

class User extends Select {
	public function __construct($label, $name, array $properties = null) {
		$options = array('');
		$cap = $properties['cap'];
		global $wp_roles;
		$all_roles = $wp_roles->roles;
		$users = array();
		foreach ( $all_roles as $roleID => $role ) : // Foreach role
			if ( $role['capabilities'][$cap] ) : // If the role has the capability
				$users = array_merge( $users, get_users( array( 'role' => $roleID ) ) ); // Get users who have this role
			endif;
		endforeach;
		foreach ( $users as $user ) : // Foreach role
			$options[$user->ID] = $user->display_name;
		endforeach;
		parent::__construct($label, $name, $options, $properties);
    }
}
