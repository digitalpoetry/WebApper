<?php
namespace PFBC\Element;

class UploadCSV extends \PFBC\Element {

    public function jQueryDocumentReady() {
        echo '';
    }

    public function render() {
		echo '
			<div id="attachContainer">
				<a id="pickfiles" href="javascript:;" class="btn">Browse for CSV files</a>
			</div>
			<div id="console"></div>
			<div id="filelist">Your browser doesn\'t have HTML5 support.</div>
			<input type="hidden" name="web_apper_attachment_ids" id="web_apper_attachment_ids">
			
			<script type="text/javascript">
				// Custom example logic
				var uploader = new plupload.Uploader({
					runtimes : "html5,html4",
					browse_button : "pickfiles", // you can pass in id...
					container: "attachContainer", // ... or DOM Element itself
					urlstream_upload: true,
					url: "' . site_url("/wp-content/plugins/WebApper/Attachment/ajax_handler.php") . '",
					filters : {
						max_file_size : "100mb",
						mime_types: [
							{title : "CSV file", extensions : "csv"}
						]
					},
					// Flash settings
					flash_swf_url : "' . site_url("/wp-content/plugins/WebApper/assets/js/plupload/Moxie.swf") . '",
					// Silverlight settings
					silverlight_xap_url : "' . site_url("/wp-content/plugins/WebApper/assets/js/plupload/Moxie.xap") . '",
					init: {
						PostInit: function() {
							document.getElementById("filelist").innerHTML = "";
						},
						FilesAdded: function(up, files) {
							plupload.each(files, function(file) {
								document.getElementById("filelist").innerHTML += \'<div id="\' + file.id + \'">\' + file.name + \' (\' + plupload.formatSize(file.size) + \') <b></b></div>\';
							});
							uploader.start();
						},
						UploadProgress: function(up, file) {
							document.getElementById(file.id).getElementsByTagName("b")[0].innerHTML = "<span>" + file.percent + "%</span>";
						},
						Error: function(up, err) {
							document.getElementById("console").innerHTML += "\nError #" + err.code + ": " + err.message;
						},
						FileUploaded: function(up, file, info) {
			                var result = jQuery.parseJSON(info.response);  // Parse response
							var current_val = jQuery("#web_apper_attachment_ids").val();
			                if ( current_val == "" ) {
								jQuery("#web_apper_attachment_ids").val( result.id );
							} else {
								jQuery("#web_apper_attachment_ids").val( current_val + "," + result.id );
							}
			            },
					}
				});
				uploader.init();
			</script>
		';
    }

    /*public function renderCSS() {
        echo '#', $this->_attributes["id"], ' { list-style-type: none; margin: 0; padding: 0; cursor: pointer; max-width: 400px; }';
        echo '#', $this->_attributes["id"], ' li { margin: 0.25em 0; padding: 0.5em; font-size: 1em; }';
    }*/
}
