<?php
namespace PFBC\View;

class Collapsible2Col extends \PFBC\View {
	public function render() {
		echo '<form', $this->_form->getAttributes(), '>';
		$this->_form->getErrorView()->render();

		$elements = $this->_form->getElements();
        $elementSize = sizeof($elements);
		$elementsRendered = 0;
        for($e = 0; $e < $elementSize; ++$e) {
            $element = $elements[$e];
			if ( $elementsRendered == ($col_length + 1) && $found_foot == true ) {
				echo '</div><div class="accordion2col">';
			}
            if ($element instanceof \PFBC\Element\CollaspeHead ) {
				$count = 0;
				while ( $found_foot !== true ) {
					if ( $elements[($e + 1 + $count)] instanceof \PFBC\Element\CollaspeFoot ) {
						$found_foot = true;
					} else {
						++$count;
					}
				}
				$col_length = round( ($count / 2) );
				$elementsRendered = 0;
                $element->render();
               	echo '<div class="accordion2col">';
            } elseif ($element instanceof \PFBC\Element\CollaspeFoot ) {
				$found_foot = false;
				$col_length = 0;
               	echo '</div><div class="clearfix"></div>';
                $element->render();
            #} elseif($element instanceof \PFBC\Element\Button) {
            #    echo '<div>';
            #    $element->render();
            #    echo '</div>';
			} else {
                $this->renderLabel($element);
                $element->render();
				$this->renderDescriptions($element);
				if( ($e + 1) == $elementSize && !$elements[($e + 1)] instanceof \PFBC\Element\CollaspeFoot) {
					echo '<input type="submit" value="Save" class="btn btn-primary" data-loading-text="Submitting...">';
				}
            }
			++$elementsRendered;
        }

		echo '</form>';
    }

	protected function renderLabel(\PFBC\Element $element) {
        $label = $element->getLabel();
		echo '<label for="', $element->getAttribute("id"), '">';
        if(!empty($label)) {
			if($element->isRequired())
				echo '<span class="required">* </span>';
			echo $label;	
        }
		echo '</label>'; 
    }
}	
