<?php
namespace PFBC\View;

class Modal extends \PFBC\View {
	protected $class = "form-horizontal";

	public function render() {
		$this->_form->appendAttribute("class", $this->class);

		echo '<div class="modal-dialog modal-lg"><div class="modal-content"><form', $this->_form->getAttributes(), '><fieldset>';
		$this->_form->getErrorView()->render();

		$elements = $this->_form->getElements();
		$elementSize = sizeof($elements);
		$elementCount = 0;
		$submitCount = 1;
		
		$formID = $this->_form->getAttribute("id");
		
		for($e = 0; $e < $elementSize; ++$e) {
			$element = $elements[$e];

			if ( $element instanceof \PFBC\Element\ModalHeading ) {
				echo '<div class="modal-header">
						   ', $element->render(), '
					  </div>
					  <div class="modal-body">
					  	<div class="row">';
			} elseif ( $element instanceof \PFBC\Element\Hidden || $element instanceof \PFBC\Element\HTML || $element instanceof \PFBC\Element\Legend || $element instanceof \PFBC\Element\ModalToggle ) {
				$element->render();
			} elseif ( $element instanceof \PFBC\Element\Button ) {
				if ( $submitCount == 1 ) {
				echo '	</div>
					  </div>
					  <div class="modal-footer">
					  <button id="' . $formID . '_close" class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							  ', $element->render();
				} elseif ( $submitCount == 2 ) {
					$element->render();
					echo '</div>
					</div>';
					  
				}
				$submitCount++;
            } else {
				echo '<div class="form-group">', $this->renderLabel($element), '<div class="col-sm-9">', $element->render(), $this->renderDescriptions($element), '</div></div>';
				++$elementCount;
			}
		}

		echo '</fieldset></form></div><!-- /.modal-content --></div><!-- /.modal-dialog -->';
    }

	protected function renderLabel(\PFBC\Element $element) {
        $label = $element->getLabel();
        if(!empty($label)) {
			echo '<label class="col-sm-3 control-label" for="', $element->getAttribute("id"), '">';
			if($element->isRequired())
				echo '<span class="required">* </span>';
			echo $label, '</label>'; 
        }
    }
}	
