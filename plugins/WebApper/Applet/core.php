<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.

/*
WebApper Moddule Name: Applets
*/

namespace WebApper\Applet;

// Include Module files
require_once 'class.php';
require_once 'api.php';
foreach( glob ( dirname(__FILE__) . '/Shortcode/*.php' ) as $filename ) {
	require_once( $filename );
}


// Hook into plugin activation to install Module DB table
add_filter( 'web_apper_create_table_sql', function ( $sql_array ) {
	global $wpdb;
	
	$sql_array[] = "CREATE TABLE {$wpdb->prefix}web_apper_applets (
		`ID` SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL,
		`applet_id` VARCHAR(55) NOT NULL,
		`applet_name` VARCHAR(100) NOT NULL,
		`applet_description` VARCHAR NOT NULL,
		`applet_shortcode` TEXT NOT NULL,
		`core_item` TINYINT(1) UNSIGNED DEFAULT '0' NOT NULL,
		UNIQUE INDEX (applet_id),
		PRIMARY KEY (ID)
	);";
	
	$sql_array[] = "CREATE TABLE {$wpdb->prefix}web_apper_applet_userstates (
		`ID` INT UNSIGNED AUTO_INCREMENT NOT NULL,
		`applet_user_id` bigint(20) UNSIGNED NOT NULL,
		`applet_workspace_id` VARCHAR(55) NOT NULL,
		`applet_id` VARCHAR(55) NOT NULL,
		`applet_column_id` VARCHAR(55) NOT NULL,
		`applet_sort_no` TINYINT UNSIGNED NOT NULL,
		`applet_collapsed` TINYINT(1) UNSIGNED NOT NULL,
		INDEX (`applet_user_id`),
		INDEX (`applet_workspace_id`),
		PRIMARY KEY (`ID`)
	);";
	return $sql_array;
	
}, 2, 1 );


add_action( 'web_apper_insert_table_rows', function () {
	global $wpdb;
	
	$wpdb->query( "INSERT INTO `{$wpdb->prefix}web_apper_fields` (`field_name`, `field_id`, `field_index_only`, `field_form_only`, `field_field_only`, `field_type`, `field_attributes`, `field_required`, `field_placeholder`, `field_short_desc`, `field_long_desc`, `field_options`, `field_default_value`, `field_validation`, `field_regex`, `field_error_message`, `field_dt_show_col_default`, `field_dt_format_value`, `field_dt_filter_type`, `field_dt_filter_options`, `field_dt_filter_value`, `field_bulk_edit`, `field_read_only`, `field_dynamic_value_flow_id`, `field_action_flow_id`, `field_form_flow_id`, `core_item`) VALUES
		('Applet ID','applet_id',0,0,0,'Textbox','',1,'','','','','','','','',1,'','search','','',0,0,'','','',0),
		('Title','applet_name',0,0,0,'Textbox','',0,'','','','','','','','',1,'','search','','',0,0,'','','',0),
		('Description','applet_description',0,0,0,'Textarea','',0,'','','','','','','','',1,'','search','','',0,0,'','','',0),
		('Shortcode','applet_shortcode',0,0,0,'Textbox','',1,'','','','','','','','',1,'','search','','',0,0,'','','',0)
	;" );
	
}, 1, 0 );