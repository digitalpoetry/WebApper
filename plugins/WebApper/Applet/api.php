<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.


/**
 * Add Applet
 * 
 * @since WebApper (1.0)
 * @param arr $itemData The Applet details
 * @return mixed (int) insert ID on success, (bool) false on fail
 */
function web_apper_insert_applet( $itemData ) {
	$itemData = apply_filters( 'applet_pre_insert', $itemData ); // Allow filtering of the Applet data before saving;

	$item = new WebApper\Applet\Applet;

	foreach ( $itemData as $key => $val ) :
		$item->$key = $val;
	endforeach;

	if ( $item->save() ) :
		do_action( 'applet_post_insert', $item ); // Allow Applet data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Update Applet
 * 
 * @since WebApper (1.0)
 * @param int $id The Applet ID
 * @param arr $itemData The Applet details
 * @return bool true on success, false on failure
 */
function web_apper_update_applet( $id, $itemData ) {	

	$itemData = apply_filters( 'applet_pre_update', $itemData ); // Allow filtering of the Applet data before saving;
	
	$item = new WebApper\Applet\Applet( $id );

	foreach ( $itemData as $key => $val ) :
		$item->$key = $val;
	endforeach;

	if ( $item->save() ) :
		do_action( 'applet_post_update', $item ); // Allow Applet data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Delete Applet
 *
 * @since WebApper (1.0)
 * @param int $id The Applet ID
 * @return bool true on success, false on failure
 */
function web_apper_delete_applet( $id ) {
	
	do_action( 'applet_pre_delete', $id ); // Allow Applet data to be hooked onto
	
	$item = new WebApper\Applet\Applet($id);
	
	if ( $item->delete() ) :
		do_action( 'applet_post_delete', $item ); // Allow Applet data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Get Applet
 * 
 * @since WebApper (1.0)
 * @param int $id The Applet ID
 * @return (obj) The Applet
 */
function web_apper_get_applet( $id ) {
	$item = new WebApper\Applet\Applet( $id );
	return (object) $item->get_data();
}

/**
 * Get Results
 *
 * @since WebApper (1.0)
 * @return arr Associative array of Applet objects
*/
function web_apper_applet_get_results( $query ) {
	return $items = WebApper\Applet\Applet::get_results( $query );
}

/**
 * Queries the DB to get applets by 'applet_id'
 *
 * @param array $atts
 * @param string $orderBy
 * @since 1.0
 */
function web_apper_get_applet_by_applet_id( $applet_id ) {
	global $wpdb;
	$item = web_apper_applet_get_results(  "SELECT * FROM {$wpdb->prefix}web_apper_applets WHERE applet_id = '{$applet_id}'" );
	return $item[0];	
}

/**
 * Evaluate applet
 *
 * @since WebApper (1.0)
 * @return mixed
*/
function web_apper_applet_evaluate( $id ) {
	$item = web_apper_get_applet( $id ); // Get the equaltion
	return eval( "return " . $item->applet_shortcode . ";");
}

/**
 * Add Applet
 * 
 * @since WebApper (1.0)
 * @param arr $itemData The Applet details
 * @return mixed (int) insert ID on success, (bool) false on fail
 */
function web_apper_insert_applet_userstate( $itemData ) {
	$itemData = apply_filters( 'applet_userstate_pre_insert', $itemData ); // Allow filtering of the Applet data before saving;

	$item = new WebApper\Applet\AppletUserState;

	foreach ( $itemData as $key => $val ) :
		$item->$key = $val;
	endforeach;

	if ( $item->save() ) :
		do_action( 'applet_post_insert', $item ); // Allow Applet data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Update Applet
 * 
 * @since WebApper (1.0)
 * @param int $id The Applet ID
 * @param arr $itemData The Applet details
 * @return bool true on success, false on failure
 */
function web_apper_update_applet_userstates( $id, $itemData ) {	

	$itemData = apply_filters( 'applet_pre_update', $itemData ); // Allow filtering of the Applet data before saving;
	
	$item = new WebApper\Applet\AppletUserState( $id );

	foreach ( $itemData as $key => $val ) :
		$item->$key = $val;
	endforeach;

	if ( $item->save() ) :
		do_action( 'applet_post_update', $item ); // Allow Applet data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Get Results
 *
 * @since WebApper (1.0)
 * @return arr Associative array of Applet objects
*/
function web_apper_applet_userstates_get_results( $query ) {
	return $items = WebApper\Applet\AppletUserState::get_results( $query );
}

/**
 * Queries the DB to get applet user states by 'user_id' & 'workspace_id'
 *
 * @param array $atts
 * @param string $orderBy
 * @since 1.0
 */
function web_apper_get_applet_userstates( $user_id, $workspace_id ) {
	global $wpdb;
	return $items = web_apper_applet_userstates_get_results(  "SELECT * FROM `{$wpdb->prefix}web_apper_applet_userstates` WHERE `applet_user_id`= '{$user_id}' AND `applet_workspace_id` = '{$workspace_id}'" );
}
