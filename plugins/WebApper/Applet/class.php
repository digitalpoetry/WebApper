<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.


namespace WebApper\Applet;

class Applet extends \WebApper\Module\Item {

	/**
	 * The name of the module DB table
	 *
	 * @var string
	 */
	public $table = "web_apper_applets";

}

class AppletUserState extends \WebApper\Module\Item {

	/**
	 * The name of the module DB table
	 *
	 * @var string
	 */
	public $table = "web_apper_applet_userstates";

}