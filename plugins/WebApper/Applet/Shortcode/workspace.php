<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.


namespace WebApper\Applet;

/*
 * [workspace id="test" include="user-tasks"]
 *
 */
class Workspace extends \WebApper\IndexView {
	
    /**
     * Define shortcode properties
     *
     */
	protected $item_id = 'applet_user_state';
	protected $item_label = 'Applet User State';
	protected $shortcode = 'workspace';
	protected $defaults = array(
		'id' => null,
		'include' => null,
		'viewcap' => 'edit_posts',	// The Required capability to view
	);

    /**
     * Handles the shortcode
     *
     * @param array $atts
     */
    public function shortcode( $atts ) {
    	global $current_user;
		$columns = 2; // TODO: Set this var with a shortcode attribute

		// Allow filtering of shortcode attributes before rendering
		$atts = apply_filters( $this->shortcode . '_atts', $atts );

		// Extract shortcode attributes into individual vars and also store as an array 
		extract( $atts = shortcode_atts( $this->defaults, $atts ) );

		// Check for required shortcode attributes
		$msg = $this->has_req_attrs( $atts );
		if ( $msg !== true )
			return $msg;

        // Check if current user has proper privileges to view
		if ( !$this->current_user_has_cap($viewcap) ) :
			echo 'You do not have sufficient permissions to access this content.';
			return;
		endif;
		
		// Get Applet user states from the DB
		$applet_user_states = web_apper_get_applet_userstates( $current_user->id, $id );
		if ( !empty($applet_user_states) ) :
			foreach ( $applet_user_states as $applet_state ) {
				$temp_columns[$applet_state['applet_column_id']][$applet_state['applet_sort_no']] = $applet_state;
			}
			foreach ( $temp_columns as $temp_column_id => $temp_column ) {
				ksort($temp_column);
				$applet_columns[$temp_column_id] = $temp_column;
			}
			ksort($applet_columns);
		endif;
		
		// Build the shortcode output hrml string
		?>
		
		<div class="row">
	
		<?php if ( empty($applet_user_states) ) :
			$action = 'add_applet_user_states';
			$i_columns = 1;
			echo '<div class="panel-group col-sm-6 column" id="' . $i_columns . '" role="tablist" aria-multiselectable="true">';
			foreach ( explode( ',', $include ) as $include_applet ) :
				$applet = web_apper_get_applet_by_applet_id( $include_applet );
				?>
				<div class="panel panel-default applet" applet" id="<?php echo $applet['ID']; ?>">
					<div class="panel-heading" role="tab" id="Heading<?php echo $applet['ID']; ?>">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#Collapse<?php echo $applet['ID']; ?>" aria-expanded="false" aria-controls="Collapse<?php echo $applet['ID']; ?>">
								<?php echo  $applet['applet_name']; ?>
							</a>
						</h4>
					</div>
					<div id="Collapse<?php echo $applet['ID']; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Heading<?php echo $applet['ID']; ?>">
						<div class="panel-body">
							<?php echo do_shortcode( stripslashes($applet['applet_shortcode']) ); ?>
						</div>
					</div>
				</div>
				<?php
			endforeach;
			echo '</div>';
			while ( $i_columns < $columns ) :
				$i_columns++;
				echo '<div class="column col-sm-6" id="' . $i_columns . '" ></div>';
			endwhile;
		else:
			$action = 'update_applet_user_states';
			$i_columns = 0;
			foreach ( $applet_columns as $applet_column_id => $applet_states ) :
				$i_columns++;
				echo '<div class="panel-group col-sm-6 column" id="' . $i_columns . '" role="tablist" aria-multiselectable="true">';
				foreach ( $applet_states as $applet_state ) :
					$applet = web_apper_get_applet( $applet_state['applet_id'] );
					?>
					<div class="panel panel-default applet" applet" id="<?php echo $applet_state['ID']; ?>">
						<div class="panel-heading" role="tab" id="Heading<?php echo $applet_state['ID']; ?>">
							<h4 class="panel-title">
								<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#Collapse<?php echo $applet_state['ID']; ?>" aria-expanded="<?php echo $applet_state['applet_collapsed'] ? 'true' : 'false'; ?>" aria-controls="Collapse<?php echo $applet_state['ID']; ?>">
									<?php echo $applet->applet_name; ?>
								</a>
							</h4>
						</div>
						<div id="Collapse<?php echo $applet_state['ID']; ?>" class="panel-collapse collapse<?php if ( !$applet_state['applet_collapsed']) echo ' in'; ?>" role="tabpanel" aria-labelledby="Heading<?php echo $applet_state['ID']; ?>">
							<div class="panel-body">
								<?php echo do_shortcode( stripslashes($applet->applet_shortcode) ); ?>
							</div>
						</div>
					</div>
					<?php
				endforeach;
				echo '</div>';
			endforeach;
			while ( $i_columns < $columns ) :
				$i_columns++;
				echo '<div class="column col-sm-6" id="' . $i_columns . '" ></div>';
			endwhile;
		endif; ?>

		</div><!--/.row -->
		<script type="text/javascript" >
		// MAIN STAGE DRAGGABLE BOXES
		
		jQuery(function(){
			jQuery('.applet')
			.each(function(){
				jQuery(this).find('.panel-title a').hover()
				.click( function(){
					setTimeout( function() { updateWidgetData(); }, 500 );
				} )
				.end()
			});
		    
			jQuery('.column').sortable({
				connectWith: '.column',
				handle: 'h4',
				cursor: 'move',
				placeholder: 'placeholder',
				forcePlaceholderSize: true,
				opacity: 0.4,
				stop: function(event, ui){
					updateWidgetData();
				}
			})
			.disableSelection();
		});
		
		var web_apper_action = '<?php echo $action; ?>';
		function updateWidgetData(){
			var items=[];
			jQuery('.column').each(function(){
				var columnId=jQuery(this).attr('id');
				jQuery('.applet', this).each(function(i){
					var collapsed = 1;
					console.log(jQuery(this).find('.collapse'));
					console.log(jQuery(this).find('.collapse').hasClass('in'));
					if(jQuery(this).find('.collapse').hasClass('in'))
						collapsed = 0;
					var item = {
						id: jQuery(this).attr('id'),
						column: columnId,
						order : i,
						collapsed: collapsed
					};
					items.push(item);
				});
			});

			// Pass sortorder variable to server using ajax to save state
			var data = {
				id: '<?php echo $id; ?>',
				action: 'web_apper<?php echo $this->shortcode; ?>',
				web_apper_action: web_apper_action,
				web_apper_nonce: '<?php echo wp_create_nonce("AwesomeSauce!87"); ?>',
				web_apper_items: items,
			};
			jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", data, function(response){
				var result = jQuery.parseJSON(response);
				if( result.success === true ) {
					// TODO: Figure out where the 4 below lines belong
					//if( result.refresh === true ) {
					//	web_apper_action = 'update_applet_user_states';
					//	window.location = window.location.pathname; // refresh page
					//}
				} else {
					jQuery('.page-content').prepend(result.htmlalert);  // Show and alert
				}
			});

		}

		</script>
		<?php
	}

    /**
     * Checks if a shortcode has required attributes
     *
	 * @param string $usercap
     * @since 1.0
     */
	protected function has_req_attrs( $atts ) {
		// Check for required shortcode attributes
		if ( $atts['id'] == null )
			return 'You must give the shortcode a unique ID, i.e. [shortcode id=\'code-1\']';
		if ( $atts['include'] == null )
			return 'You must include at least 1 applet_id, i.e. [shortcode include=\'applet-1\']';
		return true;
	}

    /**
     * Build modal wrapper for a dataTable
     *
     * @since 0.1.0
     */
	function build_dt_modal_wrapper( $id ) {
		?><div id="<?php echo $id; ?>Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="<?php echo $id; ?>ModalLabel" aria-hidden="true"></div><?php
	}

    /**
     * Add Applet user states to the database
     *
     * @since 0.1.0
     */
	public function add_applet_user_states() {
		global $current_user;
		$errors = false;
		foreach ( $_POST['web_apper_items'] as $applet ) {
			$itemData = array(
				'applet_user_id' => $current_user->ID,	
				'applet_workspace_id' => $_POST['id'],	
				'applet_id' => $applet['id'],	
				'applet_column_id' => $applet['column'],
				'applet_sort_no' => $applet['order'],	
				'applet_collapsed' => $applet['collapsed'],	
			);
			$result = web_apper_insert_applet_userstate( $itemData );
			// TODO: Return the insert id and html inject it
			if ( $result == false ) {
				$errors = true;	
			}
		}

		// Send ajax response
		if ( $errors ) :
			return json_encode( $this->send_response( 'There was a problem saving the changes to your workspace.', 'Oh snap!', 'alert-error', false ) );  // Send Response
		else :
			return json_encode( array( 'success' => true, 'refresh' => true ) );  // Send Response
		endif;
	}

    /**
     * Update Applet user states to the database
     *
     * @since 0.1.0
     */
	public function update_applet_user_states() {
		
		foreach ( $_POST['web_apper_items'] as $applet ) {
			$itemData = array(
				'applet_column_id' => $applet['column'],
				'applet_sort_no' => $applet['order'],	
				'applet_collapsed' => $applet['collapsed'],	
			);
			$result = web_apper_update_applet_userstates( $applet['id'], $itemData );
			if ( $result == false ) {
				$errors = true;	
			}
		}

		// Send ajax response
		if ( $errors ) :
			return json_encode( $this->send_response( 'There was a problem saving the changes to your workspace.', 'Oh snap!', 'alert-error', false ) );  // Send Response
		else :
			return json_encode( array( 'success' => true, 'refresh' => false ) );  // Send Response
		endif;
	}

}

$initialize = new Workspace(); 

?>