<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.

/**
 * Flow module functions
 *
 * Flow module API functions. Use theses functions to work with Flows
 *
 * @since 0.1.0
 *
 * @package WebApper
 * @subpackage Flow
 */


/**
 * Add Flow
 * 
 * @since 0.1.0
 * @param arr $itemData The Flow details
 * @return mixed (int) insert ID on success, (bool) false on fail
 */
function web_apper_insert_flow( $itemData ) {
	$itemData = apply_filters( 'flow_pre_insert', $itemData ); // Allow filtering of the Flow data before saving;

	$item = new WebApper\Flow\Flow;

	foreach ( $itemData as $key => $val ) :
		$item->$key = $val;
	endforeach;

	if ( $item->save() ) :
		do_action( 'flow_post_insert', $item ); // Allow Flow data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Update Flow
 * 
 * @since 0.1.0
 * @param int $id The Flow ID
 * @param arr $itemData The Flow details
 * @return bool true on success, false on failure
 */
function web_apper_update_flow( $id, $itemData ) {	

	$itemData = apply_filters( 'flow_pre_update', $itemData ); // Allow filtering of the Flow data before saving;
	
	$item = new WebApper\Flow\Flow( $id );

	foreach ( $itemData as $key => $val ) :
		$item->$key = $val;
	endforeach;

	if ( $item->save() ) :
		do_action( 'flow_post_update', $item ); // Allow Flow data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Delete Flow
 *
 * @since 0.1.0
 * @param int $id The Flow ID
 * @return bool true on success, false on failure
 */
function web_apper_delete_flow( $id ) {
	
	do_action( 'flow_pre_delete', $id ); // Allow Flow data to be hooked onto
	
	$item = new WebApper\Flow\Flow($id);
	
	if ( $item->delete() ) :
		do_action( 'flow_post_delete', $item ); // Allow Flow data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Get Flow
 * 
 * @since 0.1.0
 * @param int $id The Flow ID
 * @return (obj) The Flow
 */
function web_apper_get_flow( $id ) {
	$item = new WebApper\Flow\Flow( $id );
	return (object) $item->get_data();
}

/**
 * Get Results
 *
 * @since 0.1.0
 * @return arr Associative array of Flow objects
*/
function web_apper_flow_get_results( $query ) {
	return $items = WebApper\Flow\Flow::get_results( $query );
}

/**
 * Queries the DB to get flow by 'flow_id'
 *
 * @param array $atts
 * @param string $orderBy
 * @since 1.0
 */
function web_apper_get_flow_by_flow_id( $flow_id ) {
	global $wpdb;
	$item = web_apper_flow_get_results(  "SELECT * FROM {$wpdb->prefix}web_apper_flows WHERE flow_id = '{$flow_id}'" );
	return $item[0];	
}

/**
 * Hook to evalute Action Flow.
 *
 * Hook onto Fields module and evalute Custom Field Action Flows, first checking
 * if the Flow conditions (if any) are met, subsequently preforming an Action.
 * Used for multiple Custom Fields.
 *
 * @since 0.1.0
 *
 * @see WebApper\field\Field.
 * @see WebApper/Field/api.php.
 * @uses web_apper_flow_actions_eval_field().
 *
 * @param array $itemData Associative array of custom field values by name.
 */
function web_apper_flow_actions_eval_fields( $itemData ) {
	// Loop thru each custom field.
	foreach ( $itemData as $field_id => $value ) {
		// Get the custom field data.
		$field = web_apper_get_field_by_field_id( $field_id );
		// Set the flow_ids
		$flow_ids = $field->field_action_flow_id;
		if ( !empty($flow_ids) ) {
			web_apper_flow_actions_eval_field( $field_value, $flow_ids );
		}
	}
}
add_action( 'field_post_insert', 'web_apper_flow_actions_eval_fields', 10, 1 );
add_action( 'field_post_update', 'web_apper_flow_actions_eval_fields', 10, 1 );

/**
 * Evalute Action Flows.
 *
 * Evalutes Action Flows, first checking if the Flow conditions (if any) are
 * met, subsequently preforming an Action. Used for a single Custom Field.
 *
 * @since 0.1.0
 *
 * @uses web_apper_flow_eval_condition().
 *
 * @param string $flow_ids A comma-seperated string of flow_id's of the Flows to  be evaluated.
 * @return boolean Returns true if at least 1 Flow condition was true, false if not.
 */
function web_apper_flow_actions_eval_field( $field_value, $flow_ids ) {
	$return = false;
	// Loop thru each flow_id.
	foreach ( explode(',', $flow_ids ) as $flow_id ) {
		// Get the Flow evaluation result and subsequently preform an Action
		$action_id = web_apper_flow_eval_condition( $field_value, $flow_id );
		if ( $action_id != false ) {
			$action = web_apper_get_action_by_action_id( $action_id );
			$action_code = stripcslashes( $action['action_code'] );
			eval( $action_code );
			$return = true;
		}
	}
	return $return;
}

/**
 * Hook to evaluate Dynamic Value Flow.
 *
 * Hook onto Fields module and evalute Custom Field Dynamic Value Flow, first
 * checking if the Flow conditions (if any) are met, subsequently returning a
 * static or calculated value. Used for multiple Custom Fields.
 *
 * @since 0.1.0
 *
 * @see WebApper\field\Field.
 * @see WebApper/Field/api.php.
 * @uses web_apper_flow_values_eval_field().
 *
 * @param array $itemData Associative array of custom field values by name.
 * @return array Associative array of custom field values by name.
 */
function web_apper_flow_values_eval_fields( $itemData ) {
	// Loop thru each custom field.
	foreach ( $itemData as $field_id => $value ) {
		// Get the custom field data.
		$field = web_apper_get_field_by_field_id( $field_id );
		// Set the flow_id, the should only be 1 value
		$flow_id = $field->field_dynamic_value_flow_id;
		if ( !empty($flow_id) ) {
			$dynamic_value = web_apper_flow_values_eval_field($flow_id);
			if ( !empty($dynamic_value) ) {
				$itemData[$field_id] = $dynamic_value;
			}
		}
	}
	return $itemData;
}
add_filter( 'field_pre_insert', 'web_apper_flow_values_eval_fields', 10, 1 );
add_filter( 'field_pre_update', 'web_apper_flow_values_eval_fields', 10, 1 );

/**
 * Evalute Dynamic Value Flow.
 *
 * Evalutes a Dynamic Value Flow, first checking if the Flow conditions (if any)
 * are met, subsequently returning a static or calculated value. Used for a
 * single Custom Field.
 *
 * @since 0.1.0
 *
 * @uses web_apper_flow_eval_condition().
 *
 * @param string $flow_id The string flow_id of the Flow to be evaluated.
 * @return mixed Returns string|array if the Flow conditions are true, false if not.
 */
function web_apper_flow_values_eval_field( $flow_id ) {
	// Get the Flow evaluation result and subsequently return a Dynamic Value
	$equation_id = web_apper_flow_eval_condition( $flow_id  );
	if ( 0 < $equation_id ) {
		return web_apper_equation_evaluate( $equation_id );
	}
}

/**
 * Evalute Flow Conditionals and default.
 *
 * Evalutes a Dynamic Value Flow, first checking if the Flow conditions (if any)
 * are met and subsequently returning on the first true conditional or the defualt.
 *
 * @since 0.1.0
 *
 * @param string $flow_id The string flow_id of the Flow to be evaluated.
 * @return mixed Returns string|array if the Flow conditions are true or a defualt
 * 				 is provided, false if not.
 */
function web_apper_flow_eval_condition( $field_value, $flow_id  ) {
	// Get the flow
	$flow =  web_apper_get_flow_by_flow_id($flow_id);
	// Extract the flow parts
	preg_match_all( "%IF\s(.+?\sTHEN\s.+?)(ELSE|$)%", $flow['flow_code'], $flow_parts );
	// Get the default.
	preg_match( "%ELSE\s(.*)%", $flow['flow_code'], $default );
	// Loop thru the conditionals, and return on the first one that is true
	foreach ( $flow_parts[1] as $flow ) :
		$parts = explode( 'THEN', $flow );
		if ( web_apper_condition_evaluate( $field_value, trim($parts[0]) ) ) :
			return trim($parts[1]);
		endif;
	endforeach;
	// If nothing has been returned, return the default or false if there is none.
	if ( !empty($default[1]) ) {
		return trim($default[1]);
	} else {
		return false;
	}
}

/**
 * Build Flow javascript
 *
 * @since 0.1.0
 * @return mixed
*/
function web_apper_build_flow_js( $field ) {
	$field_flow =  web_apper_get_flow_by_flow_id( $field['field_form_flow_id']  );
	preg_match_all( "%(IF|ELSEIF)\s{0,}([^\s]+?)\s{0,}(SHOW|HIDE)\s{0,}([^\s]*)%", $field_flow['flow_code'], $flows );
	preg_match( "%ELSE\s{0,}(SHOW|HIDE)\s{0,}([^\s]*)%", $field_flow['flow_code'], $default );
	$default_toggle_fields = array();
	if ( !empty($default) ) :
		$default_toggle_fields = explode(',', $default[1]);
	endif;

	echo '<script type="text/javascript">
		jQuery(document).ready(function($) {	
	';
	
	$flow_js = "
		function condition_" . str_replace('-', '_', $field['field_form_flow_id']) . "() {

			var show = [];
			var hide = [];
	";
	//print_r($flows);
	foreach ( $flows[0] as $flow ) :
		preg_match( "%(SHOW|HIDE)%", $flow, $toggle );
		preg_match( "%(IF|ELSEIF)\s{0,}([^\s]+?)\s{0,}(SHOW|HIDE)%", $flow, $condition );
		$condition = $condition[2];
		preg_match( "%(SHOW|HIDE)\s{0,}([^\s]*)%", $flow, $toggle_fields );
		$toggle_fields = explode(',', $toggle_fields[2]);
		$flow_js_show = '';
		$flow_js_hide = '';
		$default_flow_js_show = '';
		$default_flow_js_hide = '';

		foreach ( $toggle_fields as $toggle_field ) :
			$flow_js_show .= "show.push('" . trim($toggle_field) . "');
			";
			$flow_js_hide .= "hide.push('" . trim($toggle_field) . "');
			";
		endforeach;
		
		foreach ( $default_toggle_fields as $default_toggle_field ) :
			$default_flow_js_show .= "show.push('" . trim($default_toggle_field) . "');
			";
			$default_flow_js_hide .= "hide.push('" . trim($default_toggle_field) . "');
			";
		endforeach;
		
		if ( $toggle[0] == 'SHOW' ) :
			$toggle_js_true = $flow_js_show . $default_flow_js_hide;
			$toggle_js_false = $default_flow_js_show . $flow_js_hide;
		else :
			$toggle_js_true = $flow_js_hide . $default_flow_js_show;
			$toggle_js_false = $default_flow_js_hide . $flow_js_show;
		endif;
		


		$flow_js .= web_apper_build_condition_js_vars( trim($condition) );
		$flow_js .= "
			if ( " . web_apper_build_condition_js( trim($condition) ) . " ) {
		" . $toggle_js_true . "
			} else {
		" . $toggle_js_false . "
			}
		";
		endforeach;
		$flow_js .= "
		//	console.log(show);
		//	console.log(hide);
		//	console.log('===');
				showHideFields( show, hide, '" . $toggle[0] . "' );
			}
		";
		
		echo $flow_js . "
			condition_" . str_replace('-', '_', $field['field_form_flow_id']) . "();
			jQuery('form [name=\"" . $field['field_id'] . "\"]').live('change keyup', function() {
				condition_" . str_replace('-', '_', $field['field_form_flow_id']) . "();
			});
		});
		";
	echo '</script>';
}