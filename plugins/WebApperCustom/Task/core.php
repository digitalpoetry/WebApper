<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.

/*
WebApper Moddule Name: Tasks
*/

namespace WebApper\Task;

// Include Module files
require_once 'class.php';
require_once 'api.php';
foreach( glob ( dirname(__FILE__) . '/Shortcode/*.php' ) as $filename ) {
	require_once( $filename );
}


// Add all hooks at WordPress init
add_filter( 'init', function () {
	
	// Hook into plugin activation to install Module DB table
	add_filter( 'web_apper_create_table_sql', function ( $sql_array ) {
		global $wpdb;
		
		$sql_array[] = "CREATE TABLE `{$wpdb->prefix}web_apper_tasks` (
			`ID` SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL,
			`task_subject` VARCHAR(255) DEFAULT '' NOT NULL,
			`task_due` DATETIME,
			`task_task_type` VARCHAR(55) DEFAULT 'generic' NOT NULL,
			`task_author_id` SMALLINT(6) UNSIGNED DEFAULT '0' NOT NULL,
			`task_user_id` SMALLINT(6) UNSIGNED NOT NULL,
			`task_description` VARCHAR(255) DEFAULT '' NOT NULL,
			`task_status` VARCHAR(55) NOT NULL,
			`task_post_id` SMALLINT UNSIGNED,
			`task_created_on` DATETIME NOT NULL,
			`task_edited_on` DATETIME NOT NULL,
			PRIMARY KEY (`ID`)
		);";
		
		return $sql_array;
	}, 2, 1 );
	
	add_action( 'web_apper_insert_table_rows', function () {
		global $wpdb;
		
		// Deafult Fields
		$wpdb->query( "INSERT INTO `{$wpdb->prefix}web_apper_fields` (`field_name`, `field_id`, `field_index_only`, `field_form_only`, `field_field_only`, `field_type`, `field_attributes`, `field_required`, `field_placeholder`, `field_short_desc`, `field_long_desc`, `field_options`, `field_default_value`, `field_validation`, `field_regex`, `field_error_message`, `field_dt_show_col_default`, `field_dt_format_value`, `field_dt_filter_type`, `field_dt_filter_options`, `field_dt_filter_value`, `field_bulk_edit`, `field_read_only`, `field_dynamic_value_flow_id`, `field_action_flow_id`, `field_form_flow_id`, `core_item`) VALUES
			('Subject','task_subject',0,0,0,'Textbox','',1,'','','','','','','','',1,'','search','','',0,0,'','','',1),
			('Due on','task_due',1,0,0,'Hidden','',0,'','','','','','','','',1,'date','date','','',0,0,'','','',1),
			('Type','task_task_type',0,1,0,'Select','',0,'','','','generic,Generic','generic','','','',1,'','search','','',0,0,'','','',1),
			('Created by','task_author_id',1,0,0,'Hidden','',0,'','','','','','','','',1,'user_id','','','',0,0,'','','',1),
			('Assigned to','task_user_id',0,0,0,'User','',1,'','','','','edit_posts','','','',1,'user_id','','','',0,0,'','','',1),
			('Description','task_description',0,0,0,'Textarea','',1,'','','','','','','','',1,'','search','','',0,0,'','','',1),
			('Status','task_status',0,0,0,'Select','',0,'','','','assigned,Assigned|in-progress,In-progress|completed,Completed|archived,Archived','assigned','','','',1,'','equals','assigned,Assigned|in-progress,In-progress|completed,Completed','',0,0,'','','',1),
			('Record','task_post_id',1,0,0,'Hidden','',0,'','','','','','','','',1,'post_id','search','','',0,0,'','','',1),
			('Created on','task_created_on',1,0,0,'Hidden','',0,'','','','','','','','',1,'date','date','','',0,0,'','','',1),
			('Edited on','task_edited_on',1,0,0,'Hidden','',0,'','','','','','','','',1,'date','date','','',0,0,'','','',1),
			('Due','task_due_type',0,1,1,'Radio','',0,'','','','in,in|on,on','due_in','','','',1,'','search','','',0,0,'','','tasks_due_type',0),
			('Due in','task_due_in_number',0,1,1,'Number','',0,'','','','','','','','',1,'','search','','',0,0,'','','',0),
			(' ','task_due_in_unit',0,1,1,'Radio','',0,'','','','day,day|week,week|month,month','','','','',1,'','search','','',0,0,'','','',0),
			('Due on','task_due_on_datetime',0,1,1,'DateTimeLocal','',0,'','','','','','','','',1,'','search','','',0,0,'','','',0)
		;" );
		
		// Deafult Conditionals
		$wpdb->query( "INSERT INTO `{$wpdb->prefix}web_apper_conditions` (`condition_name`, `condition_id`, `condition_left_type`, `condition_left_side`, `condition_operator`, `condition_right_type`, `condition_right_side`, `core_item`) VALUES
			('Tasks - Due type on','tasks_due_type_on','field','task_due_type','is equal to','static','on',1),
			('Tasks - Due type in','tasks_due_type_in','field','task_due_type','is equal to','static','in',1)
		;" );
		
		// Deafult Flows
		$wpdb->query( "INSERT INTO `{$wpdb->prefix}web_apper_flows` (`flow_name`, `flow_id`, `flow_type`, `flow_code`, `flow_hook`, `core_item`) VALUES
			('Tasks - Due type','tasks_due_type','form','IF tasks_due_type_in SHOW task_due_in_number,task_due_in_unit ELSEIF tasks_due_type_on SHOW task_due_on_datetime','',1)
		;" );

	}, 1, 0 );

}, 1, 0 );

/*
// Shows Tasks related to a record
function dt_ssp_filter_tasks_singleview( $where, $id ) {
	if ( $id === 'record_tasks' ) :
		$where = 'task_post_id = ' . $_POST['web_apper_post_ids'];
	endif;
	return $where;
}
add_filter( 'dt_ssp_filter', 'WebApper\Task\dt_ssp_filter_tasks_singleview', 1, 2 );
*/
/*
// Shows Tasks assigned to logged in user
function dt_ssp_filter_tasks_user( $where, $id ) {
	if ( $id === 'tasks' ) :
		global $current_user;
		$where = 'task_user_id = ' . $current_user->ID . ' AND `status` != \'archived\'';
	endif;
	return $where;
}
add_filter( 'dt_ssp_filter', 'WebApper\Task\dt_ssp_filter_tasks_user', 1, 2 );
*/

