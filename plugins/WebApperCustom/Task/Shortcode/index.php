<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.


namespace WebApper\Task;

/*
 * [tasks]
 *
 */
class TaskIndex extends \WebApper\IndexView {
	
    /**
     * Define shortcode properties
     *
     */
	protected $item_id = 'task';
	protected $item_label = 'Task';
	protected $shortcode = 'tasks';
	protected $defaults = array(
		'id' => 'tasks',
		'include' => 'task_task_type,task_subject,task_description,task_due,task_status,task_post_id,task_user_id,task_created_on,task_edited_on,task_due_type,task_due_in_number,task_due_in_unit,task_due_on_datetime',
		'viewcap' => 'edit_posts',		 // The Required capability to view
		'addcap' => 'publish_posts',	 // The Required capability to add
		'editcap' => 'edit_posts',		 // The Required capability to edit
		'deletecap' => 'delete_plugins', // The Required capability to delete
		'filter' => null,				 // The Condition to filter the dataTable records
		'colvis_control' => true,		 // Enable the colVis button for the dataTable, true or false
		'form_controls' => true,		 // Enable the form controls for the dataTable, true or false
		'colfilter_controls' => true,	 // Enable the column utton for the dataTable, true or false
		'actions_control' => true,		 // Enable the actions filters for the dataTable, true or false
		'adtl_actions' => 'div,div|SelectAll,Select All|ResetFilters,Reset Filters', // Specify addition action buttons for the dataTable
		'rightclick_menu' => true,		 // Enable rightclick menu on dataTable rows, true or false.
		'row_selection' => true,		 // Enable dataTable row selection, true or false.
	);

}

$initialize = new TaskIndex(); 

?>