<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.


/**
 * Add Task
 * 
 * @since WebApper (1.0)
 * @param arr $itemData The Task details
 * @return mixed (int) insert ID on success, (bool) false on fail
 */
function web_apper_insert_task( $itemData ) {
	global $current_user;
	
	$itemData['task_created_on'] = current_time('mysql');
	$itemData['task_edited_on'] = current_time('mysql');
	$itemData['task_author_id'] = $current_user->ID;
	$itemData['task_due'] = web_apper_task_due( 
		$_POST['task_due_type'],
		$_POST['task_due_in_number'],
		$_POST['task_due_in_unit'],
		$_POST['task_due_on_datetime']
	);
	// Assign this task to the current user if a user ID is not passed
	if ( !empty( $_POST['task_user_id'] ) ) :
		$itemData['task_user_id'] = $_POST['task_user_id'];
	else :
		$itemData['task_user_id'] =  $current_user->ID;
	endif;
	
	// Allow filtering of the Task data before saving;
	$itemData = apply_filters( 'task_pre_insert', $itemData );

	$item = new WebApper\Task\Task;

	foreach ( $itemData as $key => $val ) :
		$item->$key = $val;
	endforeach;

	if ( $item->save() ) :
		do_action( 'task_post_insert', $item ); // Allow Task data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Update Task
 * 
 * @since WebApper (1.0)
 * @param int $id The Task ID
 * @param arr $itemData The Task details
 * @return bool true on success, false on failure
 */
function web_apper_update_task( $id, $itemData ) {	
	
	$itemData['task_edited_on'] = current_time('mysql');
	$itemData['task_due'] = web_apper_task_due( 
		$_POST['task_due_type'],
		$_POST['task_due_in_number'],
		$_POST['task_due_in_unit'],
		$_POST['task_due_on_datetime']
	);

	$itemData = apply_filters( 'task_pre_update', $itemData ); // Allow filtering of the Task data before saving;
	
	$item = new WebApper\Task\Task( $id );

	foreach ( $itemData as $key => $val ) :
		$item->$key = $val;
	endforeach;

	if ( $item->save() ) :
		do_action( 'task_post_update', $item ); // Allow Task data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Delete Task
 *
 * @since WebApper (1.0)
 * @param int $id The Task ID
 * @return bool true on success, false on failure
 */
function web_apper_delete_task( $id ) {
	
	do_action( 'task_pre_delete', $id ); // Allow Task data to be hooked onto
	
	$item = new WebApper\Task\Task($id);
	
	if ( $item->delete() ) :
		do_action( 'task_post_delete', $item ); // Allow Task data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Get Task
 * 
 * @since WebApper (1.0)
 * @param int $id The Task ID
 * @return (obj) The Task
 */
function web_apper_get_task( $id ) {
	$item = new WebApper\Task\Task( $id );
	return (object) $item->get_data();
}

/**
 * Get Results
 *
 * @since WebApper (1.0)
 * @return arr Associative array of Task objects
*/
function web_apper_task_get_results( $query ) {
	return $items = WebApper\Task\Task::get_results( $query );
}

/**
 * Queries the DB to get task by 'task_id'
 *
 * @param array $atts
 * @param string $orderBy
 * @since 1.0
 */
function web_apper_get_task_by_task_id( $task_id ) {
	global $wpdb;
	$item = web_apper_task_get_results(  "SELECT * FROM {$wpdb->prefix}web_apper_tasks WHERE task_id = '{$task_id}'" );
	return $item[0];	
}

/**
 * Calculate a taks Due timestamp
 *
 * @param string $due_type
 * @param string $due_in_number
 * @param string $due_in_unit
 * @param string $due_on_datetime
 * @return string $due The 'Due on' DateTime
 */
function web_apper_task_due( $due_type, $due_in_number, $due_in_unit, $due_on_datetime  ) {
	if ( $due_type == 'in' ) : // Get a due date for the task
		$due = strtotime( '+' . $due_in_number . ' ' . $due_in_unit, current_time('timestamp') );
	elseif ( $due_type == 'on' ) :
		$due = strtotime( str_replace('T', '', $due_on_datetime) );		echo $due;
	endif;
	return date('Y-m-d H:i:s', $due);
}