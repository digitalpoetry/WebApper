<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.

namespace WebApper\Notification;

class Notification extends \WebApper\Module\Item {

	/**
	 * The name of the module DB table
	 *
	 * @var string
	 */
	public $table = "web_apper_notifications";

	/** Static Methods ********************************************************/

	/**
	 * Fetches all the notifications in the database for a specific user.
	 *
	 * @global $wpdb
	 * @param integer $user_id User ID
	 * @param bool $is_new
	 * @return array Associative array
	 * @static
	 */
	static function get_all_for_user( $user_id, $is_new = false ) {
		global $wpdb;
		$is_new = ( $is_new === true ) ? ' AND is_new = 1 ' : '';
 		return $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}web_apper_notifications WHERE user_id = {$user_id}{$is_new}" );
	}

	/**
	 * Delete all the notifications for a user based on type.
	 *
	 * @global $wpdb
	 * @param integer $user_id
	 * @param string $type
	 * @static
	 */
	function delete_for_user_by_type( $user_id, $type ) {
		global $wpdb;
 		return $this->get_results( "DELETE FROM {$wpdb->prefix}{$this->table} WHERE user_id = {$user_id} AND type = {$type}" );
	}

	/**
	 * Delete notification for a user based on item ID.
	 *
	 * @global $wpdb
	 * @param integer $user_id
	 * @param string $type
	 * @param integer $item_id
	 * @static
	 */
	function delete_for_user_by_item_id( $user_id, $type, $item_id ) {
		global $wpdb;
 		return $this->get_results( "DELETE FROM {$wpdb->prefix}{$this->table} WHERE user_id = {$user_id} AND type = {$type} AND item_id = {$item_id}" );
	}

}