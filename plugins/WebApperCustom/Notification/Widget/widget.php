<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.

class webapperNotificationWidget extends WP_Widget {

	function webapperNotificationWidget() {
		// Instantiate the parent object
		parent::__construct( false, 'WebApper Notifications' );
	}

	function widget( $args, $instance ) {
		require_once dirname( dirname( __FILE__ ) ) . '/class.php';
		require_once dirname( dirname( __FILE__ ) ) . '/api.php';
		
		global $current_user;

		$notifications = web_apper_get_notifications_for_user( $current_user->ID );

		if ( empty($notifications) ) {
			$notification_list .= '<li>No notifications</li>';
		} else {
			foreach ( $notifications as $notification ) :
				$notification_list .= '<li><a href="' . $notification->url . '">' . $notification->short_message . '</a></li>';
			endforeach;
		}

		//$menu_items .= '<li><a href="' . wp_logout_url( get_permalink() ) . '" title="Logout">Logout</a></li>';
		//if( has_filter('bootstrapper_user_quick_menu_items') ) {
		//	$menu_items = apply_filters( 'bootstrapper_user_quick_menu_items', $menu_items );
		//}
		
		echo '<h2 class="widget-title">Notifications</h2>';
		echo '<ul>' . $notification_list . '</ul>';
	}

	function form( $instance ) {
		echo '<p>This widget has no options.</p>';
	}
}

function webapper_notification_widget() {
	register_widget( 'webapperNotificationWidget' );
}

add_action( 'widgets_init', 'webapper_notification_widget' );