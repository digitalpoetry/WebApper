<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.


/**
 * Add a Notification
 * 
 * @since WebApper (1.0)
 * @param arr $notificationData The notification details
 * @return mixed (int) insert ID on success, (bool) false on fail
 */
function web_apper_insert_notification( $notificationData ) {
	
	$notificationData = apply_filters( 'notification_pre_insert', $notificationData ); // Allow filtering of the notification data before saving the notification;

	$notification = new WebApper\Notification\Notification;

	foreach ( $notificationData as $key => $val ) :
		$notification->$key = $val;
	endforeach;

	$notification->created_on  = current_time('mysql');
	$notification->notified_on = ( $notification->is_new == true ) ? NULL : current_time('mysql');
	
	/**
	 * Send an email for this notification
	 */
	$user = get_userdata( $notificationData['user_ID'] );
	$headers = 'From: No reply <noreply@' . $_SERVER['HTTP_HOST'] . '>' . "\r\n";
	$subject = 'You have a notification from ' . $_SERVER['HTTP_HOST'];
	$message = 'You have a notification from ' . $_SERVER['HTTP_HOST'] . ".\r\n" . $notificationData['long_message'] . "\r\n<a href=" . $notificationData['url'] . '>View</a>';
	mail( $user->user_email, $subject, $message, $headers );

	if ( $notification->save() ) :
		do_action( 'notification_post_insert', $notification ); // Allow notification data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Get notifications for a specific user
 *
 * @since WebApper (1.0)
 * @param int $user_id
 * @return boolean Object or array on success, false on fail
 */
function web_apper_get_notifications_for_user( $user_id, $is_new ) {
	return $notifications = WebApper\Notification\Notification::get_all_for_user( $user_id );
}

/**
 * Delete notifications for an ID
 *
 * @since WebApper (1.0)
 * @param int $ID
 * @return boolean True on success, false on fail
 */
function web_apper_delete_notification_by_id( $ID ) {
	return WebApper\Notification\Notification::delete_by_id( $ID );
}

/**
 * Delete notifications for an item ID
 *
 * Used when clearing out notifications for a specific component when the user
 * has visited that item.
 *
 * @since WebApper (1.0)
 * @param int $user_id
 * @param string $type
 * @param int $item_id
 * @return boolean True on success, false on fail
 */
function web_apper_delete_notifications_by_item_id( $user_id, $type, $item_id ) {
	return WebApper\Notification\Notification::delete_for_user_by_item_id( $user_id, $type, $item_id );
}

/**
 * Delete notifications for a user by type
 *
 * Used when clearing out notifications for a specific component when the user
 * has visited that component.
 *
 * @since WebApper (1.0)
 * @param int $user_id
 * @param string $type
 * @return boolean True on success, false on fail
 */
function web_apper_delete_notifications_by_type( $user_id, $type ) {
	return WebApper\Notification\Notification::delete_for_user_by_type( $user_id, $type );
}
