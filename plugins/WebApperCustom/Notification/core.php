<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.

/*
WebApper Moddule Name: Notifications
*/

namespace WebApper\Notification;

// Include Module files
require_once 'class.php';
require_once 'api.php';
foreach( glob ( dirname(__FILE__) . '/Widget/*.php' ) as $filename ) {
	require_once( $filename );
}

// Hook into plugin activation to install Module DB table
add_filter( 'web_apper_create_table_sql', function ( $sql_array ) {
	global $wpdb;
	$sql_array[] = "CREATE TABLE {$wpdb->prefix}web_apper_notifications (
		ID BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
		user_ID BIGINT(20) UNSIGNED NOT NULL,
		item_ID BIGINT(20) UNSIGNED DEFAULT '0' NOT NULL,
		type VARCHAR(55) NOT NULL,
		short_message VARCHAR(100) NOT NULL,
		long_message VARCHAR(255) NOT NULL,
		url VARCHAR(255) NOT NULL,
		is_new BOOL DEFAULT '1' NOT NULL,
		notified_on DATETIME DEFAULT NULL,
		created_on DATETIME NOT NULL,
		INDEX (user_ID),
		PRIMARY KEY (ID)
	);";
	return $sql_array;
}, 1, 1 );