<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.

/*
WebApper Moddule Name: Entries
*/

namespace WebApper\Entry;

// Include Module files
require_once 'class.php';
require_once 'api.php';
foreach( glob ( dirname(__FILE__) . '/Shortcode/*.php' ) as $filename ) {
	require_once( $filename );
}

// Hook into plugin activation to install Module DB table
function create_table_sql( $sql_array ) {
	global $wpdb;
	
	$sql_array[] = "CREATE TABLE {$wpdb->prefix}web_apper_entrys (
		ID SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
		entry_type VARCHAR(55) NOT NULL,
		entry_content VARCHAR(255) NOT NULL,
		entry_post_id SMALLINT UNSIGNED NOT NULL,
		entry_user_id SMALLINT(6) UNSIGNED,
		entry_created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		PRIMARY KEY (ID)
	);";
	
	// TODO: Add default Fields for this module here
	
	return $sql_array;
}
add_filter( 'web_apper_create_table_sql', 'WebApper\Entry\create_table_sql', 1, 1 );