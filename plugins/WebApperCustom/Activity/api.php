<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.


/**
 * Add Entry
 * 
 * @since WebApper (1.0)
 * @param arr $itemData The Entry details
 * @return mixed (int) insert ID on success, (bool) false on fail
 */
function web_apper_insert_entry( $itemData ) {
	global $current_user;
	
	$itemData = apply_filters( 'entry_pre_insert', $itemData ); // Allow filtering of the Entry data before saving;

	$itemData['entry_created_on'] = current_time('mysql');
	$itemData['entry_user_id'] = $current_user->ID;

	$item = new WebApper\Entry\Entry;

	foreach ( $itemData as $key => $val ) :
		$item->$key = $val;
	endforeach;

	if ( $item->save() ) :
		do_action( 'entry_post_insert', $item ); // Allow Entry data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Delete Entry
 *
 * @since WebApper (1.0)
 * @param int $id The Entry ID
 * @return bool true on success, false on failure
 */
function web_apper_delete_entry( $id ) {
	
	do_action( 'entry_pre_delete', $id ); // Allow Entry data to be hooked onto
	
	$item = new WebApper\Entry\Entry($id);
	
	if ( $item->delete() ) :
		do_action( 'entry_post_delete', $item ); // Allow Entry data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Get Entry
 * 
 * @since WebApper (1.0)
 * @param int $id The Entry ID
 * @return (obj) The Entry
 */
function web_apper_get_entry( $id ) {
	$item = new WebApper\Entry\Entry( $id );
	return (object) $item->get_data();
}

/**
 * Get Results
 *
 * @since WebApper (1.0)
 * @return arr Associative array of Entry objects
*/
function web_apper_entry_get_results( $query ) {
	return $items = WebApper\Entry\Entry::get_results( $query );
}