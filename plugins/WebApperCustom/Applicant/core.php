<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.

/**
 * WebApper Module: Applicant
 * 
 * The Applicant module extends Records, adding the ability to associate Records
 * to one another. The Records are associated as Co-Applicants, with one of the
 * records being a Primary Applicant. Requires the 
 * {@link http://codex.webapper.info WebApperRecords} plugin to be installed, 
 * activated and configured.
 * 
 * @todo Update codex.webapper.info URL in above DocBlock desciption.
 *
 * @since 0.1.2
 *
 * @package WebApper
 * @subpackage Applicant
 */

namespace WebApper\Record;

// Include Module files
require_once 'api.php';
foreach( glob ( dirname(__FILE__) . '/Shortcode/*.php' ) as $filename ) {
	require_once( $filename );
}


// Add all hooks at WordPress init
add_filter( 'init', function () {
	
	// Hook into plugin activation to install Module DB table
	add_action( 'web_apper_insert_table_rows', function () {
		global $wpdb;
		
		// Deafult Fields
		$wpdb->query( "INSERT INTO `{$wpdb->prefix}web_apper_fields` (`field_name`, `field_id`, `field_index_only`, `field_form_only`, `field_field_only`, `field_type`, `field_attributes`, `field_required`, `field_placeholder`, `field_short_desc`, `field_long_desc`, `field_options`, `field_default_value`, `field_validation`, `field_regex`, `field_error_message`, `field_dt_show_col_default`, `field_dt_format_value`, `field_dt_filter_type`, `field_dt_filter_options`, `field_dt_filter_value`, `field_bulk_edit`, `field_read_only`, `field_dynamic_value_flow_id`, `field_action_flow_id`, `field_form_flow_id`, `core_item`) VALUES
			('Primary Applicant','pri_applicant_id',0,1,1,'Hidden',1,'','','','','','','','','',1,'','','','',0,0,'','','',1),
		;" );

	}, 1, 0 );

}, 1, 0 );