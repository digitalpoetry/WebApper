<?php
/**
 * Applicants Shortcode
 *
 * This file is a shortcode displaying a widget for viewing & managing a Record's
 * Co-Applicants. Requires the {@link http://codex.webapper.info WebApperRecords}
 * plugin to be installed, activated and configured.
 *
 * @todo Update codex.webapper.info URL in above DocBlock desciption.
 *
 * @since 0.1.2
 *
 * @package WebApper
 * @subpackage Applicant
 */

namespace WebApper\Applicant;

/**
 * Applicants Shortcode
 *
 * This shortcode displays small applet for viewing & managing a record's 
 * Co-Applicants. Must be configured for use inconjunction a Form Shortcode. 
 * Requires the Records Modules. This Class should not be directly initiated, 
 * use the [applicants] shortcode instead. 
 *
 * Basic usage: <code>[applicants fscid='form_id' include='full-name']</code>
 *
 * @since 0.1.2
 *
 * @uses \WebApper\Shortcode
 */
class Applicant extends \WebApper\Shortcode {
	
	/**
	 * Shortcode tag.
	 *
	 * @since 0.1.2
	 * @access protected
	 * @var string $shortcode The name of the shortcode tag.
	 */
	protected $shortcode = 'applicants';
	
	/**
	 * Shortcode default attributes.
	 *
	 * @since 0.1.2
	 * @access protected
	 * @var array $defaults Associative array of shortcode default attributes.
	 */
	protected $defaults = array(
		'id' => 'applicants',
		'fscid' => null,		   // TODO: Document this attr
		'include' => null,		   // TODO: Document this attr
		'viewcap' => 'edit_posts', // The minimum user capability required to view the Form. OPTIONS: 'logged_out', 'read', 'edit_posts', 'edit_others_posts', 'edit_users', 'edit_pages', 'edit_plugins'. Default 'logged_out'.
	);

	/**
	 * Displays shortcode
	 *
	 * The shortcode callback. Handles the display of shortcode content. See the
	 * {@link http://codex.wordpress.org/Shortcode_API WordPress Shortcode API}
	 * for more information about how shortcodes work.
	 *
	 * @since 0.1.2
	 *
	 * @see  \WebApper\Shortcode The shortcode base class.
	 * @uses \WebApper\Applicant\Applicant::has_req_attrs() .
	 * @uses \WebApper\base::current_user_has_cap() .
	 * @uses \web_apper_get_co_applicant_ids() Get All Co-Applicant IDs for a given record.
	 *
	 * @param  array  $atts Associative array of the shortcode attributes.
	 * @return string The shortcode HTML.
	 */
    public function shortcode( $atts ) {

		// Allow filtering of shortcode attributes before rendering
		$atts = apply_filters( $this->shortcode . '_atts', $atts, $id );
		
		// Extract shortcode attributes into individual vars and also store as an array 
		extract( $atts = shortcode_atts( $this->defaults, $atts ) );
		
		// Check for required shortcode attributes
		$msg = $this->has_req_attrs( $atts );
		if ( $msg !== true )
			return $msg;
			
        // Check if current user has proper privileges to view
		if ( !$this->current_user_has_cap($viewcap) ) :
			echo 'You do not have sufficient permissions to access this applet.';
			return;
		endif;
		
        // Check if a record ID was passed
		if ( !empty( $_GET['postID'] ) ) :
			$itemID = $_GET['postID'];  // Set the post ID
			$item = get_post( $itemID );
		endif;

		// Check for Primary Applicant ID
		if ( !empty( $_GET['primaryID'] ) ) :
			$primaryID = $_GET['primaryID'];
		else :
			$primaryID = get_post_meta( $itemID, 'applicant_primary_id', true );
		endif;
					
		// Build Primary Applicant link
		if ( !empty($primaryID) ) : // If Primary Applicant ID found
			// TODO: Change below 8 lines to func call for web_apper_equaltion_eval()
			$label_fields = array();
			foreach ( explode(',', $atts['include']) as $custom_field ) :
				$label_fields[] = get_post_meta( $primaryID, $custom_field, true );
			endforeach;
			if ( empty($label_fields) ) :
				$label_fields[] = get_post_type( $primaryID );
			endif;
			$href = get_permalink() . '?postID=' . $primaryID;
			$primary_applicant_link = '<a href="' . $href . '">' . implode(' ', $label_fields) . '</a>';
		endif;

		// Build the shortcode output hrml string
		if ( isset($itemID) ) : // If current Applicant exists

			// Display Applicants Information
			if ( !empty($primaryID) ) : // If Primary Applicant ID found
				
				// Display Primary Applicant
				echo '<strong>Primary Applicant:</strong> ';
				if ( $itemID == $primaryID ) : // If Applicant is Primary Applicant
					
					echo '<em>yes</em><br />'; // Output Primary Applicant default
					
				else : // Else Applicant is not Primary Applicant

					// Output Primary Applicants HTML
					echo $primary_applicant_link;
					
				endif;
				echo '<br />';
				
				// Get Co-Applicant IDs
				$coIDs = web_apper_get_co_applicant_ids( $primaryID );
				
				// Display Co-Applicants
				echo '<strong>Co-Applicants:</strong> ';
				if ( empty($coIDs) ) : // If no Co-Applicant IDs found
					
					echo '<em>none</em><br />'; // Output Co-Applicants default
					
				else : // Else Co-Applicant IDs found
					
					// Loop thru each Co-Applicant
					$coLinks = array();
					foreach ( $coIDs as $coID ) :
						// If Co-Applicant is not current Applicant
						if ( $coID != $itemID ) :
							// TODO: Change below 8 lines to func call for web_apper_equaltion_eval()
							// Loop thru each link label Field (Again)
							$label_fields = array();
							foreach ( explode(',', $atts['include']) as $custom_field ) :
								$label_fields[] = get_post_meta( $coID, $custom_field, true );
							endforeach;
							if ( empty($label_fields) ) :
								$label_fields[] = get_post_type( $coID );
							endif;
							$href = get_permalink() . '?postID=' . $coID;
							
							// Add the Co-Applicant link to the links array
							$coLinks[] = '<a href="' . $href . '">' . implode(' ', $label_fields) . '</a>';
						endif;
					endforeach;
					
					// Output Co-Applicants HTML
					echo implode(', ', $coLinks);
					
				endif;
				echo '<br />';
				
			else : // Else Primary Applicant ID not found
				
				// Output Primary Applicant & Co-Applicants default HTML
				echo '<strong>Primary Applicant:</strong> <em>yes</em><br />';
				echo '<strong>Co-Applicants:</strong> <em>none</em><br />';
				
			endif;
			
			?>
			<a id="createCoApplicant" href="#">Create Co Applicant</a>
			
			<script type="text/javascript">
				jQuery(document).ready(function($) {
					/**
					 * Create Co-Applicant button click handler.
					 */
					$('#createCoApplicant').live("click", function() {
						// Search for Applicant ID query parameter in current url
						var postIDStr = window.location.search.match(/postID\=\d{1,}/g);
						// Get the Applicant ID
						var primaryID = postIDStr[0].match(/\d{1,}/g);
						// Build create Co-Applicant url
						var redirectURL = window.location.pathname + '?primaryID=' + primaryID[0];
						// Redirect to create Co-Applicant url
						window.location = redirectURL;
					});
				});
			</script>
			<?php
			
		else : // Else Applicant does not exist

			// Display Applicants Information
			if ( !empty($primaryID) ) : // If Primary Applicant ID found
				echo '<strong>Primary Applicant:</strong> ';
				echo $primary_applicant_link;
			else :
				echo 'You must save this record first...';
			endif;

			?>
			<script type="text/javascript">
				jQuery(document).ready(function($) {
					/**
					 * Check for Co-Applicant creation.
					 * Search for Co-Applicant ID query parameter in current url.
					 */
					var primaryIDStr = window.location.search.match(/primaryID\=\d{1,}/g);
					// If Co-Applicant ID query parameter found
					if ( primaryIDStr != null && 10 < primaryIDStr[0].length ) {
						// Get the Co-Applicant ID
						var primaryID = primaryIDStr[0].match(/\d{1,}/g);
						// Build Co-Applicant hidden input string
						var inputHTML = '<input type="hidden" name="applicant_primary_id" value="' + primaryID[0] + '" id="applicant_primary_id">';
						// Prepend Co-Applicant hidden input to Form
						$('form#<?php echo $atts['fscid']; ?> fieldset').prepend( inputHTML );
						// Get the web_apper_include input value
						var includeStr = $('form#<?php echo $atts['fscid']; ?> [name="web_apper_include"]').val();
						// Build new value
						includeStr = 'applicant_primary_id,' +  includeStr;
						// Update the web_apper_include input value
						$('form#<?php echo $atts['fscid']; ?> [name="web_apper_include"]').val( includeStr );
					}
				});
			</script>
			<?php
			
		endif;
	}

	/**
     * Check shortcode attributes.
	 *
     * Checks if a shortcode has the required attributes.
	 *
	 * @since 0.1.0
	 *
	 * @param  array   $atts Associative array of the shortcode attributes.
	 * @return boolean Returns true if the shortcode has the required
	 *                 attributes, false if not.
	 */
	protected function has_req_attrs( $atts ) {
		
		// Check for required shortcode attributes
		if ( $atts['id'] === null ) :
			return 'You must give the shortcode a unique ID, i.e., [applicants id=\'form-1\']';
		elseif ( $atts['include'] === null ) :
			return 'You must give the shortcode 1 or more Fields for an Applicant link label, i.e., [applicants include=\'first-name,last-name\']';
		elseif ( $atts['fscid'] === null ) :
			return 'You must give the shortcode a fscid, i.e., [applicants fscid=\'form-1\']';
		endif;
		return true;
	}

}


$initialize = new Applicant(); 

?>