<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.

/**
 * Applicant API
 *
 * API functions for easy access to the Applicant module. These functions may be
 * hooked onto using WordPress Actions/Filters. See the
 * {@link http://codex.wordpress.org/Plugin_API/ WordPress Hooks API} for more
 * information. Requires the {@link http://codex.webapper.info WebApperRecords}
 * plugin to be installed, activated and configured.
 * 
 * @todo Update codex.webapper.info URL in above DocBlock desciption.
 *
 * @link http://codex.wordpress.org/Function_Reference/add_action WordPress
 *       function reference: add_action()
 * @link http://codex.wordpress.org/Function_Reference/add_filter WordPress
 *       function reference: add_filter()
 * @since 0.1.2
 *
 * @package WebApper
 * @subpackage Applicant
 */


/**
 * Set Applicant fields callback.
 *
 * Hooks onto *record_post_insert* in 
 * {@link \web_apper_insert_record() web_apper_insert_record() } from the Record
 * API. Whenever a new Record is created with a Co-Applicant, will set the 
 * *Primary Applicant ID* of the Co-Applicant. Will also set the
 * *Primary Applicant ID* of the Primary Applicant if not yet set.
 * 
 * @todo Check inline link in above DocBlock desciption.
 *
 * @since 0.1.2
 *
 * @param  integer $itemID The record ID of the Applicant being added.
 * @param  array   $itemData Associative array containing the Custom Fields of
 *                 the Applicant being added.
 * @return void	   This function does not return a value.
 */
function web_apper_insert_co_applicant( $itemID, $itemData ) {

	// If Co-Applicant ID is set
	if ( isset($itemData['applicant_primary_id']) ) :
		
		// Get Co-Applicant ID
		$coID = $itemData['applicant_primary_id'];
		
		// If Co-Applicant ID is found
		if ( !empty($coID) ) :
			
			// Get the Primary Applicant ID of the Co-Applicant
			$tempID = get_post_meta( $coID, 'applicant_primary_id', true );
			
			// If Co-Applicant has Primary Applicant
			if ( !empty($tempID) ) :
				
				// Set Primary Applicant ID
				$primaryID = $tempID;
				
			else : // Else Co-Applicant is new Primary Applicant
			
				// Update Primary Applicant ID of Co-Applicant to own ID
				update_post_meta( $coID, 'applicant_primary_id', $coID );
				
				// Set Primary Applicant ID
				$primaryID = $coID;
				
			endif;
			
			// Update Primary Applicant ID of Applicant 
			update_post_meta( $itemID, 'applicant_primary_id', $primaryID );
			
		endif;
		
	endif;
}
/** This action is documented in /WebApperRecords/Record/api.php */
add_action( 'record_post_insert', 'web_apper_insert_co_applicant', 1, 2 );


/**
 * Get Co-Applicant IDs.
 *
 * Get All Co-Applicant IDs for a given record.
 *
 * @since 0.1.2
 *
 * @link http://codex.wordpress.org/Template_Tags/get_posts WordPress
 *       function reference: get_posts()
 * @link http://codex.wordpress.org/Class_Reference/WP_Query WordPress
 *       class reference: WP Query
 *
 * @param  integer $primaryID The record ID to return Co-Applicants of.
 * @return array   Returns an array of Co-Applicant IDs or an empty array if no 
 *                 Co-Applicant are found.
 */
function web_apper_get_co_applicant_ids( $primaryID ) {

	// Get Co-Applicants using custom meta query
	$items = get_posts( array( 'meta_key' => 'applicant_primary_id', 'meta_value' => $primaryID ) );
	
	// Build Co-Applicant IDs return array
	$coIDs = array();
	if ( !empty($items) ) : // If Co-Applicants were found
		foreach ( $items as $item ) : // Loop thru each Co-Applicant
			$coIDs[] = $item->ID; // Adding Co-Applicant ID to return array
		endforeach;
	endif;
	
	// Retrun Co-Applicant IDs
	return $coIDs;
}

?>