<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.

/**
 * WebApper Custom Modules Plugin
 *
 * The WebApper Custom Modules main plugin file.
 *
 * @since 0.1.0
 *
 * @package WebApper
 */

/*
Plugin Name: WebApper Custom Modules
Description: This plugin contains custom modules for WebApper
Author: Jesse LaReaux
Version: 0.1.0
Author URI: http://www.facebook.com/jesse.lareaux
*/

namespace WebApper;


// Include WebApper
require_once dirname( dirname( __FILE__ ) ) . '/WebApper/WebApper.php';

// Include plugin files
require_once dirname( __FILE__ ) . '/Activity/core.php';
require_once dirname( __FILE__ ) . '/Notification/core.php';
require_once dirname( __FILE__ ) . '/Task/core.php';
require_once dirname( __FILE__ ) . '/Applicant/core.php';