<?php
/*
Plugin Name: WebApper Custom Modules
Description: This plugin contains custom modules for WebApper
Author: Jesse LaReaux
Version: 1.0
Author URI: http://www.facebook.com/jesse.lareaux
*/

namespace WebApper;


// Include plugin files
require_once '/Task/core.php';
require_once '/Notification/core.php';