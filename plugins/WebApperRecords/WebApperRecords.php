<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.

/*
Plugin Name: WebApper Records
Description: This plugin contains custom modules for WebApper
Author: Jesse LaReaux
Version: 0.1.0
Author URI: http://www.facebook.com/jesse.lareaux
*/

namespace WebApper;


// Include WebApper
require_once dirname( dirname( __FILE__ ) ) . '/WebApper/WebApper.php';

// Include plugin files
require_once dirname( __FILE__ ) . '/Posttype/core.php';
require_once dirname( __FILE__ ) . '/Record/core.php';
