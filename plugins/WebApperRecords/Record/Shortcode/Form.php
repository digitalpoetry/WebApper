<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.

namespace WebApper\Record;

/*
 * [form id='Form']
 *
 */
class Form extends \WebApper\Shortcode {
	
    /**
     * Define shortcode properties
     *
     */
	protected $shortcode = 'form';
	protected $defaults = array(
		'id' => null,					// The unique ID to use for the shortcode. Default: none. Required.
		'viewcap' => 'edit_posts',		// The minimum user capability required to view the Form. OPTIONS: 'logged_out', 'read', 'edit_posts', 'edit_others_posts', 'edit_users', 'edit_pages', 'edit_plugins'. Default 'logged_out'.
		'posttype' => null,				// The cutom post type (ID) to display. Default: none. Required.
		'fieldset' => null,				// The set of fields to include in the indextable. Default: none. Required if 'include' is not specified. You may use one or both.
		'include' => null,				// field-id's of individual fields to include in the indextable. Default: none. Required if 'fieldset' is not specified. You may use one or both.
		'exclude' => null,				// field-id's of individual fields to exclude in the indextable. Default: none
		//'titlefield' => null,			// The field to use for the post title. Default: none. Required.
		'view' => 'SideBySide',			// The styles used to display the form. OPTIONS: 'SideBySide', 'Vertical', 'Inline', 'Search', 'Modal'. Default: 'SideBySide'.
		'placeholders' => 'false',		// Whether to use field labels as placeholders. OPTIONS: true, false.
		'submitlabel' => 'Submit',		// The label to use for the submit button. Default: 'Submit'.
		'ajax' => 'true',				// Whether to submit the form via ajax. OPTIONS: true, false.
		'modalbtn' => null,				// The label to use for the trigger modal button. Default: none. Required if 'view=Modal'.
		'modalheading' => null,			// The heading to use for the modal. Default: none. Required if 'view=Modal'.
		'novalidate' => 'false',		// Whether to add a 'novalidate' attribute to the form. OPTIONS: true, false. Default: false.
	);
	protected $ajax_nopriv = true; // allow ajax when logged out

    /**
     * Handles the add post shortcode
     *
     * @param array $atts
     */
    public function shortcode( $atts ) {
    	
		// Allow filtering of shortcode attributes before rendering
		$atts = apply_filters( $this->shortcode . '_atts', $atts, $id );

		// Extract shortcode attributes into individual vars and also store as an array 
		extract( $atts = shortcode_atts( $this->defaults, $atts ) );

		// Check for required shortcode attributes
		$msg = $this->has_req_attrs( $atts );
		if ( $msg !== true )
			return $msg;

        // Check if current user has proper privileges to view
		if ( !$this->current_user_has_cap($viewcap) ) :
			echo 'You do not have sufficient permissions to access this content.';
			return;
		endif;

        // Check if a record ID was passed
		global $post;
		$pageID = $post->ID;
		if ( !empty( $_GET['postID'] ) ) :
			// Get the single post
			$itemID = $_GET['postID'];  // Set the post ID
		endif;

		// Check for $_POST
		if ( isset( $_POST['web_apper_form'] ) && $ajax == false ) :
			if ( \PFBC\Form::isValid($_POST['web_apper_form']) ) :
				$this->add_record(); // If the form's submitted data validates, proceed.
			endif;
		endif;

		// Get the fields
		$this->fields = web_apper_get_fields( $atts['include'] );
			
		// Configure form settings
		$this->config_form( $atts );

		// Build the form
		if ( isset($itemID) ) :
			$item = web_apper_get_record( $itemID );
			$web_apper_action = 'update_record';
			$submit_label = 'Update';
			$submit_label_loading = 'Updating...';
		else : // Else add a new row
			$web_apper_action = 'add_record';
			$submit_label = 'Save';
			$submit_label_loading = 'Saving...';
		endif;

		$form = new \PFBC\Form($id);
		$form->configure( $this->formConfig );
		$form->addElement(new \PFBC\Element\Hidden('web_apper_form', $id));
		if ( $ajax == 'true' )
			$form->addElement(new \PFBC\Element\Hidden('action', 'web_apper' . $this->shortcode));
		$form->addElement( new \PFBC\Element\Hidden( 'web_apper_nonce', wp_create_nonce( 'AwesomeSauce!87' ) ) );
		$form->addElement( new \PFBC\Element\Hidden( 'web_apper_include', $atts['include'] ) );
		$form->addElement( new \PFBC\Element\Hidden( 'web_apper_item_id', $itemID ) );
		if ( $itemID ) :
			$form->addElement(new \PFBC\Element\Hidden('web_apper_action', 'update_record'));
		else :
			$form->addElement(new \PFBC\Element\Hidden('web_apper_action', 'add_record'));
		endif;
		// Form Modal body
		foreach ( $this->fields as $field ) :
			$field['field_value'] = stripslashes( $item->$field['field_id'] );
			$this->add_form_field( $field, $form );
		endforeach;
		
		// Form Modal footer
		$form->addElement(new \PFBC\Element\Button($submit_label, 'submit', array(
			'id' => 'submit',
			'data-loading-text' => $submit_label_loading
		)));
	    $form->render();  // Output the form

		// Add ajax script to footer
		if ( $ajax == 'true' || $view == 'Modal' ) :
			?>
			<script type="text/javascript">
				jQuery("form#<?php echo $id; ?>").bind('submit', function() {
					jQuery('.alert').alert('close');  // Close any alerts that may be open
				});
				function parseResponse<?php echo $id; ?>(response) {
					var result = jQuery.parseJSON(response);  // Parse response
					jQuery("form#<?php echo $id; if ( $view == 'Modal' ) echo ' .modal-body'; ?>").prepend(result.htmlalert);  // Insert into Modal
				}
			</script>
			<?php
		endif;

		do_action( $this->shortcode . '_html_js_end', $html_js_end, $id );
	}

    /**
     * Checks if a shortcode has required attributes
     *
     * @param array $atts
     * @since 1.0
     */
	protected function has_req_attrs( $atts ) {
		
		// Check for required shortcode attributes
		if ( $atts['id'] === null )
			return 'You must give the form a unique ID, i.e., [form id=\'form-1\']';
		if ( $atts['posttype'] === null )
			return 'You must specify a post type for the form, i.e., [form posttype=\'lead-pt\']';
		if ( $atts['include'] === null )
			return 'You must specify the \'include\' attribute so you have some fields the form, i.e., [form include=\'group_1\']';
		if ( $atts['view'] == 'Modal' && $atts['modalbtn'] == null )
			return 'You must specify the label to use for the trigger modal button, i.e., [form modalbtn=\'Apply Now\']';
		if ( $atts['view'] == 'Modal' && $atts['modalheading'] == null )
			return 'You must specify the heading to use for the modal, i.e., [form modalheading=\'1-Step Approval\']';
		return true;
	}

    /**
     * Configure the form obj settings
     *
     * @param array $atts
     * @since 1.0
     */
	public function config_form( $atts ) {
		
		$this->formConfig['prevent'] = array('bootstrap', 'jQuery', 'focus');  // Prevents scripts from loading twice
		
		switch ( $atts['view'] ) :
			case 'SideBySide' :
				$this->formConfig['view'] = new \PFBC\View\SideBySide;
				break;
			case 'Vertical' :
				$this->formConfig['view'] = new \PFBC\View\Vertical;
				break;
			case 'Inline' :
				$this->formConfig['view'] = new \PFBC\View\Inline;
				break;
			case 'Search' :
				$this->formConfig['view'] = new \PFBC\View\Search;
				break;
			case 'Modal' :
				$this->formConfig['view'] = new \PFBC\View\Modal;
				$this->formConfig['errorView'] = new \PFBC\ErrorView\Modal;
				break;
		endswitch;
		
		if ( $atts['placeholders'] == 'true' ) :
			$this->formConfig['labelToPlaceholder'] = 1;
		endif;
		
		if ( $atts['ajax'] == 'true' || $atts['view'] == 'Modal' ) :
			$this->formConfig['action'] = admin_url('admin-ajax.php');
			$this->formConfig['ajax'] = 1;
			$this->formConfig['ajaxCallback'] = 'parseResponse' . $atts['id'];
		else :
			$this->formConfig['action'] = $this->cur_page_url();
		endif;
		
		if ( $atts['novalidate'] == 'true' ) :
			$this->formConfig['novalidate'] = true;
		endif;
	}

    /**
     * Add a new record from form $_POST
     *
     * @since 1.0
     */
	public function add_record() {
		// Get the fields
		$this->fields = web_apper_get_fields( $_POST['web_apper_include'] );
		// Get the post data
		$itemData = $this->get_field_data_from_post();
		// Add new post
		$itemID = web_apper_insert_record( $itemData );
		// Send ajax response
		if ( $itemID ) :
			// TODO: Remove below action
			//do_action( $this->shortcode . '_insert', $itemID, $_POST  ); // Allow form post values to be hooked onto
			return json_encode( $this->send_response( 'Save successful!.', 'Hurray!', 'alert-success', true, null, $itemID ) );  // Send Response
		else :
		// Else post not inserted
			return json_encode( $this->send_response( 'There was a problem saving the record. Please Try again.', 'Oh snap!', 'alert-error', false ) );  // Send Response
		endif;
	}

    /**
     * Update a record
     *
     * @since 1.0
     */
	public function update_record() {
		// Get the fields
		$this->fields = web_apper_get_fields( $_POST['web_apper_include'] );
		// Get the Record ID
		$itemID = $_POST['web_apper_item_id'];
		// Get the post data
		$itemData = $this->get_field_data_from_post();
		// Update the post
		$result = web_apper_update_record( $itemID, $itemData );// Update post
		// If post was updated successfully
		if ( $result ) :
			// TODO: Remove below action
			//do_action( $this->shortcode . '_update', $itemID, $_POST  ); // Allow form post values to be hooked onto
			return json_encode( $this->send_response( 'Updated successful.', 'Hurray!', 'alert-success', true ) );  // Send Response
		else :
			return json_encode( $this->send_response( 'There was a problem updating the record. Please Try again.', 'Oh snap!', 'alert-error', false ) );  // Send Response
		endif;
	}

}

$initialize = new Form(); 

?>