<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.

namespace WebApper\Shortcode;

/*
 * RecordSingle
 *
 */
class single extends \WebApper\Shortcode
{
    /**
     * Define shortcode properties
     *
     */
    protected $shortcode = 'recordsingle';
    protected $defaults = array(
        'id' => null,                    // The unique ID to use for the shortcode. Default: none. Required.
        'viewcap' => 'edit_posts',    // The Required capability to view
        'addcap' => 'edit_posts',     // The Required capability to add
        'editcap' => 'edit_posts',    // The Required capability to edit
        'deletecap' => 'edit_posts', // The Required capability to delete
        'posttype' => null,                    // The cutom post type (ID) to display. Default: none. Required.
        'include' => null,                    // field-id's of individual fields to include in the singleview. Default: none. Required if 'fieldset' is not specified. You may use one or both.
    );

    /**
     * Handles the add post shortcode
     *
     * @param array $atts
     */
    public function shortcode($atts)
    {
        // Allow filtering of shortcode attributes before rendering
        $atts = apply_filters($this->shortcode.'_atts', $atts, $id);

        // Extract shortcode attributes into individual vars and also store as an array
        extract($atts = shortcode_atts($this->defaults, $atts));

        // Check for required shortcode attributes
        $msg = $this->has_req_attrs($atts);
        if ($msg !== true) {
            return $msg;
        }

        // Check if current user has proper privileges to view
        if (!$this->current_user_has_cap($viewcap)) :
            echo 'You do not have sufficient permissions to access this content.';

        return;
        endif;

        // Check if a record ID was passed
        global $post;
        $pageID = $post->ID;
        if (!empty($_GET['postID'])) :
            // Get the single post
            $itemID = $_GET['postID'];  // Set the post ID
        endif;

        // Get the fields
        $this->fields = web_apper_get_fields($atts['include']);

        // Ouput form & accordion HTML
        ?>

            <header class="page-header">
				<h1 class="page-title pull-left">
					<?php
                        if (empty($_GET['postID'])) :
                            echo 'Add Record'; else :
                            echo get_post_meta($itemID, 'first-name', true).' '.get_post_meta($itemID, 'last-name', true);
        endif;
        ?>
				</h1>

            	<?php if (!empty($_GET['postID'])) : ?>
                    <div class="pull-right">
						<p id="record_meta">
							<strong>Created:</strong> <?php echo date('F jS h:i A', strtotime($single_post->post_date));
        ?><br />
							<strong>Last Modified:</strong> <?php echo date('F jS h:i A', strtotime($single_post->post_modified));
        ?>
						</p>
                    </div>

				<div class="clearfix"></div>

            	<div class="pull-right">
					<form class="form-inline" method="post">
						<input type="hidden" name="web_apper_item_ids" value="<?php echo $_GET['postID'];
        ?>">
						<div class="input-append<?php if (empty($_GET['postID'])) {
    echo ' hide';
}
        ?>">
							<select id="record_type" name="web_apper_posttype" class="input-med">
								<option>change <?php echo $single_post->post_type;
        ?> to</option>
								<?php
                                $post_types  = get_option('web_apper_cpts', array());  // Get CPTs array from site options
                                foreach ($post_types as $post_type_id => $post_type) :
                                    if ($single_post->post_type != $post_type_id) :
                                        echo '<option value="'.$post_type_id.'">'.$post_type['name_singular'].'</option>';
        endif;
        endforeach;
        ?>
							</select>
							<button id="changeRecordType" type="submit" class="btn">Submit</button>
						</div>
					</form>
					<button id="createCoApp" type="button" class="btn<?php if (empty($_GET['postID'])) {
    echo ' hide';
}
        ?> pull-right">Create Co-applicant</button>
				</div>
                <?php endif;
        ?>

				<div class="clearfix"></div>

				<?php
                    if (get_post_meta($itemID, 'has_co_applicant', true) == 1) :
                        $is_primary_applicant = get_post_meta($itemID, 'is_primary_applicant', true);
        if ($is_primary_applicant == 1) :
                            $co_applicantIDs =  get_post_meta($itemID, 'co_applicantIDs', true); else :
                            $primary_applicantID = get_post_meta($itemID, 'primary_applicantID', true);
        $co_applicantIDs =  get_post_meta($primary_applicantID, 'co_applicantIDs', true);
        echo '<span><strong>Primary Applicant:</strong> <a href="'.get_permalink($pageID).'?postID='.$primary_applicantID.'" target="_blank">'.get_post_meta($primary_applicantID, 'first-name', true).' '.get_post_meta($primary_applicantID, 'last-name', true).'</a></span><br />';
        endif;
        if ($is_primary_applicant == 1 || count($co_applicantIDs) > 1) :
                            echo '<span><strong>Co-Applicants:</strong>';
        foreach ($co_applicantIDs as $coAppID) :
                                if ($itemID != $coAppID) :
                                    $coAppstr .= ' <a href="'.get_permalink($pageID).'?postID='.$coAppID.'" target="_blank">'.get_post_meta($coAppID, 'first-name', true).' '.get_post_meta($coAppID, 'last-name', true).'</a>,';
        endif;
        endforeach;
        echo substr($coAppstr, 0, -1).'</span>';
        endif;
        endif;
        ?>
			</header><!-- .page-header -->

			<div class="clearfix"></div>

			<?php
                // Set form values
                if (isset($itemID)) : // If rows are being edited
                    $itemID = $itemID;
        $item = web_apper_get_task($itemID); // Get the Record were calculating open seats for
                    $modal_heading = 'Edit Task';
        $bulk_edit = false;
        $web_apper_action = 'update_record';
        $submit_label = 'Update';
        $submit_label_loading = 'Updating...'; else : // Else add a new row
                    $web_apper_action = 'add_record';
        $modal_heading = 'Add Task';
        $submit_label = 'Save';
        $submit_label_loading = 'Saving...';
        endif;

                // Configure form settings
                $config['prevent'] = array('bootstrap', 'jQuery', 'focus');  // Prevents scripts from loading twice
                $config['view'] = new \PFBC\View\Collapsible2Col();
        $config['action'] = admin_url('admin-ajax.php');
        $config['ajax'] = 1;
        $config['ajaxCallback'] = 'parseResponse'.$id;
                // Build the form
                $this->config_form($id); // Set form settings
                $form = new \PFBC\Form($id);
        $form->configure($config); // Configure form settings
                $form->addElement(new \PFBC\Element\Hidden('web_apper_form', $id));
        $form->addElement(new \PFBC\Element\Hidden('action', 'web_apper'.$this->shortcode));
        $form->addElement(new \PFBC\Element\Hidden('web_apper_nonce', wp_create_nonce('AwesomeSauce!87')));
        $form->addElement(new \PFBC\Element\Hidden('web_apper_action', $web_apper_action));
        $form->addElement(new \PFBC\Element\Hidden('web_apper_include', $_POST['data']['include']));
        if (isset($itemID)) :
                    $form->addElement(new \PFBC\Element\Hidden('web_apper_item_ids', $itemID));
        endif;
        foreach ($this->fields as $field) :
                    $field['field_required'] = 0;
        $field['field_value'] = get_post_meta($itemID, $field['field_id'], true);
        $this->add_form_field($field, $form);
        endforeach;
        $form->render(); // Output the form
            ?>

            <script type="text/javascript">
				jQuery(document).ready(function($) {
					//  Show/Hide edit button on collaspe
					$('.collapse').on('show', function () {
						$('.accordion-heading input' , $(this).parent()).fadeIn(400);
					});
					$('.collapse').on('hide', function () {
						$('.accordion-heading input' , $(this).parent()).fadeOut(400);
					});

					$("form#<?php echo $id;
        ?>").bind('submit', function() {
						$('.alert').alert('close');  // Close any alerts that may be open
					});
				});


				function parseResponse<?php echo $id;
        ?>(response) {
					var result = jQuery.parseJSON(response);  // Parse response
					if ( result.action == 'add_record' ) {  // If we added a row
						window.location = window.location.host + window.location.pathname + '?postID=' + result.id
					}
					jQuery("form#<?php echo $id;
        ?>").prepend(result.htmlalert);  // Insert into Modal
				}
			</script>

			<?php
            do_action($this->shortcode.'_html_js_end', $html_js_end, $id);
    }

    /**
     * Checks if a shortcode has required attributes
     *
     * @param string $usercap
     * @since 1.0
     */
    protected function has_req_attrs($atts)
    {
        // Check for required shortcode attributes
        if ($atts['id'] == null) {
            return 'You must give the shortcode a unique ID, i.e., [shortcode id=\'form-1\']';
        }
        if ($atts['fieldset'] == null && $atts['include'] == null) {
            return 'You must specify the \'fieldset\' or \'include\' attribute so you have some fields the indextable, i.e., [indextable fieldset=\'group_1\']';
        }

        return true;
    }

    /**
     * Update a record from $_POST
     *
     * @since 1.0
     */
    public function add_record()
    {
        // Get the fields
        $this->fields = web_apper_get_fields($_POST['web_apper_include']);
        // Get the post data
        $itemData = $this->get_field_data_from_post();
        // Add new post
        $itemID = web_apper_insert_record($itemData);
        // Send ajax response
        if ($itemID) :
            web_apper_update_field_data($itemID);  // Save post meta
            return json_encode($this->send_response('Record saved.', 'Hurray!', 'alert-success', true, null, $itemID));  // Send Response
        else :
        // Else post not inserted
            return json_encode($this->send_response('There was a problem saving the record. Please Try again.', 'Oh snap!', 'alert-error', false));  // Send Response
        endif;
    }

    /**
     * Update a record from $_POST
     *
     * @since 1.0
     */
    public function update_record()
    {
        $itemID = $_POST['web_apper_item_ids'];
        // Update post
        $result = web_apper_update_record($itemID, $_POST);// Update post
        // Get the fields
        $this->fields = web_apper_get_fields($_POST['web_apper_include']);
        // Get the post data
        $itemData = $this->get_field_data_from_post();
        // Add new post
        $result = web_apper_update_record($itemID, $itemData);
        // Send ajax response
        if ($result) :
            return json_encode($this->send_response('Record updated.', 'Hurray!', 'alert-success', true, null, $itemID));  // Send Response
        else :
        // Else post not inserted
            return json_encode($this->send_response('There was a problem updating the record. Please Try again.', 'Oh snap!', 'alert-error', false));  // Send Response
        endif;
    }

    /**
     * Delete Action from the database
     *
     * @since 1.0
     */
    public function delete_record()
    {
        // Delete Actions
        foreach ($_POST['web_apper_item_ids'] as $itemID) :
            $result = web_apper_delete_record($itemID);
        if (!$result) :
                $errors[] = $itemID;
        endif;
        endforeach;
        // Send ajax response
        if (!empty($errors)) :
            return json_encode($this->send_response('The following Records were not deleted: '.implode(', ', $errors), 'Oh snap!', 'alert-error', false));  // Send Response
        else :
            return json_encode($this->send_response('Delete successful.', 'Hurray!', 'alert-success', true));  // Send Response
        endif;
    }

    /**
     * Change a record post type
     *
     * @since 1.0
     */
    public function change_record_type()
    {
        $id = $_POST['web_apper_id'];

        $itemID = $_POST['web_apper_item_ids'];

        // Set post type
        $result = set_post_type($itemID, $_POST['web_apper_posttype']);

        // If post was updated successfully
        if ($result) :
            return json_encode($this->sendResponse('Record has been changed to '.$_POST['web_apper_posttype'].'.', 'Hurray!', 'alert-success', true));  // Send Response
        else :
        // Else post not updated
            return json_encode($this->sendResponse('The record type was not changed. Please try again.', 'Oh snap!', 'alert-error', false));  // Send Response
        endif;
    }
}

$initialize = new RecordSingle();

?>
