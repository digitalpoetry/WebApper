<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.


namespace WebApper\Record;

/*
 * [record_index id='' posttype='' include='']
 *
 */
class RecordIndex extends \WebApper\IndexView {
	
    /**
     * Define shortcode properties
     *
     */
	protected $item_id = 'record';
	protected $item_label = 'Record';
	protected $shortcode = 'record_index';
	protected $defaults = array(
		'id' => null,
		'include' => null,  // The field_id's and/or fieldset_id's to be include in the indextable. Default: none. Required.
		'posttype' => null,		   // The cutom post type (ID) to display. Default: none. Required.
		'viewcap' => 'edit_posts',	// The Required capability to view
		'addcap' => 'edit_posts',	 // The Required capability to add
		'editcap' => 'edit_posts',	// The Required capability to edit
		'deletecap' => 'edit_posts', // The Required capability to delete
		'colvis_control' => true, // Enable the colVis button for the dataTable, true or false
		'form_controls' => true, // Enable the form controls for the dataTable, true or false
		'colfilter_controls' => true, // Enable the column filters for the dataTable, true or false
		'actions_control' => true, // 
		'adtl_actions' => 'div,div|View,View Record|div2,div|SelectAll,Select All|ResetFilters,Reset Filters', // Specify addition action buttons for the dataTable
		'row_selection' => true, // Enable dataTable row selection, true or false.
		'rightclick_menu' => true, // Enable rightclick menu on dataTable rows, true or false.
		'singleview_url' => null,
	);

    /**
     * Handles the shortcode
     *
     * @param array $atts
     */
    public function shortcode( $atts ) {
$notificationData = array(
	'user_ID' => 1,
	'type' => 'lead',	
	'short_message' => 'New lead assigned',	
	'long_message' => 'You have a new lead assigned to you.',	
	'url' => 'http://webapper.info/leads/',	
	'is_new' => 1,	
);
//web_apper_insert_notification( $notificationData );
web_apper_flow_actions_eval_field( 'dfghfgh', 'notify-lender-new-record' );

		// Allow filtering of shortcode attributes before rendering
		$atts = apply_filters( $this->shortcode . '_atts', $atts, $id );

		// Extract shortcode attributes into individual vars and also store as an array 
		extract( $atts = shortcode_atts( $this->defaults, $atts ) );

		// Check for required shortcode attributes
		$msg = $this->has_req_attrs( $atts );
		if ( $msg !== true )
			return $msg;

        // Check if current user has proper privileges to view
		if ( !$this->current_user_has_cap($viewcap) ) :
			echo 'You do not have sufficient permissions to access this content.';
			return;
		endif;

		// Get the fields
		$this->fields = web_apper_get_fields( $include );

		// Build the shortcode output hrml string
		?>

		<div class="row">
			<div class="col-sm-12 dt_controls">
				<?php
				if ( $actions_control ) :
					$adtl_actions = $this->textToArray( $adtl_actions );
					$this->build_dt_actions_control( $id, $atts, $adtl_actions ); // Echos the action buttons HTML for the dataTable
				endif;
				if ( $colvis_control ) :
					$this->build_dt_colvis_control( $id ); // Echos the colVis button HTML for the dataTable
				endif;
				if ( $form_controls ) :
					$this->build_dt_form_controls(); // Echos the form controls HTML for the dataTable
				endif;

				$this->build_dt_modal_wrapper( $id ); // Echos the Modal wrapper HTML
				?>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12 dt_table">
				<?php $this->build_dt( $atts, $colfilter_controls, array( 'include' => $include, 'posttype' => $posttype ) ); // Echos the dataTable HTML ?>
			</div>
		</div>

		<?php
		
		if ( $rightclick_menu ) :
			$this->build_dt_rightclick_menu( $id ); // Echos the browser context menu HTML
		endif;
		
		?>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				// Context Menu
			//	$('tbody tr', '<?php echo $id; ?>DataTable').live('mouseover', function() {
			//		$('#<?php echo $id; ?>ContextMenu #view').attr('href', $('tr.clicked', table).data('singleview') );
			//	});
				
				// 'View' button click handler
				$('.<?php echo $id; ?>View').live("click", function() {
					if ( $('tr.row_selected').length > 0 ) {
						
						<?php if ( !empty($singleview_url) ) : ?>
						
							var item_ids = []; 
							$('tr.row_selected').each(function() {
								item_ids.push( $(this).data('item-id') );
							});
							$.each( item_ids, function( i, val ) {
								window.open( '<?php echo $singleview_url; ?>?postID='+val,'_blank' );
							});
							
						<?php else : ?>
						
							var urls = []; 
							$('tr.row_selected').each(function() {
								urls.push( $(this).data('singleview') );
							});
							$.each( urls, function( i, val ) {
								window.open( val,'_blank' );
							});
							
						<?php endif; ?>
						
						
					} else {
						alert("Please select a row first!");
					}
					return false;
				});
			});
		</script>
		<?php
		
		$this->shortcode_js( $atts );
	}

    /**
     * Get Items from the database
     *
     * @since 0.1.0
     */
	protected function get_records() {
		$columns[] = array(
			'db' => 'ID', 'dt' => 'DT_RowData',
			'formatter' => function( $d, $row ) {
				global $wpdb;
				$post = get_post( $d );
				//$singleview = get_option( 'singleview_url_' . $post->post_type );
				$posttype = web_apper_posttype_get_results( "SELECT `posttype_singleview_url` FROM `{$wpdb->prefix}web_apper_posttypes` WHERE `posttype_slug` = '{$post->post_type}';" );
				return array(
					'item-id' => $d,
					'singleview' => $posttype[0]['posttype_singleview_url'] . '?postID=' . $d
				);
			}
		);
		// Get the fields
		$this->fields = web_apper_get_fields( $_POST['web_apper_include'] );
		foreach ( $this->fields as $field ) :
			if ( $field['field_form_only'] != true ) :
				$column['db'] = 'web_apper_' . str_replace( '-', '_', $field['field_id'] );
				$column['dt'] = $field['field_id'];
				if ( !empty( $field['field_dt_format_value'] ) ) :
					$column['formatter'] = $this->dt_format_value( $field['field_dt_format_value'] );
				else :
					$column['formatter'] = function( $d, $row ){
						if ( $this->isSerialized( $d ) ) :
							$d = unserialize( $d );
						endif;
						return $d;
					};
				endif;
				$columns[] = $column;
			endif;
		endforeach;
		require_once( dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) . '/WebApper/SSP.php' );
		global $wpdb;		
		echo json_encode(
			\SSP::simple( $_POST, $wpdb->prefix . 'web_apper_records', 'ID', $columns ) // $_GET, $sql_details, $table, $primaryKey, $columns
		);
	}

    /**
     * Echos a form for the dataTable
     *
     * @since 0.1.0
     */
	public function get_form() {
		// Set form values
		$id = $_POST['id']; // The shortcode ID
		if ( isset($_POST['web_apper_item_ids']) ) : // If rows are being edited
			if ( 1 < count($_POST['web_apper_item_ids']) ) : // If 1 rows is being edited
				$itemIDs = implode( ',' , $_POST['web_apper_item_ids'] );
				$modal_heading = 'Edit ' . $this->item_label . 's';
				$bulk_edit = true;
			else : // Else multiple rows are being edited
				$itemIDs = $_POST['web_apper_item_ids'][0];
				$web_apper_get_item = 'web_apper_get_' . $this->item_id;
				$item = $web_apper_get_item( $itemIDs );
				$modal_heading = 'Edit ' . $this->item_label;
				$bulk_edit = false;
			endif;
			$web_apper_action = 'update_record';
			$submit_label = 'Update';
			$submit_label_loading = 'Updating...';
		else : // Else add a new row
			$web_apper_action = 'add_record';
			$modal_heading = 'Add ' . $this->item_label;
			$submit_label = 'Save';
			$submit_label_loading = 'Saving...';
		endif;
		// Form Modal header
		$this->config_form( $id ); // Set form settings
		$form = new \PFBC\Form( $id );
		$form->configure( $this->formConfig ); // Configure form settings
		$form->addElement( new \PFBC\Element\Hidden( 'web_apper_form', $id ) );
		$form->addElement( new \PFBC\Element\Hidden( 'action', 'web_apper' . $this->shortcode ) );
		$form->addElement( new \PFBC\Element\Hidden( 'web_apper_nonce', wp_create_nonce( 'AwesomeSauce!87' ) ) );
		$form->addElement( new \PFBC\Element\Hidden( 'web_apper_action', $web_apper_action ) );
		$form->addElement( new \PFBC\Element\Hidden( 'web_apper_include', $_POST['data']['include'] ) );
		$form->addElement( new \PFBC\Element\Hidden( 'web_apper_posttype', $_POST['data']['posttype'] ) );
		if ( isset($itemIDs) ) :
			$form->addElement( new \PFBC\Element\Hidden( 'web_apper_item_ids', $itemIDs ) );
		endif;
		$form->addElement(new \PFBC\Element\ModalHeading($modal_heading));
		// Form Modal body
		$this->fields = web_apper_get_fields( $_POST['data']['include'] ); // Get the fields
		foreach ( $this->fields as $field ) :
			if ( $bulk_edit ) :
				if ( $field['field_bulk_edit'] == 1 ):
					$field['field_required'] = 0;
					$field['field_default_value'] = NULL;
					$this->add_form_field( $field, $form );
				endif;
			else:
				$field['field_value'] = stripslashes( $item->$field['field_id'] );
				$this->add_form_field( $field, $form );
			endif;
		endforeach;
		// Form Modal footer
		$form->addElement(new \PFBC\Element\Button($submit_label, 'submit', array(
			'id' => 'submit',
			'data-loading-text' => $submit_label_loading
		)));
		$form->render(); // Output the form
	}

    /**
     * Add Record to the database
     *
     * @since 1.0
     */
	public function add_record() {
		// Get the fields
		$this->fields = web_apper_get_fields( $_POST['web_apper_include'] );
		// Get the post data
		$itemData = $this->get_field_data_from_post();
		// Save the Record
		$postID = web_apper_insert_record( $itemData );
		// Send ajax response
		if ( $postID ) :
			return json_encode( $this->send_response( 'Record saved.', 'Hurray!', 'alert-success' ) );  // Send Response
		else :
			return json_encode( $this->send_response( 'There was a problem saving the Record. Please Try again.', 'Oh snap!', 'alert-error', false ) );  // Send Response
		endif;
	}

    /**
     * Update Record in the database
     *
     * @since 1.0
     */
	public function update_record() {
		// Get the fields
		$this->fields = web_apper_get_fields( $_POST['web_apper_include'] );
		// Get the post data
		$itemData = $this->get_field_data_from_post();
		// Save Records
		$errors = array();
		foreach( explode( ',', $_POST['web_apper_item_ids'] ) as $itemID ) : // Fields ids are a comma-delimited string here since they are coming from a PFBC form 
			$result = web_apper_update_record( $itemID, $itemData );
			if ( !$result ) : 
				$errors[] = $itemID;
			endif;
		endforeach;
		// Send ajax response
		if ( empty($errors) ) :
			return json_encode( $this->send_response( 'Update  successful.', 'Hurray!', 'alert-success', true ) );  // Send Response
		else :
			return json_encode( $this->send_response( 'The following Records were not updated: ' . implode(', ', $errors), 'Oh snap!', 'alert-error', false ) );  // Send Response
		endif;
	}

    /**
     * Delete Record from the database
     *
     * @since 1.0
     */
	public function delete_record() {
		// Delete Records
		foreach( $_POST['web_apper_item_ids'] as $itemID ) :
			$result = web_apper_delete_record( $itemID );
			if ( !$result ) : 
				$errors[] = $itemID;
			endif;
		endforeach;
		// Send ajax response
		if ( empty($errors) ) :
			return json_encode( $this->send_response( 'Delete successful.', 'Hurray!', 'alert-success', true ) );  // Send Response
		else :
			return json_encode( $this->send_response( 'The following Records were not deleted: ' . implode(', ', $errors), 'Oh snap!', 'alert-error', false ) );  // Send Response
		endif;
	}

}

$initialize = new RecordIndex(); 

?>