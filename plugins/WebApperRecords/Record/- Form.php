<?php
/*
 * [form id='Form'][form id=\"test\" include=\"user-tasks,user-tasks5\"]
 *
 */

namespace WebApper;

class Form extends Shortcode {
	
    /**
     * Define shortcode properties
     *
     */
	protected $shortcode = 'form';
	protected $defaults = array(
		'id' => null,					// The unique ID to use for the shortcode. Default: none. Required.
		'viewcap' => 'edit_posts',		// The minimum user capability required to view the Form. OPTIONS: 'logged_out', 'read', 'edit_posts', 'edit_others_posts', 'edit_users', 'edit_pages', 'edit_plugins'. Default 'logged_out'.
		'posttype' => null,				// The cutom post type (ID) to display. Default: none. Required.
		'fieldset' => null,				// The set of fields to include in the indextable. Default: none. Required if 'include' is not specified. You may use one or both.
		'include' => null,				// field-id's of individual fields to include in the indextable. Default: none. Required if 'fieldset' is not specified. You may use one or both.
		'exclude' => null,				// field-id's of individual fields to exclude in the indextable. Default: none
		//'titlefield' => null,			// The field to use for the post title. Default: none. Required.
		'view' => 'SideBySide',			// The styles used to display the form. OPTIONS: 'SideBySide', 'Vertical', 'Inline', 'Search', 'Modal'. Default: 'SideBySide'.
		'placeholders' => 'false',		// Whether to use field labels as placeholders. OPTIONS: true, false.
		'submitlabel' => 'Submit',		// The label to use for the submit button. Default: 'Submit'.
		'successsave' => 'Submission successful!',		// The message to display upon successful save.
		'failuresave' => 'Submission unsuccessful.',	// The message to display upon unsuccessful save.
		'ajax' => 'false',				// Whether to submit the form via ajax. OPTIONS: true, false.
		'modalbtn' => null,				// The label to use for the trigger modal button. Default: none. Required if 'view=Modal'.
		'modalheading' => null,			// The heading to use for the modal. Default: none. Required if 'view=Modal'.
		'novalidate' => 'false',		// Whether to add a 'novalidate' attribute to the form. OPTIONS: true, false. Default: false.
	);
	protected $ajax_nopriv = true; // allow ajax when logged out

    /**
     * Handles the add post shortcode
     *
     * @param array $atts
     */
    public function shortcode( $atts ) {

		// Allow filtering of shortcode attributes before rendering
			$atts = apply_filters( $this->shortcode . '_atts', $atts, $id );

		// Get shortcode attributes
			extract( $atts = shortcode_atts( $this->defaults, $atts ) );

		// Check for required shortcode attributes
			$msg = $this->has_req_attrs( $atts );
			if ( $msg !== true )
				return $msg;

        // Check if a record ID was passed
			if ( isset( $_GET['postID'] ) && !empty( $_GET['postID'] ) ) :
				$postID = $_GET['postID'];  // Set the post ID
			else :
				$postID = false;
			endif;

       // Check if current user has proper privileges
			if ( isset($_GET['access_key']) && isset($_GET['postID']) ) :
				if ( get_post_meta( $postID, 'access_key', true ) == $_GET['access_key'] ) :
					$access_key = true;
				else :
					return;
				endif;
			elseif ( !$this->user_has_cap( $viewcap ) ) :
				return;
			endif;

		// Check for $_POST
			if ( isset( $_POST['web_apper_form'] ) && $ajax == true ) :
				if ( \PFBC\Form::isValid($_POST['web_apper_form']) ) :
					$this->add_record(); // If the form's submitted data validates, proceed.
				endif;
			endif;

		// Get the fields
			$this->get_fields( $atts, true );
			
		// Configure form settings
			$this->config_form( $atts );  // Check if user has proper privileges

		// Build the form
			$form = new \PFBC\Form($id);
			$form->configure( $this->formConfig );
			$form->addElement(new \PFBC\Element\Hidden('web_apper_form', $id));
			if ( $postID ) :
				$form->addElement(new \PFBC\Element\Hidden('web_apper_action', 'update_record'));
			else :
				$form->addElement(new \PFBC\Element\Hidden('web_apper_action', 'add_record'));
			endif;
			$form->addElement(new \PFBC\Element\Hidden('web_apper_nonce', wp_create_nonce( 'WebApperAwesomeness!87' )));
			$form->addElement( new \PFBC\Element\Hidden( 'web_apper_post_id', $postID ) );
			if ( $ajax == 'true' || $view == 'Modal' )
				$form->addElement(new \PFBC\Element\Hidden('action', 'web_apper' . $this->shortcode));
			if ( $view == 'Modal' ) :
				$form->addElement(new \PFBC\Element\ModalToggle($modalbtn));
				$form->addElement(new \PFBC\Element\ModalHeading($modalheading));
			endif;
			foreach ( $this->fields as $field ) :
				$field['required'] = false;
				$this->add_form_field( $field, $form, $postID );
			endforeach;
			$form->addElement(new \PFBC\Element\Button($submitlabel));
            $form->render();  // Output the form

		// Add ajax script to footer
			if ( $ajax == 'true' || $view == 'Modal' ) :
				?>
				<script type="text/javascript">
					jQuery("form#<?php echo $id; ?>").bind('submit', function() {
						jQuery('.alert').alert('close');  // Close any alerts that may be open
					});
					function parseResponse<?php echo $id; ?>(response) {
						var result = jQuery.parseJSON(response);  // Parse response
						jQuery("form#<?php echo $id; if ( $view == 'Modal' ) echo ' .modal-body'; ?>").prepend(result.htmlalert);  // Insert into Modal
					}
				</script>
				<?php
			endif;

			do_action( $this->shortcode . '_html_js_end', $html_js_end, $id );
	}

    /**
     * Checks if a shortcode has required attributes
     *
     * @param array $atts
     * @since 1.0
     */
	protected function has_req_attrs( $atts ) {

		// Check for required shortcode attributes
			if ( $atts['id'] == null )
				return 'You must give the form a unique ID, i.e., [form id=\'form-1\']';
			if ( $atts['posttype'] == null )
				return 'You must specify a post type for the form, i.e., [form posttype=\'lead-pt\']';
			if ( $atts['fieldset'] == null && $atts['include'] == null )
				return 'You must specify the \'fieldset\' or \'include\' attribute so you have some fields the form, i.e., [form fieldset=\'group_1\']';
			if ( $atts['view'] == 'Modal' && $atts['modalbtn'] == null )
				return 'You must specify the label to use for the trigger modal button, i.e., [form modalbtn=\'Apply Now\']';
			if ( $atts['view'] == 'Modal' && $atts['modalheading'] == null )
				return 'You must specify the heading to use for the modal, i.e., [form modalheading=\'1-Step Approval\']';
			return true;
	}

    /**
     * Configure the form obj settings
     *
     * @param array $atts
     * @since 1.0
     */
	protected function config_form( $atts ) {
		$this->formConfig['prevent'] = array('bootstrap', 'jQuery', 'focus');  // Prevents scripts from loading twice
		switch ( $atts['view'] ) :
			case 'SideBySide' :
				$this->formConfig['view'] = new \PFBC\View\SideBySide;
				break;
			case 'Vertical' :
				$this->formConfig['view'] = new \PFBC\View\Vertical;
				break;
			case 'Inline' :
				$this->formConfig['view'] = new \PFBC\View\Inline;
				break;
			case 'Search' :
				$this->formConfig['view'] = new \PFBC\View\Search;
				break;
			case 'Modal' :
				$this->formConfig['view'] = new \PFBC\View\Modal;
				$this->formConfig['errorView'] = new \PFBC\ErrorView\Modal;
				break;
		endswitch;
		if ( $atts['placeholders'] == 'true' ) :
			$this->formConfig['labelToPlaceholder'] = 1;
		endif;
		if ( $atts['ajax'] == 'true' || $atts['view'] == 'Modal' ) :
			$this->formConfig['action'] = admin_url('admin-ajax.php');
			$this->formConfig['ajax'] = 1;
			$this->formConfig['ajaxCallback'] = 'parseResponse' . $atts['id'];
		else :
			$this->formConfig['action'] = $this->cur_page_url();
		endif;
		if ( $atts['novalidate'] == 'true' ) :
			#$this->formConfig['novalidate'] = '';
			$this->formConfig['novalidate'] = true;
		endif;
	}

    /**
     * Add a new record from form $_POST
     *
     * @since 1.0
     */
	public function add_record() {
		
		// Add new post
		$postID = web_apper_insert_record( $_POST );

		// If post was created successfully
		if ( $postID  ) :
			$this->add_custom_fields_by_post();  // Save post meta
			do_action( $this->shortcode . '_post_submit_insert', $postID, $_POST  ); // Allow form post values to be hooked onto
			return json_encode( $this->sendResponse( 'Record saved.', 'Hurray!', 'alert-success', true ) );  // Send Response
		else :  // Else post not created
			return json_encode( $this->sendResponse( 'There was a problem saving the record. Please Try again.', 'Oh snap!', 'alert-error', false ) );  // Send Response
		endif;
	}

    /**
     * Update a record
     *
     * @since 1.0
     */
	public function update_record() {
		
		$postID = $_POST['web_apper_post_id'];

		// Update the post
		$result = web_apper_update_record( $postID, $_POST );// Update post

		// If post was updated successfully
		if ( $result ) :
			$this->add_custom_fields_by_post();  // Save post meta
			do_action( $this->shortcode . '_post_submit_update', $postID, $_POST  ); // Allow form post values to be hooked onto
			return json_encode( $this->sendResponse( 'Record updated.', 'Hurray!', 'alert-success', true ) );  // Send Response
		else :
			return json_encode( $this->sendResponse( 'There was a problem updating the record. Please Try again.', 'Oh snap!', 'alert-error', false ) );  // Send Response
		endif;
	}

}

$initialize = new Form(); 

?>