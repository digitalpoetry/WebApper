<?php
namespace WebApper\Shortcode;

/*
 * [index id='Index' posttype=lead fieldset='Group 1' linkedfield='field-id' titlefield='field-id' editor=false adminonly=false]
 *
 */
class RecordIndex extends \WebApper\IndexView {
	
    /**
     * Define shortcode properties
     *
     */
	protected $shortcode = 'index';
	protected $defaults = array(
		'id' => null,		// The unique ID to use for the shortcode. Default: none. Required.
		'posttype' => null,					// The cutom post type (ID) to display. Default: none. Required.
		'fieldset' => null,					// The set of fields to include in the indextable. Default: none. Required if 'include' is not specified. You may use one or both.
		'include' => null,					// field-id's of individual fields to include in the indextable. Default: none. Required if 'fieldset' is not specified. You may use one or both.
		'exclude' => null,					// field-id's of individual fields to exclude in the indextable. Default: none.
		'requirefields' => true,		// Whether to 
		'viewcap' => 'edit_posts',	// null
		'addcap' => 'edit_posts',	// null
		'editcap' => 'edit_posts',	// null
		'deletecap' => 'edit_plugins',	// null
		'archivecap' => 'edit_plugins',	// null
		'bulkcap' => 'edit_plugins',	// null
		'tableclasses' => 'table-bordered,table-striped,table-hover,table-condensed',  // Adjusts how the indextable will display. Default: 'table-bordered,table-striped,table-hover,table-condensed'.
	);

    /**
     * Handles the add post shortcode
     *
     * @param array $atts
     */
    public function shortcode( $atts ) {
			
		// Allow filtering of shortcode attributes before rendering
			$atts = apply_filters( $this->shortcode . '_atts', $atts, $id );

		// Get shortcode attributes
			extract( $atts = shortcode_atts( $this->defaults, $atts ) );

		// Check for required shortcode attributes
			$msg = $this->has_req_attrs( $atts );
			if ( $msg !== true )
				return $msg;

        // Check if current user has proper privileges to view
			if ( !$this->current_user_has_cap($viewcap) ) :
				echo 'You do not have sufficient permissions to access this content.';
				return;
			endif;

		// Get the fields
			$this->get_fields( $atts );
			$fieldPosition = apply_filters( $this->shortcode . 'DataTable_field_counter', 0, $id ); // Create hook to correct for columns added to start of table
			foreach ( $this->fields as $field ) :  // and foreach field,
				$fieldIDs[] = $field['field_id'];
				if ( !$field['show_in_table'] ) :
					$columnsToHide[] = $fieldPosition;
				endif;
				$fieldPosition++;
			endforeach;
			$fieldIDs = implode( ',', $fieldIDs );

		// Build the shortcode output hrml string
			global $template;
			if ( stristr($template, 'fullwidth') ) :
				$span = 12;
			else :
				$span = 9;
			endif;
			?>

            <table id="<?php echo $id; ?>DataTable" class="table <?php echo str_replace(",", " ", $tableclasses); ?> span<?php echo $span; ?>">
                <thead>
                    <tr>
					<?php
						$headers_start = apply_filters( $this->shortcode . '_datatable_headers_start', array(), $id ); // Create hook to add table headers
						foreach ( $headers_start as $header ) : // Loop thru custom fields and echo a table header foreach
							$this->build_dt_header( $header['field_name'], $header['field_id'], $header['filter_type'], $header['filter_options'] );
						endforeach;
						foreach ( $this->fields as $field ) : // Loop thru custom fields and echo a table header foreach
							$header_type = apply_filters( $this->shortcode . '_datatable_cf_header_type', '', $id, $field['field_id'] ); // Create hook to add table headers
							$header_options = apply_filters( $this->shortcode . '_datatable_cf_header_options', null, $id, $field['field_id'] ); // Create hook to add table headers
							$this->build_dt_header( $field['field_name'], $field['field_id'], $header_type, $header_options );
						endforeach;
						$headers_end = apply_filters( $this->shortcode . '_datatable_headers_end', array(), $id ); // Create hook to add table headers
						foreach ( $headers_end as $header ) : // Loop thru custom fields and echo a table header foreach
							$this->build_dt_header( $header['field_name'], $header['field_id'], $header['filter_type'], $header['filter_options'] );
						endforeach;
					?>
                   </tr>
                </thead>
                <tbody></tbody>
            </table>

            <ul id="<?php echo $id; ?>contextMenu" class="dropdown-menu context-menu hide fade" role="menu">
            	<?php
					$context_menu = '
						<li><a id="editSelected" tabindex="-1" href="#">Edit selected rows</a></li>
						<li><a id="edit" tabindex="-1" href="#">Edit row</a></li>
						<li class="divider"></li>
						<li><a id="view" tabindex="-1" href="#" target="_blank">View Record</a></li>
						<li class="divider"></li>
					';
					echo apply_filters( $this->shortcode . '_right_click_menu', $context_menu, $id ); // Create hook to add HTML & JS
				?>
            </ul>

            <div id="<?php echo $id; ?>Modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="<?php echo $id; ?>ModalLabel" aria-hidden="true"></div>

			<?php
				// Set the repsponsive HTML classes
				if ( stristr($template, 'fullwidth') ) :
					$spanA = 6; $spanB = 6;
				else :
					$spanA = 4; $spanB = 5;
				endif;
				// Editor Buttons
				if ( $this->current_user_has_cap($addcap) )
					$btns .= 'I';
				if ( $this->current_user_has_cap($editcap) )
					$btns .= 'U';
				if ( $this->current_user_has_cap($deletecap) )
					$btns .= 'D';
				if ( $this->current_user_has_cap($archivecap) )
					$btns .= 'A';
				if ( $this->current_user_has_cap($bulkcap) )
					$btns .= 'S';
				if ( !empty($btns) )
					$btns = "<'#editorBtns.btn-group'" . $btns . ">";
			?>
            <script type="text/javascript">

				jQuery(document).ready(function($) {


					table = $('#<?php echo $id; ?>DataTable').dataTable({
						'dom': "<'row' <'span<?php echo $spanA; ?>' R <?php echo $btns; ?> C ><'span<?php echo $spanB; ?>' l f > t <'span<?php echo $spanA; ?>' i ><'span<?php echo $spanB; ?>' p > >",  // Set HTML DOM options
						'search': { 'smart': true }, // Enable DataTables' smart filtering
						'stateSave': true,
						'processing': true,
						'serverSide': true,
						'ajax': {
							'url': '<?php echo admin_url('admin-ajax.php'); ?>',
							'type': 'POST',
							'data': {
								'action': 'web_apper<?php echo $this->shortcode; ?>',
								'web_apper_action': 'get_records',
								'web_apper_nonce': '<?php echo wp_create_nonce('WebApperAwesomeness!87'); ?>',
								'web_apper_posttype': '<?php echo $posttype; ?>',
								'web_apper_fields': '<?php echo $fieldIDs; ?>',
								'id': '<?php echo $id; ?>',
								'columns': [
									<?php echo apply_filters( $this->shortcode . '_datatable_col_filters', $datatable_col_filters, $id ); // Create hook to add table cells ?>
								],
							},
						},
						'columns': [
							<?php
								foreach ( $headers_start as $header ) : // Loop thru custom fields and echo a table header foreach
									echo "{ 'data': '" . $header['field_id'] . "', },";
								endforeach;
								foreach ( $this->fields as $field ) :  // and foreach field,
									echo "{ 'data': '" . $field['field_id'] . "', },";
								endforeach;
								foreach ( $headers_end as $header ) : // Loop thru custom fields and echo a table header foreach
									echo "{ 'data': '" . $header['field_id'] . "', },";
								endforeach;
							?>
						],
						'columnDefs': [
							{ 'defaultContent': '&nbsp;', 'targets': '_all' },
							//{ "type": "datetime-au", 'targets': [ 3, 6 ] }
						],
						'oColVis': {  // Set ColVis options
							'buttonText': 'Columns', // Set the colVis button text
							'bRestore': true, // Enable the colVis restore button
            				'sRestore': 'Restore columns' // Set the colVis restore button text
						},
					});

					// Get the DataTable thead node
					var tableHead = $('thead', table);
					// Add click handler to open TH dropdowns
					$('th', tableHead).click( function(e) {
						$(this).children('.dropdown-toggle').dropdown('toggle');
						e.stopPropagation();
					});
					// Fix TH dropdown from closing/ Prevent colReorder on dropdowns
					$('.dropdown-menu, .ColVis_collection input', tableHead).click( function(e) {
						e.stopPropagation();
					}).mousedown( function(e) {
						e.stopPropagation();
					});				
					// Fix TH dropdown input focus problem
					$('.dropdown-menu *', tableHead).click( function(e) {
						this.focus();
					});
					// Column sort handler
					$('.dropdown-menu a', tableHead).on('click', function(e) {
						var sort_count = $(this).parents('table').data('sort_count');
						var current_sort = $(this).parents('th').data('sort');
						var sort_order = $(this).parents('th').data('sort_order');
						if ( sort_count == null ) {
							sort_count = 0;
						}
						if ( current_sort == null ) {
							$(this).parents('th').data('sort','asc');
							$(this).children('i').removeClass('icon-arrow-up').addClass('icon-arrow-down');
							$(this).parents('th').data('sort_order', sort_count);
							sort_count++;
							$(this).parents('table').data('sort_count', sort_count)
						} else if ( current_sort == 'asc' ) {
							$(this).parents('th').data('sort','desc');
							$(this).children('i').removeClass('icon-arrow-down').addClass('icon-remove-circle');
							$(this).parents('th').data('sort_order', sort_count);
							sort_count++;
							$(this).parents('table').data('sort_count', sort_count)
						} else if ( current_sort == 'desc' ) {
							$(this).parents('th').data('sort', null);
							$(this).children('i').removeClass('icon-remove-circle').addClass('icon-arrow-up');
							$(this).parents('th').data('sort_order', null);
						}
						var order = [];
						$('th', tableHead).each(function(i,el) {
							if ( $(el).data('sort') != null ) {
								var order_i = $(el).data('sort_order');
								order[ order_i ] = [ i, $(el).data('sort') ];
							}
						});
						var order_final = [];
						$.each( order, function( key, value ) {
							if ( value != null ) {
								order_final.push( value );
							}
						});
						if ( 0 < order_final.length ) {
							table.api().order( order_final ).draw();
						} else {
							table.api().order( [ 0 ] ).draw();
						}
					});
					
					// Column filter handlers
					$('input:checkbox', tableHead).on('change', function(e) {
						table.api().ajax.reload();
					});
					var globalTimeout = null;  
					$('input:not(:checkbox)', tableHead).keyup( function(e) {
					  if (globalTimeout != null) {
						clearTimeout(globalTimeout);
					  }
					  globalTimeout = setTimeout(function() {
						globalTimeout = null;  
						table.api().ajax.reload();
					  }, 500);  
					} );


					// Add a click handler for the 'Select All' button
					$('#selectRowsDatatable').on("click", function() {
						if ( $(this).text() == 'Select All' ) {
							$(this).text('Deselect All');
							$('tbody tr', table).each(function() {
								$(this).addClass('row_selected');
							});
						} else {
							$(this).text('Select All');
							$('tr', table).each(function() {
								$(this).removeClass('row_selected');
							});
						}
					});

					// Add a click handler for the Editor Add row button
					$('#insert_<?php echo $id; ?>DataTable').on("click", function() {
						$('.alert').remove();  // Close any alerts that may be open
						var data = {
							action: 'web_apper<?php echo $this->shortcode; ?>',
							web_apper_action: 'get_form',
							web_apper_nonce: '<?php echo wp_create_nonce("WebApperAwesomeness!87"); ?>',
							id: '<?php echo $id; ?>',
							fields: '<?php echo $fieldIDs; ?>',
							posttype: '<?php echo $posttype; ?>',
							titlefield: '<?php echo $titlefield; ?>',
						};
						$.post("<?php echo admin_url('admin-ajax.php'); ?>", data, function(form) {
							$('#<?php echo $id; ?>Modal').html(form);  // Insert form into modal
							$('#<?php echo $id; ?>Modal').removeClass('minimize').addClass('dock').modal({ backdrop: false });  // Show the modal
						});
					});
	  
					// Add a click handler for the Editor Edit row button
					$('#edit_<?php echo $id; ?>DataTable').on("click", function() {
						$('.alert').remove(); // Close any alerts that may be open
						if ( $('tr.row_selected').length > 1 ) {
							var post_id = []; 
							$('tr.row_selected').each(function() {
								post_id.push( $(this).data('post-id') );
							});
						} else if ( $('tr.row_selected').length === 1 ) {
							var post_id = $( $('tr.row_selected')[0] ).data('post-id');
						} else {
							alert("Select a row to edit first!");
							return false;
						}
						var data = {
							action: 'web_apper<?php echo $this->shortcode; ?>',
							web_apper_action: 'get_form',
							web_apper_nonce: '<?php echo wp_create_nonce("WebApperAwesomeness!87"); ?>',
							web_apper_post_id: post_id,
							id: '<?php echo $id; ?>',
							fields: '<?php echo $fieldIDs; ?>',
							posttype: '<?php echo $posttype; ?>',
							titlefield: '<?php echo $titlefield; ?>',
						};
						$.post("<?php echo admin_url('admin-ajax.php'); ?>", data, function(form) {
							$('#<?php echo $id; ?>Modal').html(form); // Insert form into modal
							$('#<?php echo $id; ?>Modal').removeClass('minimize').addClass('dock').modal({ backdrop: false }); // Show the modal
						});
					});
	
					// Add a click handler for the Context Menu 'Edit row' button
					$('#<?php echo $id; ?>contextMenu #edit').on("click", function() {
						$('tr', table).each(function() {
							$(this).removeClass('row_selected');
						});
						$('tr.clicked', table).addClass('row_selected');
						$('#edit_<?php echo $id; ?>DataTable').click();
					});
	
					// Add a click handler for the Context Menu 'Edit selected rows' button
					$('#<?php echo $id; ?>contextMenu #editSelected').on("click", function() {
							$('#edit_<?php echo $id; ?>DataTable').click();
					});

					// Add a click handler for the Editor Delete row button 
					$('#delete_<?php echo $id; ?>DataTable').on("click", function() {
						$('.alert').remove();  // Close any alerts that may be open
						if ( $('tr.row_selected').length > 0 ) {
							post_id = []; 
							$('tr.row_selected').each(function() {
								post_id.push( $(this).data('post-id') );
							});
						} else {
							alert("Select a row to delete first!");
							return false;
						}
						var data = {
							action: 'web_apper<?php echo $this->shortcode; ?>',
							web_apper_action: 'delete_record',
							web_apper_nonce: '<?php echo wp_create_nonce("WebApperAwesomeness!87"); ?>',
							web_apper_post_id: post_id,
							id: '<?php echo $id; ?>',
						};
						$.post("<?php echo admin_url('admin-ajax.php'); ?>", data, function(response) {
							parseResponse<?php echo $id; ?>(response)
						});
					});

					// Add a click handler for the 'Select All' button
					$('#select_<?php echo $id; ?>DataTable').on("click", function() {
						if ( $(this).text() == 'Select All' ) {
							$(this).text('Deselect All');
							$('tbody tr', table).each(function() {
								$(this).addClass('row_selected');
							});
						} else {
							$(this).text('Select All');
							$('tr', table).each(function() {
								$(this).removeClass('row_selected');
							});
						}
					});

					// Context Menu
					$('tbody tr', table).live('mouseover', function() {
						this.oncontextmenu = function(e) {
							$('.clicked').removeClass('clicked');
							$(this).addClass('clicked');
							if ( $('tr.row_selected').length > 1 ) {
								$('#<?php echo $id; ?>contextMenu #editSelected').parent().show();
							} else {
								$('#<?php echo $id; ?>contextMenu #editSelected').parent().hide();
							}
							$('#<?php echo $id; ?>contextMenu #view').attr('href', $('tr.clicked', table).data('singleview') );
							$('#<?php echo $id; ?>contextMenu').css('top', e.pageY ).css('left', e.pageX ).addClass('in');
							e.preventDefault();
						}
					});
					$('#site-main').on("click", function() {
						$('#<?php echo $id; ?>contextMenu').removeClass('in');
					});
					$('tbody tr', table).mouseover(function(){
						$('#site-main').each(function() {
							this.oncontextmenu = function(e) {
								// Do nothing...
							}
						});
					});
					$('tbody tr', table).mouseout(function(){
						$('#site-main').each(function() {
							this.oncontextmenu = function(e) {
								$('#<?php echo $id; ?>contextMenu').removeClass('in');
							}
						});
					});
					$('#<?php echo $id; ?>contextMenu').mouseover(function(){
						$('#site-main').on("click", function() {
							// Do nothing...
						});
					});
					$('#<?php echo $id; ?>contextMenu').mouseout(function(){
						$('#site-main').on("click", function() {
							$('#<?php echo $id; ?>contextMenu').removeClass('in');
						});
					});
					
				});

				// Handle response from ajax post
				function parseResponse<?php echo $id; ?>(response) {
					var result = jQuery.parseJSON(response);  // Parse response
					if ( result.success ) {  // If ajax returns a successful save
						table.api().ajax.reload();
					}
					jQuery('#<?php echo $id; ?>Modal').modal('hide');
					jQuery('.page-content').prepend(result.htmlalert);  // Show and alert
					jQuery('#select_<?php echo $id; ?>DataTable').text('Select All');
				}
				
				// Add a click/dblclick handler to the table rows
				jQuery('tr', '#<?php echo $id; ?>DataTable tbody').live('click', function(e) {
					var that = this;
					setTimeout(function() {
						var dblclick = jQuery(that).data('double');
						if (dblclick > 0) {
							jQuery(that).data('double', dblclick-1);
						} else {
							trSingleClick.call(that, e);
						}
					}, 100);
				}).live('dblclick', function(e) {
					jQuery(this).data('double', 2);
					trDoubleClick.call(this, e);
				});
				function trSingleClick(e) {
					if ( jQuery('td', this).hasClass('dataTables_empty') ) { // If the dataTable is empty
						return; // Do nothing
					} else { // Else the dataTable has rows
						if ( jQuery(this).hasClass('row_selected') ) { // If the row is already selected
							jQuery(this).removeClass('row_selected'); // Unselect the row
						} else { // Else the row is not selected
							jQuery(this).addClass('row_selected'); // Select the row
						}
					}
				}
				function trDoubleClick(e) {
					if ( jQuery('td', this).hasClass('dataTables_empty') ) { // If the dataTable is empty
						return; // Do nothing
					}
					jQuery('tr.row_selected', table).removeClass('row_selected'); // Unselect any other rows
					jQuery(this).addClass('row_selected'); // Select the row
					jQuery('#edit_<?php echo $id; ?>DataTable').click(); // Click the Edit button
				}
				
			</script>        
            <?php

			echo apply_filters( $this->shortcode . '_html_js_end', $html_js_end, $id ); // Create hook to add HTML & JS
	}

    /**
     * Checks if a shortcode has required attributes
     *
	 * @param string $usercap
     * @since 1.0
     */
	protected function has_req_attrs( $atts ) {
		// Check for required shortcode attributes
		if ( $atts['id'] == null )
			return 'You must define the shortcode id, i.e., [index id=\'form-1\']';
		if ( $atts['posttype'] == null )
			return 'You must define the shortcode posttype to display, i.e., [index posttype=lead]';
		if ( $atts['fieldset'] == null && $atts['include'] == null )
			return 'You must define a shortcode  \'fieldset\' or \'include\' attribute to add some columns to the table, i.e., [index fieldset=\'group_1\']';
		if ( $atts['viewcap'] == null )
			return 'You must define a shortcode viewing capability, i.e., [index viewcap=edit_posts]';
		return true;
	}

    /**
     * Get tasks from the DB
     *
     * @since 1.0
     */
	protected function get_records() {
		$columns[] = array(
			'db' => 'ID', 'dt' => 'DT_RowId',
			'formatter' => function( $d, $row ) {
				return 'row_' . $d;
			}
		);
		$columns[] = array(
			'db' => 'ID', 'dt' => 'DT_RowData',
			'formatter' => function( $d, $row ) {
				global $wpdb;
				$post = get_post( $d );
				$singleview = get_option( 'singleview_url_' . $post->post_type );
				$posttype = web_apper_posttype_get_results( "SELECT `posttype_singleview_url` FROM `{$wpdb->prefix}web_apper_posttypes` WHERE `posttype_slug` = '{$post->post_type}';" );
				return array(
					'post-id' => $d,
					'singleview' => $singleview . '?postID=' . $d
				);
			}
		);
		$columns = apply_filters( $this->shortcode . '_datatable_cells_start', $columns, $id ); // Create hook to add table cells
		$fieldIDs = explode(',', $_POST['data']['fields']);
		foreach ( $fieldIDs as $fieldID ) :  // Foreach post,
			$dbID = 'web_apper_' . str_replace( '-', '_', $fieldID );
			$columns[] = array(
				'db' => $dbID, 'dt' => $fieldID,
				'formatter' => function( $d, $row ) {
					if ( $this->isSerialized( $d ) ) :  // Possibly unserialize value
						$d = unserialize($d);
					endif;
					if ( is_array( $d ) ) :  // Possibly cantonate array into CSV string
						$d = implode( ',', $d );
					endif;
					return $d;
				}
			);
		endforeach;
		$columns = apply_filters( $this->shortcode . '_datatable_cells_end', $columns, $id ); // Create hook to add table cells
		require_once( 'ssp.class.php' );
		global $webapper;		
		echo json_encode(
			\SSP::simple( $_POST, 'CustomFields', 'ID', $columns ) // $_GET, $sql_details, $table, $primaryKey, $columns
		);
	}

    /**
     * Echos a form for ajax
     *
     * @since 1.0
     */
	public function get_form() {
		$id = $_POST['id'];
		if ( is_array($_POST['web_apper_post_id']) ) :
			$postID = implode( ',' , $_POST['web_apper_post_id'] );
			$bulk_edit = true;
		else :
			$postID = $_POST['web_apper_post_id'];
			$bulk_edit = false;
		endif;
		$posttype = $_POST['posttype'];
		
		$this->config_form( array( 'id' => $id ) ); // Set form settings
		$form = new \PFBC\Form( $id );
		$form->configure( $this->formConfig ); // Configure form settings
		$form->addElement( new \PFBC\Element\Hidden( 'web_apper_form', $_POST['id'] ) );
		$form->addElement( new \PFBC\Element\Hidden( 'web_apper_nonce', wp_create_nonce( 'WebApperAwesomeness!87' ) ) );
		$form->addElement( new \PFBC\Element\Hidden( 'action', 'web_apper' . $this->shortcode ) );
		if ( is_null($postID) ) :
			$form->addElement( new \PFBC\Element\Hidden( 'web_apper_action', 'add_record' ) );
		else:
			$form->addElement( new \PFBC\Element\Hidden( 'web_apper_action', 'update_record' ) );
		endif;
		$form->addElement( new \PFBC\Element\Hidden( 'web_apper_posttype', $posttype ) );
		$form->addElement( new \PFBC\Element\Hidden( 'web_apper_fields', $_POST['fields'] ) );
		$form->addElement( new \PFBC\Element\Hidden( 'web_apper_titlefield', $_POST['titlefield'] ) );
		if ( !is_null($postID) ) :
			$form->addElement( new \PFBC\Element\Hidden( 'web_apper_post_id', $postID ) );
		endif;
		// Form Modal header
		if ( is_null($postID) ) :
			$form->addElement(new \PFBC\Element\ModalHeading('Add ' . $posttype));
		else:
			$form->addElement(new \PFBC\Element\ModalHeading('Edit ' . $posttype));
		endif;
		// Form Modal body
		$this->get_fields( array( 'include' => $_POST['fields'] ) ); // Get the fields
		foreach ( $this->fields as $field ) :
			if ( !$requirefields ) :
				$field['required'] = 0;
			endif;
			if ( $bulk_edit ) :
				if ( $field['bulk_edit'] == 1 ):
					$this->add_form_field( $field, $form, $postID );
				endif;
			else:
				$this->add_form_field( $field, $form, $postID );
			endif;
		endforeach;


		// Form Modal footer
		if ( is_null($postID) ) :
			$form->addElement(new \PFBC\Element\Button('Save', 'submit', array(
				'id' => 'save',
				'data-loading-text' => 'Saving...'
			)));
		else:
			$form->addElement(new \PFBC\Element\Button('Update', 'submit', array(
				'id' => 'update',
				'data-loading-text' => 'Updating...'
			)));
		endif;
		$form->render();  // Output the form
	}

    /**
     * Add new record
     *
     * @since 1.0
     */
	public function add_record() {

		// Add new post
		$postID = web_apper_insert_record( $_POST );

/*
		$user_name = trim($title);
		$userID = username_exists( $user_name );
		if ( $userID !== null ) {
			$append = 1;
			while( $userID !== null ) {
				$append++;
				$user_name .= $append;
				$userID = username_exists( $user_name  );
			}
		}
		$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
		$user_email = $random_password.'@testing.com';
		if ( email_exists($user_email) == false ) {
			$userID = wp_insert_user( array ('user_login' => $user_name, 'user_pass' => $random_password, 'role' => 'client' ) ) ;
		} else {
			return $this->sendResponse( 'Save unsuccessful! New user could not be created.', 'alert-error', 'Oh snap!', false );  // Send Response
		}
		add_user_meta( $userID, 'postID', $postID, true );
		add_post_meta( $postID, 'userID', $userID, true );
*/


		// Send ajax response
		if ( $postID ) :
			do_action( $this->shortcode . '_post_insert', $postID, $_POST  ); // Allow form post values to be hooked onto
			$this->update_custom_fields_by_id( $postID, $_POST['web_apper_fields'] );  // Save post meta
			return json_encode( $this->sendResponse( 'Record saved.', 'Hurray!', 'alert-success', true ) );  // Send Response
		else :
			return json_encode( $this->sendResponse( 'There was a problem saving the record. Please Try again.', 'Oh snap!', 'alert-error', false ) );  // Send Response
		endif;
	}

    /**
     * Update a record
     *
     * @since 1.0
     */
	public function update_record() {

		$postIDs = explode( ',', $_POST['web_apper_post_id'] );

		$records = array();
		foreach( $postIDs as $postID ) :
			$error = false;
			$result = web_apper_update_record( $postID, $_POST );// Update post
			// If post was updated successfully
			if ( $result ) : 
				$this->update_custom_fields_by_id( $postID, $_POST['web_apper_fields'] );  // Save post meta
			else :
				$error = true;
				$records[] = $postID;
			endif;
		endforeach;
		
		$records = implode( ',', $records );

		// If post was updated successfully
		if ( $error ) :
			return json_encode( $this->sendResponse( 'The following records were not updated: ' . $records, 'Oh snap!', 'alert-error', false ) );  // Send Response
		else :
			do_action( $this->shortcode . '_post_submit_update', $postID, $_POST  ); // Allow form post values to be hooked onto
			return json_encode( $this->sendResponse( 'Record updated.', 'Hurray!', 'alert-success', true ) );  // Send Response
		endif;
	}

    /**
     * Delete a record
     *
     * @since 1.0
     */
	public function delete_record() {

		$records = array();
		foreach( $_POST['web_apper_post_id'] as $postID ) :
			$error = false;
			$result = web_apper_delete_record( $postID );
			// If something went wrong
			if ( !$result ) : 
				$error = true;
				$records[] = $postID;
			endif;
		endforeach;

		$records = implode( ',', $records );

		if ( $error ) :
			return json_encode( $this->sendResponse( 'The following records were not deleted: ' . $records, 'Oh snap!', 'alert-error', false ) );  // Send Response
		else :
			do_action( $this->shortcode . '_post_submit_delete', $postID, $_POST  ); // Allow form post values to be hooked onto
			return json_encode( $this->sendResponse( 'Record deleted.', 'Hurray!', 'alert-success', true ) );  // Send Response
		endif;
	}
	
}

$initialize = new RecordIndex(); 

?>