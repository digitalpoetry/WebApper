<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.



/**
 * Add Record
 * 
 * @since WebApper (1.0)
 * @param arr $itemData The Record details
 * @return mixed (int) insert ID on success, (bool) false on fail
 */
function web_apper_insert_record( $itemData ) {

	/**
	 * Pre Record insert hook.
	 *
	 * Allow hooking onto Record data before saving Record to database.
	 *
	 * @since 0.1.2
	 * 
	 * @param array $itemData Associative array containing the Fields of the
	 *              Record being added.
	 */
	$itemData = apply_filters( 'record_pre_insert', $itemData );
	
	$author = web_apper_set_record_author(); // Set the post author
	$postData = array( // Build post array
		'post_content' => 'Web Apper custom post type, do not delete.',
		'post_type'    => $_POST['web_apper_posttype'],
		'post_status'  => 'publish',
		'post_author'  => $author,
	);
	$itemID = wp_insert_post( $postData ); // Save post to database

	if ( $itemID ) :
		
		// Save Record Fields
		web_apper_update_field_data( $itemID, $itemData );
		// Update the 'last_modified_by' Field
		update_post_meta( $postID, 'last_modified_by', $author );

		/**
		 * Post Record insert hook.
		 *
		 * Allow hooking onto Record ID & data after saving Record to database.
		 *
		 * @since 0.1.2
		 * 
		 * @param integer $itemID The Record ID of the Record being added.
		 * @param array   $itemData Associative array containing the Fields of
		 *                the Record being added.
		 */
		do_action( 'record_post_insert', $itemID, $itemData );
		
		return $itemID;
	else :
		return false;
	endif;
}


/**
 * Update Record
 * 
 * @since WebApper (1.0)
 * @param int $itemID The Record ID
 * @param arr $itemData The Record details
 * @return bool true on success, false on failure
 */
function web_apper_update_record( $itemID, $itemData ) {	

	/**
	 * Pre Record update hook.
	 *
	 * Allow hooking onto Record data before updating Record to database.
	 *
	 * @since 0.1.2
	 * 
	 * @param array $itemData Associative array containing the Fields of the
	 *              Record being updated.
	 */
	$itemData = apply_filters( 'record_pre_update', $itemData );
	
	$postData = array( // Build post array
		'ID' => $itemID,
		'post_content' => 'Web Apper custom post type, do not delete.',
	);
	$result = wp_update_post( $postData ); // Update post to database

	if ( $result ) :
		
		// Update Record Fields
		web_apper_update_field_data( $itemID, $itemData );
		// Update the 'last_modified_by' Field
		update_post_meta( $postID, 'last_modified_by', $author );
		
		/**
		 * Post Record update hook.
		 *
		 * Allow hooking onto Record ID & data after updating Record to database.
		 *
		 * @since 0.1.2
		 * 
		 * @param integer $itemID The Record ID of the Record being updated.
		 * @param array   $itemData Associative array containing the Fields of
		 *                the Record being updated.
		 */
		do_action( 'record_post_update', $itemID, $itemData );
		
		return true;
	else :
		return false;
	endif;
}

/**
 * Delete Record
 *
 * @since WebApper (1.0)
 * @param int $itemID The Record ID
 * @return bool true on success, false on failure
 */
function web_apper_delete_record( $itemID ) {
	
	do_action( 'record_pre_delete', $itemID ); // Allow Record data to be hooked onto
	
	$result = wp_delete_post( $itemID, true );
	
	if ( $result ) :
		do_action( 'record_post_delete', $item ); // Allow Record data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Get Record
 * 
 * @since WebApper (1.0)
 * @param int $itemID The Record ID
 * @return (obj) The Record
 */
function web_apper_get_record( $itemID ) {
	$post = get_post( $itemID );
	return $post;
}

/**
 * Return an integer to use for the post author
 *
 * @since 1.0
 */
function web_apper_set_record_author() {
	// Set a post author ID
	if ( is_user_logged_in() ) :
		global $current_user;
		return $current_user->ID;
	else :
		return 0;
	endif;
}

/**
 *  Desc
 *
 * @param integer $postID
 * @since 1.0
 */
function web_apper_get_primary_app_ID( $postID ) {
	$is_primary = get_post_meta( $postID, 'is_primary_applicant', true );
	if ( $is_primary === 0 || $is_primary === false ) :
		return get_post_meta( $postID, 'primary_applicantID', true );
	else :
		return $postID;
	endif;
}


/**
 * Get post data for fields
 *
 * @since 1.0
 */
function web_apper_update_field_data( $itemID, $itemData ) {
	// removes all NULL, FALSE and Empty Strings but leaves 0 (zero) values
//	$itemData = array_filter( (array) $itemData, 'strlen');
	foreach ( $itemData as $key => $val ) :
		update_post_meta( $itemID, $key, $val );
	endforeach;
}
