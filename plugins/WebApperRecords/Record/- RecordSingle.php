<?php
/*
 * [recordsingle]
 *
 */

namespace WebApper\Shortcode;

/*
 * [recordsingle]
 *
 */
class RecordSingle extends \WebApper\Shortcode {
	
    /**
     * Define shortcode properties
     *
     */
	protected $shortcode = 'recordsingle';
	protected $defaults = array(
		'id' => null,					// The unique ID to use for the shortcode. Default: none. Required.
		'viewcap' => 'edit_posts',	// The Required capability to view
		'addcap' => 'edit_posts',	 // The Required capability to add
		'editcap' => 'edit_posts',	// The Required capability to edit
		'deletecap' => 'edit_posts', // The Required capability to delete
		'posttype' => null,					// The cutom post type (ID) to display. Default: none. Required.
		'include' => null,					// field-id's of individual fields to include in the singleview. Default: none. Required if 'fieldset' is not specified. You may use one or both.
	);

    /**
     * Handles the add post shortcode
     *
     * @param array $atts
     */
    public function shortcode( $atts ) {

		// Get shortcode attributes
		extract( shortcode_atts( $this->defaults, $atts ) );
			
		// Create Filter to allow hooking before rendering
		$atts = apply_filters(  $this->shortcode . '_pre_render', $atts );

		// Check for required shortcode attributes
		$msg = $this->has_req_attrs( $atts );
		if ( $msg !== true )
			return $msg;

        // Check if current user has proper privileges
		if ( !$this->user_has_cap( $usercap, $ownercap ) )
			return;

        // Check if a record ID was passed
		global $post;
		$pageID = $post->ID;
		if ( !empty( $_GET['postID'] ) ) :
			// Get the single post
			$postID = $_GET['postID'];  // Set the post ID
			$single_post = get_post( $postID );
		endif;


		// Clear any 'task' notifications for this item
		global $current_user;
		get_currentuserinfo();
		web_apper_delete_notifications_by_item_id( $current_user->ID, 'task', $postID );

		// Get the fields
		$this->fields = web_apper_get_fields( $atts['include'] );
		/* Arrange the fields into array of fieldsets sorted by fieldset_order,
		 * with the fieldset as the item key and each item containing an
		 * array of the fieldset's fields sorted by field_order
		 */
		$fieldsets = array();
		foreach ( $this->fields as $field ) :
			if ( !array_key_exists( $field['fieldset_id'], $fieldsets ) ) :
				$fieldsets[ $field['fieldset_id'] ] = array(
					'fieldset_id' =>  $field['fieldset_id'],
					'fieldset_name' => $field['fieldset_name'],
					'fields' => array(),
				);
			endif;
			$fieldsets[ $field['fieldset_id'] ]['fields'][] = $field;
			$fieldIDs .= $field['field_id'] . ',';
		endforeach;

		// Ouput form & accordion HTML		
		?>
            
            <header class="page-header">
            	<?php if ( !empty( $_GET['postID']) ) : ?>
                    <p id="record_meta" class="pull-right">
                        <strong>Created:</strong> <?php echo date( 'F jS h:i:s A', strtotime($single_post->post_date) ); ?><br />
                        <strong>Last Modified:</strong> <?php echo date( 'F jS h:i:s A', strtotime($single_post->post_modified) ); ?>
                    </p>
                <?php endif; ?>
                <h1 class="page-title">
					<?php
						if ( empty( $_GET['postID']) ) :
							echo 'Add Record';
						else :
							echo $single_post->post_title;
						endif;
					?>
                </h1>
            </header><!-- .page-header -->
            
            <div class="clearfix"></div>
            

			<?php
			// Configure form settings
			$config['prevent'] = array('bootstrap', 'jQuery', 'focus');  // Prevents scripts from loading twice
			$config['view'] = new \PFBC\View\VerticalRecordSingle;
			$config['action'] = admin_url('admin-ajax.php');
			$config['ajax'] = 1;
			$config['ajaxCallback'] = 'parseResponse' . $id;
			// Build the form
			$this->config_form( $id ); // Set form settings
			$form = new \PFBC\Form( $id );
			$form->configure( $this->formConfig ); // Configure form settings
			$form->addElement( new \PFBC\Element\Hidden( 'web_apper_form', $id ) );
			$form->addElement( new \PFBC\Element\Hidden( 'action', 'web_apper' . $this->shortcode ) );
			$form->addElement( new \PFBC\Element\Hidden( 'web_apper_nonce', wp_create_nonce( 'AwesomeSauce!87' ) ) );
			$form->addElement( new \PFBC\Element\Hidden( 'web_apper_action', $web_apper_action ) );
			$form->addElement( new \PFBC\Element\Hidden( 'web_apper_include', $_POST['data']['include'] ) );
			if ( isset($itemID) ) :
				$form->addElement( new \PFBC\Element\Hidden( 'web_apper_item_ids', $itemID ) );
			endif;
			$this->fields = web_apper_get_fields( $_POST['data']['include'] ); // Get the fields
			foreach ( $this->fields as $field ) :
				if ( $bulk_edit ) :
					if ( $field['field_bulk_edit'] == 1 ):
						$field['field_required'] = 0;
						$this->add_form_field( $field, $form );
					endif;
				else:
					$field['field_value'] = $item->$field['field_id'];
					$this->add_form_field( $field, $form );
				endif;
			endforeach;
			$form->addElement(new \PFBC\Element\Button($submit_label, 'submit', array(
				'id' => 'submit',
				'data-loading-text' => $submit_label_loading
			)));
			$form->render(); // Output the form
			
			?>
    
    

			</div>
            <script type="text/javascript">

				jQuery(document).ready(function($) {
					
					// Toggle expand/collapse fields
					$('#showAll').click( function(e) {
						if	( $('#showAll').html() == 'Expand All' ) {
							$('#showAll').html('Collapse All');
							$('#site-content .collapse').collapse('show');
						} else {
							$('#site-content .collapse').collapse('hide');
							$('#showAll').html('Expand All');
						}
					});

					// Set form to change the record's post type
					$('#record_type').on("change", function(e) {
						$('input[name="web_apper_action"]').val('change_record_type');
						$('input[name="web_apper_posttype"]').val( $(this).val() );
					});

					// Set form to create Co-App
					$('#createCoApp').click( function(e) {
						$(".page-header .page-title").html('Create Co-applicant');
						$(".page-header #record_meta").addClass('hide');
						$(".page-header #createCoApp").addClass('hide');
						$(".page-header div.input-append").addClass('hide');
						$("#site-content .tabbable").addClass('hide');
						$("aside#quickHistoryEntry").addClass('hide');
						$('input[name="web_apper_action"]').val('add_record');
						$('input[name="web_apper_create_co_app"]').val('true');
						$('input.submit-record', 'form#<?php echo $id; ?>').val('Save');
						$('form#<?php echo $id; ?>').clearForm();
					});

					//Show and Hide Tab View
					$('#singleRecordTabs a').click(function (e) {
						e.preventDefault();
						$(this).tab('show');
					})

					//  Show/Hide edit button on collaspe
					$('.collapse').on('show', function () {
						var nParent = $(this).parent();
						$('.submit-record' , nParent).fadeIn(400);
					});
					$('.collapse').on('hide', function () {
						var nParent = $(this).parent();
						$('.submit-record' , nParent).fadeOut(400);
					});

					// Show collaspable fieldsets
					<?php if ( $showall == 'true' ) : ?>
						$('#site-content .collapse').collapse('show');
						$('#showAll').html('Collapse All');
					<?php else : ?>
						$('#site-content .collapse:first').collapse('show');
					<?php endif; ?>

					// aJax $_POST to change post type
					$('#changeRecordType').on("click", function() {
						$('.alert').remove();  // Close any alerts that may be open
						var data = $("form#<?php echo $id; ?>").serialize();
						$.post( "<?php echo site_url('wp-admin/admin-ajax.php'); ?>", data, function(response) {
								var result = $.parseJSON(response);  // Parse response
								$(".page-header").prepend(result.htmlalert);  // Show and alert
								$("select#record_type").parent().css('display','none');  // Show and alert
						});
					});

				});
				
				jQuery("form#<?php echo $id; ?>").bind('submit', function() {
					jQuery('.alert').alert('close');  // Close any alerts that may be open
				});
				
				function parseResponse<?php echo $id; ?>(response) {
					var result = jQuery.parseJSON(response);  // Parse response
					if ( result.action == 'add_record' ) {  // If we added a row
						window.location = window.location.host + window.location.pathname + '?postID=' + result.id
					}
					jQuery("form#<?php echo $id; ?>").prepend(result.htmlalert);  // Insert into Modal
				}
			</script>
			<?php
				// Create Filter to allow hooking during rendering
				echo apply_filters(  $this->shortcode . '_render', '' );
			?>
            
			<?php if ( isset($postID) ) : ?>
            <div class="tabbable">
                <ul id="singleRecordTabs" class="nav nav-tabs">
                    <li><a href="#cal" data-toggle="tab">Book a Meeting</a></li>
                    <li><a href="#bookings" data-toggle="tab">Bookings</a></li>
					<!--<li><a href="#tags" data-toggle="tab">Email Tags</a></li>-->
                    <li class="active"><a href="#tasks" data-toggle="tab">Tasks</a></li>
                    <li><a href="#emails" data-toggle="tab">Emails</a></li>
                    <li><a href="#entries" data-toggle="tab">Activity</a></li>
                    <li><a href="#docs" data-toggle="tab">Documents</a></li>
                </ul>
                <div id="singleRecordTabContent" class="tab-content">
                    <div class="tab-pane fade in" id="cal">
                        <?php do_shortcode("[calendar id='record_calendar' viewotherscap='' addcap=edit_posts]"); ?>
                    </div>
                    <div class="tab-pane fade in" id="bookings">
                        <?php do_shortcode("[bookingindex id='singleview_bookings']"); ?>
                    </div>
                    <?php // do_shortcode("[record_emails id='mailBox']"); ?>
                    <div class="tab-pane fade in active" id="tasks">
                        <?php do_shortcode("[tasks id='record_tasks']"); ?>
                    </div>
                    <div class="tab-pane fade in" id="emails">
                        <?php do_shortcode("[emailindex id='record_emails']"); ?>
                    </div>
                    <div class="tab-pane fade in" id="entries">
                        <?php do_shortcode("[history id='record_history']"); ?>
                    </div>
                    <div class="tab-pane fade in" id="docs">
                        <?php do_shortcode("[record_documents id='record_documents']"); ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            
			<?php
			
			do_action( $this->shortcode . '_html_js_end', $html_js_end, $id );
	}

    /**
     * Checks if a shortcode has required attributes
     *
	 * @param string $usercap
     * @since 1.0
     */
	protected function has_req_attrs( $atts ) {
		// Check for required shortcode attributes
		if ( $atts['id'] == null )
			return 'You must give the shortcode a unique ID, i.e., [shortcode id=\'form-1\']';
		if ( $atts['fieldset'] == null && $atts['include'] == null )
			return 'You must specify the \'fieldset\' or \'include\' attribute so you have some fields the indextable, i.e., [indextable fieldset=\'group_1\']';
		return true;
	}

    /**
     * Update a record from $_POST
     *
     * @since 1.0
     */
	public function add_record() {
		$id = $_POST['web_apper_id'];

		// Add new post
		$postID = web_apper_insert_record( $_POST );


		$user_name = trim($title);
		$userID = username_exists( $user_name );
		if ( $userID !== null ) {
			$append = 1;
			while( $userID !== null ) {
				$append++;
				$user_name .= $append;
				$userID = username_exists( $user_name  );
			}
		}
		$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
		$user_email = $random_password.'@testing.com';
		if ( email_exists($user_email) == false ) {
			$userID = wp_insert_user( array ('user_login' => $user_name, 'user_pass' => $random_password, 'role' => 'client' ) ) ;
		} else {
			return $this->sendResponse( 'Save unsuccessful! New user could not be created.', 'Oh snap!', 'alert-error', false );  // Send Response
		}
		add_user_meta( $userID, 'postID', $postID, true );
		add_post_meta( $postID, 'userID', $userID, true );



		// Send ajax response
		if ( $postID ) :
			if ( $_POST['web_apper_create_co_app'] == 'true' ) :
				if ( !empty($_POST['primary-applicant-id']) ) :
					$priAppPostID = $_POST['primary-applicant-id'];
				else :
					$priAppPostID = $_POST['web_apper_post_id'];
				endif;
				update_post_meta( $postID, 'has_co_applicant', 1 );
				update_post_meta( $postID, 'is_primary_applicant', 0 );
				update_post_meta( $postID, 'primary_applicantID', $priAppPostID );
				update_post_meta( $priAppPostID, 'has_co_applicant', 1 );
				update_post_meta( $priAppPostID, 'is_primary_applicant', 1 );
				$coApplicantIDs = get_post_meta( $priAppPostID, 'co_applicantIDs', true );
				if ( $coApplicantIDs == '' ) :
					$coApplicantIDs = array($postID);
				else:
					$coApplicantIDs[] = $postID;
				endif;
				update_post_meta( $priAppPostID, 'co_applicantIDs', $coApplicantIDs );
			endif;
			do_action( $this->shortcode . '_post_submit_add', $priAppPostID, $coApplicantIDs, $_POST  ); // Allow form post values to be hooked onto
			do_action( $this->shortcode . '_post_submit_insert', $postID, $_POST  ); // Allow form post values to be hooked onto
			$this->update_custom_fields_by_id( $postID, $_POST['web_apper_fields'] );  // Save post meta
			return json_encode( $this->sendResponse( 'Record saved.', 'Hurray!', 'alert-success', true, null, $postID ) );  // Send Response
		else :
		// Else post not inserted
			return json_encode( $this->sendResponse( 'There was a problem saving the record. Please Try again.', 'Oh snap!', 'alert-error', false ) );  // Send Response
		endif;
	}

    /**
     * Update a record from $_POST
     *
     * @since 1.0
     */
	public function update_record() {
		$id = $_POST['web_apper_id'];
		
		$postID = $_POST['web_apper_post_id'];
		
		// Update post
		$result = web_apper_update_record( $postID, $_POST );// Update post
		
		$this->update_custom_fields_by_id( $postID, $_POST['web_apper_fields'] );  // Save post meta

		// If post was updated successfully
		if ( $result ) :
			$this->update_custom_fields_by_id( $postID, $_POST['web_apper_fields'] );  // Save post meta
			do_action( $this->shortcode . '_post_submit_update', $postID, $_POST  ); // Allow form post values to be hooked onto
			return json_encode( $this->sendResponse( $_POST['web_apper_success_save'], 'Hurray!', 'alert-success', true ) );  // Send Response
		else :
		// Else post not updated
			return json_encode( $this->sendResponse( $_POST['web_apper_success_save'], 'Oh snap!', 'alert-error', false ) );  // Send Response
		endif;
	}

    /**
     * Change a record post type
     *
     * @since 1.0
     */
	public function change_record_type() {
		$id = $_POST['web_apper_id'];
		
		$postID = $_POST['web_apper_post_id'];

		// Set post type
		$result = set_post_type( $postID, $_POST['web_apper_posttype'] );

		// If post was updated successfully
		if ( $result  ) :
			return json_encode( $this->sendResponse( 'Record has been changed to ' . $_POST['web_apper_posttype'] . '.', 'Hurray!', 'alert-success', true ) );  // Send Response
		else :
		// Else post not updated
			return json_encode( $this->sendResponse( 'The record type was not changed. Please try again.', 'Oh snap!', 'alert-error', false ) );  // Send Response
		endif;
	}

    /**
     * Change a record post type
     *
     * @since 1.0
     */
	public function send_to_lender() {
		if ( isset($_POST['web_apper_post_id']) ) {
			$this->update_record();
		} else {
			$this->add_record();
		}
		$emailData = array(
			'toString' => $post['send_to_lender'],
			'subject' => 'New Profile for ' . $post['first-name'] . ' ' . $post['last-name'],
			'fromName' => 'AZRTO',
			'fromAddress' => 'noreply@azrto.info',
			'textHtml' => '<p>New profile for ' . $post['first-name'] . ' ' . $post['last-name'] . '</p><p>Please visit the following link to view:<br><br>' . site_url('financial-profile') . '?postID=' . $postID . '&access_key=' . get_post_meta( $postID, 'access_key', true ) . '</p>',
			'label' => 3,
			'user_id' => 0,
			'post_id' => $postID,
		);
		$result = web_apper_send_email( $emailData );
	}

}

$initialize = new RecordSingle(); 

?>