<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.

namespace WebApper\Applicant;

/*
 * [change_posttype]
 *
 */
class ChangePosttype extends \WebApper\Shortcode {
	
    /**
     * Define shortcode properties
     */
	protected $shortcode = 'change_posttype';
	protected $defaults = array(
		'id' => 'changerecord',
		'viewcap' => 'edit_posts',		// The minimum user capability required to view the Form. OPTIONS: 'logged_out', 'read', 'edit_posts', 'edit_others_posts', 'edit_users', 'edit_pages', 'edit_plugins'. Default 'logged_out'.
	);

    /**
     * Handles the add post shortcode
     *
     * @param array $atts
     */
    public function shortcode( $atts ) {

		// Allow filtering of shortcode attributes before rendering
		$atts = apply_filters( $this->shortcode . '_atts', $atts, $id );

		// Extract shortcode attributes into individual vars and also store as an array 
		extract( $atts = shortcode_atts( $this->defaults, $atts ) );

		// Check for required shortcode attributes
		$msg = $this->has_req_attrs( $atts );
		if ( $msg !== true )
			return $msg;

        // Check if current user has proper privileges to view
		if ( !$this->current_user_has_cap($viewcap) ) :
			echo 'You do not have sufficient permissions to access this content.';
			return;
		endif;

        // Check if a record ID was passed
		if ( !empty( $_GET['postID'] ) ) :
			$itemID = $_GET['postID'];  // Set the post ID
			$item = get_post( $itemID );
		endif;

		// Build the shortcode output hrml string
		if ( isset($itemID) ) :
			global $wpdb;
			?>
			<form id="change_record_type" class="form-inline" method="post">
				<input type="hidden" name="web_apper_item_id" value="<?php echo $itemID; ?>">
				<div class="form-group">
					<select id="record_type" name="web_apper_posttype" class="input-med">
						<?php
						$post_types  = web_apper_posttype_get_results( "SELECT posttype_singularname, posttype_slug FROM {$wpdb->prefix}web_apper_posttypes" );
						foreach ( $post_types as $post_type ) :
							if ( $item->post_type == $post_type['posttype_slug'] ) :
								echo '<option>Change ' . $post_type['posttype_singularname'] . ' to</option>';
							endif;
						endforeach;
						foreach ( $post_types as $post_type ) :
							if ( $item->post_type !== $post_type['posttype_slug'] ) :
								echo '<option value="' . $post_type['posttype_slug'] . '">' . $post_type['posttype_singularname'] . '</option>';
							endif;
						endforeach;
						?>
					</select>
				</div>
				<button id="changeRecordType" type="submit" class="btn">Submit</button>
			</form>

			<script type="text/javascript">
				jQuery(document).ready(function($) {
					// 'Submit' button click handler
					$('#changeRecordType').live("change", function() {
						$('.alert').remove(); // Close any alerts that may be open
						var data = {
							id: '<?php echo $id; ?>',
							action: 'web_apper<?php echo $this->shortcode; ?>',
							web_apper_action: 'change_record_type',
							web_apper_nonce: '<?php echo wp_create_nonce("AwesomeSauce!87"); ?>',
							web_apper_item_id: '<?php echo $itemID; ?>',
							web_apper_posttype: $('#record_type').val(),
						};
						$.post("<?php echo admin_url('admin-ajax.php'); ?>", data, function(response) {
							var result = jQuery.parseJSON(response);  // Parse response
							jQuery('#change_record_type').prepend(result.htmlalert);  // Show and alert
						});
						return false;
					});
				});
			</script>
			<?php
		else :
			echo 'You must save this record first...';
		endif;
	}

    /**
     * Update a Applicant
     *
     * @since 1.0
     */
	public function change_record_type() {
		// Get the Applicant ID
		$itemID = $_POST['web_apper_item_id'];
		// Set post type
		$result = set_post_type( $itemID, $_POST['web_apper_posttype'] );
		// If post was updated successfully
		if ( $result  ) :
			return json_encode( $this->send_response( 'Record change successful', 'Hurray!', 'alert-success' ) );  // Send Response
		else :
			return json_encode( $this->sendResponse( 'There was a problem changing the record. Please Try again.', 'Oh snap!', 'alert-error', false ) );  // Send Response
		endif;
	}

}

$initialize = new ChangePosttype(); 

?>