<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.


/**
 * Add Posttype
 * 
 * @since WebApper (1.0)
 * @param arr $itemData The Posttype details
 * @return mixed (int) insert ID on success, (bool) false on fail
 */
function web_apper_insert_posttype( $itemData ) {
	$itemData = apply_filters( 'posttype_pre_insert', $itemData ); // Allow filtering of the Posttype data before saving;

	$item = new WebApper\Posttype\Posttype;

	foreach ( $itemData as $key => $val ) :
		$item->$key = $val;
	endforeach;

	if ( $item->save() ) :
		do_action( 'posttype_post_insert', $item ); // Allow Posttype data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Update Posttype
 * 
 * @since WebApper (1.0)
 * @param int $id The Posttype ID
 * @param arr $itemData The Posttype details
 * @return bool true on success, false on failure
 */
function web_apper_update_posttype( $id, $itemData ) {	

	$itemData = apply_filters( 'posttype_pre_update', $itemData ); // Allow filtering of the Posttype data before saving;
	
	$item = new WebApper\Posttype\Posttype( $id );

	foreach ( $itemData as $key => $val ) :
		$item->$key = $val;
	endforeach;

	if ( $item->save() ) :
		do_action( 'posttype_post_update', $item ); // Allow Posttype data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Delete Posttype
 *
 * @since WebApper (1.0)
 * @param int $id The Posttype ID
 * @return bool true on success, false on failure
 */
function web_apper_delete_posttype( $id ) {
	
	do_action( 'posttype_pre_delete', $id ); // Allow Posttype data to be hooked onto
	
	$item = new WebApper\Posttype\Posttype($id);
	
	if ( $item->delete() ) :
		do_action( 'posttype_post_delete', $item ); // Allow Posttype data to be hooked onto
		return true;
	else :
		return false;
	endif;
}

/**
 * Get Posttype
 * 
 * @since WebApper (1.0)
 * @param int $id The Posttype ID
 * @return (obj) The Posttype
 */
function web_apper_get_posttype( $id ) {
	$item = new WebApper\Posttype\Posttype( $id );
	return (object) $item->get_data();
}

/**
 * Get Results
 *
 * @since WebApper (1.0)
 * @return arr Associative array of Posttype objects
*/
function web_apper_posttype_get_results( $query ) {
	return $items = WebApper\Posttype\Posttype::get_results( $query );
}

/**
 * Registers custom post types
 *
 * @since 1.0
 */
function web_apper_posttype_init() {
	// Get CPTs array from site options
	global $wpdb;
	$web_apper_pts  = web_apper_posttype_get_results( "SELECT * FROM {$wpdb->prefix}web_apper_posttypes" );
	// Set CPT args & register
	foreach ( $web_apper_pts as $posttype ) :
		$labels = array(
			'name' => $posttype['posttype_name'],
			'singular_name' => $posttype['posttype_singularname'],
			'add_new' => 'Add New',
			'add_new_item' => 'Add New ' . $posttype['posttype_singularname'],
			'edit_item' => 'Edit ' . $posttype['posttype_singularname'],
			'new_item' => 'New ' . $posttype['posttype_singularname'],
			'all_items' => 'All ' . $posttype['posttype_name'],
			'view_item' => 'View ' . $posttype['posttype_singularname'],
			'search_items' => 'Search ' . $posttype['posttype_name'],
			'not_found' =>  'No ' . strtolower( $posttype['posttype_name'] ) . ' found',
			'not_found_in_trash' => 'No ' . strtolower( $posttype['posttype_name'] ) . ' found in Trash', 
			'menu_name' => $posttype['posttype_name']
		);
		$args = array(
			'labels' => $labels,
			'public' => true,
			'exclude_from_search' => $posttype['posttype_excludefromsearch'],
			'show_ui' => $posttype['posttype_showinadmin'] == 1 ? true : false, 
			'menu_position' =>  intval( $posttype['posttype_adminmenuposition'] ),
			'show_in_admin_bar' =>  $posttype['posttype_showinadminbar'],
			'show_in_nav_menus' =>  $posttype['posttype_showinnavmenus'],
			'rewrite' => array( 'slug' => $posttype['posttype_slug'] ),
			'has_archive' => $posttype['posttype_archive'], 
			'supports' => array( 'title', 'author', 'custom-fields' )
		); 
		if ( $posttype['posttype_revisions'] ) : 
			$args['supports'][] = 'revisions';
		endif;
		register_post_type( $posttype['posttype_slug'], $args );
	endforeach;
}
add_action( 'init', 'web_apper_posttype_init' );