<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.

namespace PFBC\Element;

class Posttype extends Select {
	public function __construct($label, $name, array $properties = null) {
		$options = array('' => '');
		$value = $properties['value'];
		
		global $wpdb;
		$posttypes = \web_apper_posttype_get_results( "SELECT ID, posttype_name FROM {$wpdb->prefix}web_apper_posttypes" );
		if ( !empty($posttypes) ) :
			foreach ( $posttypes as $posttype ) : // Foreach role
				$options[$posttype['posttype_slug']] = $posttype['posttype_name'];
			endforeach;
		endif;
		parent::__construct($label, $name, $options, $properties);
    }
}