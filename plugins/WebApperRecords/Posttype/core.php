<?php

// TODO: Complete documention in this file. Do not remove this marker until file is fully documented.

/*
WebApper Moddule Name: Posttypes
*/

namespace WebApper\Posttype;

// Include Module files
require_once 'class.php';
require_once 'api.php';
foreach( glob ( dirname(__FILE__) . '/Shortcode/*.php' ) as $filename ) {
	require_once( $filename );
}
foreach( glob ( dirname(__FILE__) . '/Element/*.php' ) as $filename ) {
	require_once( $filename );
}


// Add all hooks at WordPress init
add_filter( 'init', function () {
	
	// Hook into plugin activation to install Module DB table
	add_filter( 'web_apper_create_table_sql', function ( $sql_array ) {
		global $wpdb;
		$sql_array[] = "CREATE TABLE `{$wpdb->prefix}web_apper_posttypes` (
			`ID` SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL,
			`posttype_name` VARCHAR(100),
			`posttype_singularname`  VARCHAR(100),
			`posttype_slug`  VARCHAR(100),
			`posttype_excludefromsearch` TINYINT(1) UNSIGNED NOT NULL,
			`posttype_archive` TINYINT(1) UNSIGNED NOT NULL,
			`posttype_revisions` TINYINT(1) UNSIGNED NOT NULL,
			`posttype_showinadmin` TINYINT(1) UNSIGNED NOT NULL,
			`posttype_adminmenuposition` TINYINT UNSIGNED NOT NULL,
			`posttype_showinadminbar` TINYINT(1) UNSIGNED NOT NULL,
			`posttype_showinnavmenus` TINYINT(1) UNSIGNED NOT NULL,
			`posttype_singleview_url` VARCHAR(100) NOT NULL,
			UNIQUE INDEX (posttype_slug),
			PRIMARY KEY (ID)
		);";
		return $sql_array;
	}, 2, 1 );
	
	add_action( 'web_apper_insert_table_rows', function () {
		global $wpdb;
		// Deafult Fields
		$wpdb->query( "INSERT INTO `{$wpdb->prefix}web_apper_fields` (`field_name`, `field_id`, `field_type`, `field_attributes`, `field_required`, `field_placeholder`, `field_short_desc`, `field_long_desc`, `field_options`, `field_default_value`, `field_validation`, `field_regex`, `field_error_message`, `field_dt_show_col_default`, `field_dt_format_value`, `field_dt_filter_type`, `field_dt_filter_options`, `field_dt_filter_value`, `field_bulk_edit`, `field_read_only`, `field_form_only`, `field_index_only`, `field_dynamic_value_flow_id`, `field_action_flow_id`, `field_form_flow_id`, `core_item`) VALUES
			('Name', 'posttype_name', 'Textbox', '', 1, '', '', 'A plural name for the custom post type. e.g., Oppertunities, not Oppertunity.', '', '', '', '', '', 1, '', 'search', '', '', 0, 0, 0, 0, '', '', '', 1),
			('Singular Name', 'posttype_singularname', 'Textbox', '', 1, '', '', 'A singular name for the custom post type. e.g., Lead, not Leads.', '', '', '', '', '', 1, '', 'search', '', '', 0, 0, 0, 0, '', '', '', 1),
			('Slug', 'posttype_slug', 'Textbox', '', 1, '', 'A unique ID for the custom post type', 'Must be alphanumeric: contain only lowercase letters, numbers, underscores(_) and/or hyphens(-). Note: will be used as the custom post type slug. Cannot be changed once saved.', '', '', '', '', '', 1, '', 'search', '', '', 0, 0, 0, 0, '', '', '', 1),
			('Exclude from search', 'posttype_excludefromsearch', 'YesNo', '', 0, '', '', 'Whether to exclude posts with this post type from front end search results.', '', '0', '', '', '', 1, 'bool_to_text', 'equals', '1,Yes|0,No', '', 0, 0, 0, 0, '', '', '', 1),
			('Archive', 'posttype_archive', 'YesNo', '', 0, '', '', 'Whether to enable post type archives', '', '0', '', '', '', 1, 'bool_to_text', 'equals', '1,Yes|0,No', '', 0, 0, 0, 0, '', '', '', 1),
			('Revisions', 'posttype_revisions', 'YesNo', '', 0, '', '', 'Whether to enable post revisions.', '', '0', '', '', '', 1, 'bool_to_text', 'equals', '1,Yes|0,No', '', 0, 0, 0, 0, '', '', '', 1),
			('Show in WP Admin menu', 'posttype_showinadmin', 'YesNo', '', 0, '', '', 'Whether to make this post type available in the WP Admin menu.', '', '0', '', '', '', 1, 'bool_to_text', 'equals', '1,Yes|0,No', '', 0, 0, 0, 0, '', '', 'posttype-showadmin', 1),
			('WP Admin menu position', 'posttype_adminmenuposition', 'Number', '', 0, '', '', 'The position in the menu order the post type should appear, 5 is below \'Posts\'', '', '', '', '', '', 1, 'bool_to_text', 'range', '', '', 0, 0, 0, 0, '', '', '', 1),
			('Show in admin bar', 'posttype_showinadminbar', 'YesNo', '', 0, '', '', 'Whether to make this post type available in the WordPress admin bar.', '', '0', '', '', '', 1, 'bool_to_text', 'equals', '1,Yes|0,No', '', 0, 0, 0, 0, '', '', '', 1),
			('Show in nav menus', 'posttype_showinnavmenus', 'YesNo', '', 0, '', '', 'Whether post_type is available for selection in navigation menus.', '', '0', '', '', '', 1, 'bool_to_text', 'equals', '1,Yes|0,No', '', 0, 0, 0, 0, '', '', '', 1)
			('Workspace', 'posttype_singleview_url', 'Textbox', '', 1, '', '', 'A default Workspace to use for viewing a single record of the type.', '', '', '', '', '', 1, '', 'search', '', '', 0, 0, 0, 0, '', '', '', 1),
		;" );
		// Deafult Conditionals
		$wpdb->query( "INSERT INTO `{$wpdb->prefix}web_apper_conditions` (`condition_name`, `condition_id`, `condition_left_type`, `condition_left_side`, `condition_operator`, `condition_right_type`, `condition_right_side`, `core_item`) VALUES
			('Posttype Gen - Show Admin equals Yes', 'posttype-showadmin-equals-yes', 'field', 'posttype_showinadmin', 'is greater than', 'static', '0', 1)
		;" );
		// Deafult Flows
		$wpdb->query( "INSERT INTO `{$wpdb->prefix}web_apper_flows` (`flow_name`, `flow_id`, `flow_type`, `flow_code`, `flow_hook`, `core_item`) VALUES
			('Posttype Gen - Show Admin', 'posttype-showadmin', 'form', 'IF posttype-showadmin-equals-yes SHOW posttype_adminmenuposition', '', 1)
		;" );
	}, 1, 0 );

	// Add custom field type to PFBC
	add_action( 'web_apper_add_form_field', function ( $form, $label, $name, $options, $additionalParams ) {
		if ( $field['field_type'] = 'Posttype' ) :
			$form->addElement( new \PFBC\Element\Posttype($label, $name, $additionalParams) ); // Captcha
		endif;
	}, 1, 5 );	

}, 1, 0 );
