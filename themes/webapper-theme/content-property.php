<?php
/**
 * Theme: WebApper Theme
 * 
 * The template used for displaying page content in page.php
 * everything after the_content()
 *
 * @package WebApper/Theme
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="entry-content">

        <?php
        $thumbnail_urls = unserialize( get_post_meta( get_the_ID(), 'property_thumbnail_urls', true ) );
        if ( !empty( $thumbnail_urls ) ) {
            foreach ( $thumbnail_urls as $thumbnail ) {
                echo '<div class="col-md-3">' .
           '<image src="' . $thumbnail . '">' .
                     '</div>';
            }
        }
        ?>
		
        <div class="clearfix"></div>

        <div class="well property_meta">
            <ul>
            <?php
            $property_details = array(
                'property_type' 	 => 'Type',
                'property_sqft' 	 => 'SQFT',
                'property_bedrooms'  => 'Bedrooms',
                'property_bathrooms' => 'Bathrooms',
                'property_address' 	 => 'Address',
            );
		
            foreach ( $property_details as $key => $label ) :
                $value = get_post_meta( get_the_ID(), $key, true );
                
                if ( !empty( $value ) ) :
                    echo '<li><strong>' . $label . ':</strong> ' . $value . '</li>';
                endif;
            endforeach;
				?>
				<div class="clearfix"></div>
			</ul>
		</div>
		
		<?php the_content(); ?>
		
		<div class="well property_meta">

			<h3>Features:</h3>

			<?php
		        $property_meta = array(
		            'property_living_room' 		  => 'Living room',
		            'property_dining_room' 		  => 'Dining room',
		            'property_walk-in_closet' 		  => 'Walk-in closet',
		            'property_master_bath' 		  => 'Master bath',
		            'property_family_room' 		  => 'Family room',
		            'property_storage_space'  		  => 'Storage space',
		            'property_breakfast_nook'             => 'Breakfast nook',
		            'property_pantry'  			  => 'Pantry',
		            'property_office'  			  => 'Office ',
		            'property_basement'  		  => 'Basement',
		            'property_recreation_room' 		  => 'Recreation room',
		            'property_attic'  			  => 'Attic ',
		            'property_workshop' 		  => 'Workshop',
		            'property_range_oven' 		  => 'Range / Oven',
		            'property_refrigerator'  		  => 'Refrigerator',
		            'property_dishwasher'  		  => 'Dishwasher ',
		            'property_microwave'  		  => 'Microwave',
		            'property_garbage_disposal'           => 'Garbage disposal',
		            'property_stainless_steel_appliances' => 'Stainless steel appliances ',
		            'property_freezer'                    => 'Freezer',
		            'property_trash_compactor'            => 'Trash compactor',
		            'property_private_pool'  		  => 'Private pool',
		            'property_balcony,_deck,_or_patio'    => 'Balcony, Deck, or Patio',
		            'property_yard_fenced_yard'  	  => 'Yard Fenced yard',
		            'property_lawn'  			  => 'Lawn',
		            'property_heat:_forced_air' 	  => 'Heat: forced air',
		            'property_central_a/c' 		  => 'Central A/C',
		            'property_air_conditioning'  	  => 'Air conditioning ',
		            'property_ceiling_fans'  		  => 'Ceiling fans',
		            'property_double_pane_storm_windows'  => 'Double pane / Storm windows',
		            'property_cable-ready'  		  => 'Cable-ready',
		            'property_high-speed_internet'        => 'High-speed internet',
		            'property_hardwood_floor' 		  => 'Hardwood floor',
		            'property_tile_floor'  		  => 'Tile floor',
		            'property_granite_countertop'  	  => 'Granite countertop ',
		            'property_fireplace'  		  => 'Fireplace',
		            'property_high_vaulted_ceiling'	  => 'High / Vaulted ceiling',
		            'property_jacuzzi_whirlpool'  	  => 'Jacuzzi / Whirlpool ',
		            'property_sauna'  			  => 'Sauna',
		            'property_hot_tub_spa'  		  => 'Hot tub / Spa',
		            'property_skylights' 		  => 'Skylights ',
		            'property_jetted_tub' 		  => 'Jetted tub',
		            'property_wet_bar' 			  => 'Wet bar',
		            'property_shared_pool' 		  => 'Shared pool',
		            'property_garage_attached' 		  => 'Garage - Attached',
		            'property_rv_parking_garage_detached' => 'RV parking Garage - Detached',
		        );

			$icon = '<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span> ';
			
			echo '<ul>';

			foreach ( $property_meta as $key => $label ) :
	            $show = get_post_meta( get_the_ID(), $key, true );
	            
	            if ( !empty( $show ) ) 
	            echo '<li>' . $icon . $label . '</li>';
	        endforeach;

			echo '<div class="clearfix"></div>';
			echo '</ul>';
			?>

		</div>
			
		<div class="property_meta">
			<?php
			$api_key	= 'AIzaSyAyiB-YUa9LNw3iXqDDxFBxz-HS_zvhckk';
			$lat		= get_post_meta( get_the_ID(), 'property_lat', true );
			$lng		= get_post_meta( get_the_ID(), 'property_lng', true );
			$heading	= get_post_meta( get_the_ID(), 'property_heading', true );
			$address = get_post_meta( get_the_ID(), 'property_address', true );
			$address = str_replace( ' ', '+', $address );
			$place_map_url   = "https://www.google.com/maps/embed/v1/place?key=$api_key" . 
						       "&q=". $address .
							   "&attribution_source=Google+Maps+Embed+API" . 
							   "&attribution_ios_deep_link_id=comgooglemaps:\/\/?daddr=Butchart+Gardens+Victoria+BC" . $address;
			$street_view_url = "https://www.google.com/maps/embed/v1/streetview?key=$api_key" .
							   "&location=$lat,$lng&heading=$heading";
			?>

			<div class="col-md-6">
				<iframe width="600" height="450" frameborder="0" style="border:0" src="<?php echo $street_view_url; ?>"></iframe>
			</div>

			<div class="col-md-6">
				<iframe width="600" height="450" frameborder="0" style="border:0" src="<?php echo $place_map_url; ?>"></iframe>
			</div>

		</div>
		
		<?php edit_post_link( __( '<span class="glyphicon glyphicon-edit"></span> Edit', 'webapper-theme' ), '<footer class="entry-meta"><div class="edit-link hidden-xs hidden-sm">', '</div></footer>' ); ?>

	</div><!-- .entry-content -->
	
</article><!-- #post-## -->
