<?php
/**
 * Theme: WebApper Theme
 * 
 * This template is called from other page and archive templates to display the content
 * header, which is essentially the header for the page. If there is a featured image,
 * it displays that with the page title and subtitle/description overlaid on it.
 * Otherwise, it just displays the text on a colored background.
 *
 * @package webapper-theme
 */


if ( have_posts() ) :

	 // Initialize variables we need
	 $image_url = $image_width = $image_type = null;
	 $title = $subtitle = $description = null;
	 
	// Check featured image, if there is one
	if ( is_singular() AND has_post_thumbnail() ) {
		$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
		$image_width    = $featured_image[1];
		$image_url      = $featured_image[0];
		$image_type     = 'featured';
	}

	/* 
	 * Get the text to display on the image or content header section
	 */
	 
	// If home page is blog and we are on the home page
	if ( is_home() AND is_front_page() ) {
		$title    = __( 'Blog', 'webapper-theme' );
		$subtitle = __( 'latest posts', 'webapper-theme' );

	// If home page is static and we are on the blog page
	} elseif ( is_home() AND ! is_front_page() ) {
		$home_page = get_option ( 'page_for_posts' );
		if ( $home_page ) $post = get_post( $home_page );
		if ( $post ) {
			$title = $post->post_title;
		} else {
			$title = __( 'Blog', 'webapper-theme' );
		}
		$subtitle = get_post_meta( $home_page, '_subtitle', $single = true );
		if ( empty($subtitle) ) {
			$subtitle = __( 'latest posts', 'webapper-theme' );
		}

	// Jetpack Portfolio Archive
	} elseif ( is_post_type_archive( 'property' ) ) {
		$title = __( 'Properties', 'webapper-theme' );

	// Jetpack Portfolio Archive
	} elseif ( is_post_type_archive( 'jetpack-portfolio' ) OR is_tax ( 'jetpack-portfolio-type' ) OR is_tax ( 'jetpack-portfolio-tag' ) ) {
		$title = __( 'Portfolio', 'webapper-theme' );
		if ( is_tax( 'jetpack-portfolio-type' ) || is_tax( 'jetpack-portfolio-tag' ) ) {
			$subtitle = single_term_title( null, false );
		}
	
	// Jetpack Testimonial Archive
	} elseif ( is_post_type_archive( 'jetpack-testimonial' ) OR $post->post_type == 'jetpack-testimonial' ) {
		$testimonial_options = get_theme_mod( 'jetpack_testimonials' );
		if ( $testimonial_options ) { 
			$title = $testimonial_options['page-title'];
		} else {
			$title = __( 'Testimonials', 'webapper-theme' );
		}
		if ( !is_post_type_archive( 'jetpack-testimonial' ) AND $post->post_type == 'jetpack-testimonial' ) {
			$subtitle = get_the_title();
		}

	// If page or single post
	} elseif ( is_page() OR is_single() ) {
		$title = get_the_title();
		// $subtitle set below
			
	} elseif ( is_category() ) {
		$title = single_cat_title( null, false );
		// $subtitle set below

	} elseif ( is_tag() ) {
		$title = single_tag_title( null, false );
		// $subtitle set below

	} elseif ( is_author() ) {
		// Queue the first post, that way we know what author we're dealing with
		the_post();
		$title       = __( 'Author', 'webapper-theme' );
		$subtitle    = get_the_author();
		$description = get_the_author_meta('description');
		// Since we called the_post() above, we need to rewind the loop back to the beginning that way we can run the loop properly, in full.
		rewind_posts();

	} elseif ( is_search() ) {
		$title    = __( 'Search Results', 'webapper-theme' );
		$subtitle = get_search_query();

	} elseif ( is_day() ) {
		$title    = __( 'Archive', 'webapper-theme' );
		$subtitle = get_the_date();

	} elseif ( is_month() ) {
		$title    = __( 'Archive', 'webapper-theme' );
		$subtitle = get_the_date( 'F Y' );

	} elseif ( is_year() ) {
		$title    = __( 'Archive', 'webapper-theme' );
		$subtitle = get_the_date( 'Y' );

	} elseif ( is_tax( 'post_format', 'post-format-aside' ) ) {
		$title    = __( 'Archive', 'webapper-theme' );
		$subtitle = __( 'Asides', 'webapper-theme' );

	} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
		$title    = __( 'Archive', 'webapper-theme' );
		$subtitle = __( 'Images', 'webapper-theme');

	} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
		$title    = __( 'Archive', 'webapper-theme' );
		$subtitle = __( 'Videos', 'webapper-theme' );

	} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
		$title    = __( 'Archive', 'webapper-theme' );
		$subtitle = __( 'Quotes', 'webapper-theme' );

	} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
		$title    = __( 'Archive', 'webapper-theme' );
		$subtitle = __( 'Links', 'webapper-theme' );

	} else {
		$title = __( 'Archive', 'webapper-theme' );

	} //endif is_home()

	/*
	 * If Title then get Subtitle, first from the term description, then from our custom
	 * page title
	 */
	if ( $title AND ! $subtitle ) {
		$subtitle = term_description();
		$subtitle = str_replace( array('<p>','</p>'), '', $subtitle );
		if ( ! $subtitle ) $subtitle = get_post_meta( get_the_ID(), '_subtitle', $single = true );
	}
		
	/* 
	 * Output Header, as a text image overlay or as text
	 */
	if ( $image_url ) :
		// Display the image and text
		?>
		<header class="content-header-image">
			<div class="section-image" style="background-image: url('<?php echo $image_url; ?>')">
				<div class="section-image-overlay">
				<h1 class="header-image-title"><?php echo $title; ?></h1>
				<?php if ( $subtitle ) echo '<h2 class="header-image-caption">' . $subtitle . '</h2>'; ?>
				<?php if ( $description ) echo '<p class="header-image-description">' . $description . '</p>'; ?> 
				<?php				
				// Only for static home page, show a scroll down icon
				if ( is_front_page() ) {
					echo '<div class="spacer"></div>';
					echo '<a href="#primary" class="scroll-down smoothscroll"><span class="glyphicon glyphicon-chevron-down"></span></a>';
				}
				?>
				</div><!-- .cover-image-overlay or .section-image-overlay -->
			</div><!-- .cover-image or .section-image -->
		</header><!-- content-header-image -->
		<?php

	elseif ( $title ) :
		// Display the text
		?>
		<header class="content-header">
			<div class="container-fluid">
				<h1 class="page-title"><?php echo $title; ?></h1>
				<?php if ( $subtitle ) printf( '<h3 class="page-subtitle taxonomy-description">%s</h3>', $subtitle ); ?>
				<?php if ( $description ) printf( '<h4 class="page-description">%s</h4>', $description ); ?>
			</div><!-- .container-fluid -->
		</header><!-- .content-header -->
		<?php 

	endif; // if $image_url ?>

<?php endif; // have_posts() ?>
