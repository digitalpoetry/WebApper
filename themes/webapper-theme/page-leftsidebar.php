<?php
/**
 * Theme: WebApper Theme
 * 
 * Template Name: Page - Left Sidebar
 *
 * Page with the sidebar on the left instead of the right
 *
 * This template displays pages with the sidebar on the left.
 *
 * @package WebApper/Theme
 */

get_header(); ?>

	<?php get_template_part( 'content', 'header' ); ?>

	<?php get_sidebar( 'pagetop' ); ?>

	<div class="container-fluid">
		<div id="main-grid" class="row">

			<?php get_sidebar(); // col-sm-3 ?>

			<div id="primary" class="content-area col-sm-9">
				<main id="main" class="site-main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'page' ); ?>

						<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || '0' != get_comments_number() ) :
						?>
						<div class="comments-wrap">
							<?php comments_template(); ?>
						</div><!-- .comments-wrap" -->
						<?php endif; ?>

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->
			</div><!-- #primary -->

		</div><!-- .row -->
	</div><!-- .container-fluid -->

<?php get_sidebar( 'pagebottom' ); ?>
		
<?php get_sidebar( 'footer' ); ?>

<?php get_footer(); ?>
