<?php
/**
 * Theme: WebApper Theme
 * 
 * The template for displaying Search Results pages.
 *
 * @package WebApper/Theme
 */

get_header(); ?>

	<?php get_template_part( 'content', 'header' ); ?>

	<?php get_sidebar( 'pagetop' ); ?>

	<div class="container-fluid">
		<div id="main-grid" class="row">

			<div id="primary" class="content-area col-sm-9">
				<main id="main" class="site-main" role="main">

					<?php if ( have_posts() ) : ?>
			
						<?php // Start the Loop ?>
						<?php while ( have_posts() ) : the_post(); ?>
			
							<?php //get_template_part( 'content', 'search' ); ?>
							<?php get_template_part( 'content', 'page-posts' ); ?>
			
						<?php endwhile; ?>
			
						<?php get_template_part( 'content', 'index-nav' ); ?>
			
					<?php else : ?>
			
						<?php get_template_part( 'no-results', 'search' ); ?>
			
					<?php endif; ?>

				</main><!-- #main -->
			</div><!-- #primary -->

			<?php get_sidebar(); // col-sm-3 ?>

		</div><!-- .row -->
	</div><!-- .container-fluid -->

<?php get_sidebar( 'pagebottom' ); ?>
		
<?php get_sidebar( 'footer' ); ?>

<?php get_footer(); ?>
