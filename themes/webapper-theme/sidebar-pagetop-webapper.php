<?php
/**
 * Theme: WebApper Theme
 * 
 * The page bottom sidebar on WebApper templates. The sidebar for the top of the
 * page (after the nav bar).
 *
 * @package WebApper/Theme
 */


// If the sidebar has widgets, then display them
$sidebar_pagetop = get_dynamic_sidebar( 'WebApper Page Top' );
if ( $sidebar_pagetop ) :
	?>
	<div id="sidebar-pagetop-webapper" class="sidebar-page sidebar-page-top hidden-md hidden-lg">
		<div class="container-fluid">
			<div class="row">
				<?php echo $sidebar_pagetop; ?>
			</div><!-- .row -->
		</div><!-- .container-fluid -->
	</div><!-- .sidebar-pagetop -->
	<?php
endif;
