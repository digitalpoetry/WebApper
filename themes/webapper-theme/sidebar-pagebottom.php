<?php
/**
 * Theme: WebApper Theme
 * 
 * The "sidebar" for the bottom of the page (before the widgetized footer area).
 *
 * @package WebApper/Theme
 */


// If the sidebar has widgets, then display them
$sidebar_pagebottom = get_dynamic_sidebar( 'Page Bottom' );
if ( $sidebar_pagebottom ) :
	?>
	<div id="sidebar-pagebottom" class="sidebar-page sidebar-page-bottom visible-xs visible-xs-block">
		<div class="container-fluid <?php echo get_post_meta( get_the_ID(), 'theme_content_width', true ); ?>">
			<div class="row">
				<?php echo $sidebar_pagebottom; ?>
			</div><!-- .row -->
		</div><!-- .container-fluid -->
	</div><!-- .sidebar-pagebottom -->
	<?php
endif;
