<?php
/**
 * Theme: WebApper Theme
 * 
 * Functions file for child theme. If you want to make a lot more changes than what is
 * included here, consider downloading the WebApper Theme Developer child theme. It 
 * shows how to do more complex function overrides, like choosing more theme features to
 * include or exclude, loading up custom CSS or javascript, etc.
 *
 * @package WebApper/Theme-child
 */
 
/**
 * SET THEME OPTIONS HERE
 *
 * Theme options can be overriden here. These are all set the same defaults as in the 
 * parent theme, but listed here so you can easily change them. Just uncomment (remove
 * the //) from any lines that you change.
 * 
 * Parameters:
/**
 * Theme options. Can override in child theme. For theme developers, this is array so 
 * you can add these items to the customizer and store them all as a single options entry.
 * 
 * Parameters:
 * background_color - Hex code for default background color without the #. eg) ffffff
 * 
 * content_width - Only for determining "full width" image. Actual width in Bootstrap.css
 * 		is 1170 for screens over 1200px resolution, otherwise 970.
 * 
 * embed_video_width - Sets the maximum width of videos that use the <embed> tag. The
 * 		default is 1170 to handle full-width page templates. If you will ALWAYS display
 * 		the sidebar, can set to 600 for better performance.
 * 
 * embed_video_height - Leave empty to automatically set at a 16:9 ratio to the width
 * 
 * post_formats - Array of WordPress extra post formats. i.e. aside, image, video, quote,
 * 		and/or link
 * 
 * touch_support - Whether to load touch support for carousels (sliders)
 * 
 * fontawesome - Whether to load font-awesome font set or not
 * 
 * bootstrap_gradients - Whether to load Bootstrap "theme" CSS for gradients
 * 
 * navbar_classes - One or more of navbar-default, navbar-inverse, navbar-fixed-top, etc.
 * 
 * custom_header_location - If 'header', displays the custom header above the navbar. If
 * 		'content-header', displays it below the navbar in place of the colored content-
 *		header section.
 * 
 * image_keyboard_nav - Whether to load javascript for using the keyboard to navigate
 		image attachment pages
 * 
 * sample_widgets - Whether to display sample widgets in the footer and page-bottom widet
 		areas.
 * 
 * sample_footer_menu - Whether to display sample footer menu with Top and Home links
 * 
 * testimonials - Whether to activate testimonials custom post type if Jetpack plugin is active
 *
 * NOTE: $theme_options is being deprecated and replaced with $xsbf_theme_options. You'll
 * need to update your child themes.
 */
$xsbf_theme_options = array(
	//'background_color' 		=> 'f2f2f2',
	//'content_width' 			=> 1170,
	//'embed_video_width' 		=> 1170,
	//'embed_video_height' 		=> null, // i.e. calculate it automatically
	//'post_formats' 			=> null,
	//'touch_support' 			=> true,
	//'fontawesome' 			=> true,
	//'bootstrap_gradients' 	=> false,
	//'navbar_classes'			=> 'navbar-default navbar-static-top',
	//'custom_header_location' 	=> 'header',
	//'image_keyboard_nav' 		=> true,
	//'sample_widgets' 			=> true, // for possible future use
	//'sample_footer_menu'		=> true,
	//'testimonials'			=> true
);


/**
 * Disable the WordPress admin bar on the frontend.
 */
/*if ( ! current_user_can( 'manage_options' ) ) {
    show_admin_bar( false );
}*/
show_admin_bar( false );


/**
 * Set up theme defaults and register support for various WordPress features.
 */
function webapper_theme_setup() {

	// Disable the WordPress custom header feature.
	remove_theme_support( 'custom-header' );

	// Make theme available for translation. Translations can be filed in the /languages/
	// directory. If you want to translate this theme, please contact me!
	//load_theme_textdomain( 'webapper-theme', get_template_directory() . '/languages' );

} // end function
add_action( 'after_setup_theme', 'webapper_theme_setup', 100, 0 );


/**
 * Register widgetized areas. Override the parent theme function.
 */
function xsbf_widgets_init() {

	// Register main sidebar with corrected description.
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'webapper-theme' ),
		'id'            => 'sidebar-1',
		'description' 	=> __( 'Main sidebar for widgets. Hidden on extra small devices (Phones < 768px).', 'webapper-theme' ), // Corrected description.
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
		'after_widget' 	=> '</aside><!-- .widget -->',
	) );

	// Register footer sidebar with 4 columns.
	register_sidebar( array(
		'name' 			=> __( 'Footer', 'webapper-theme' ),
		'id' 			=> 'sidebar-2',
		'description' 	=> __( 'Optional site footer widgets. Add 1-4 widgets here to display in columns. Hidden on extra small devices (Phones < 768px).', 'webapper-theme' ),
		'before_widget' => '<aside id="%1$s" class="widget col-sm-3 clearfix %2$s">', // Corrected html class.
		'after_widget' 	=> "</aside>",
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
		'after_widget' 	=> '</aside><!-- .widget -->',
	) );

	// Page Top (After Header) Widget Area. Single column.
	register_sidebar( array(
		'name' 			=> __( 'Page Top', 'webapper-theme' ),
		'id' 			=> 'sidebar-3',
		'description' 	=> __( 'Optional widget section after the header. This is a single column area that spans the full width of the page. Visible only on extra small devices (Phones < 768px).', 'webapper-theme' ),
		'before_widget' => '<aside id="%1$s" class="widget visible-xs-block col-xs-12 clearfix %2$s">', // Corrected html class.
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
		'after_widget' 	=> '</aside><!-- .widget -->',
	) );

	// Page Bottom (Before Footer) Widget Area. Single Column.
	register_sidebar( array(
		'name' 			=> __( 'Page Bottom', 'webapper-theme' ),
		'id' 			=> 'sidebar-4',
		'description' 	=> __( 'Optional widget section before the footer. This is a single column area that spans the full width of the page. Visible only on extra small devices (Phones < 768px).', 'webapper-theme' ),
		'before_widget' => '<aside id="%1$s" class="widget visible-xs-block col-xs-12 clearfix %2$s">', // Corrected html class.
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
		'after_widget' 	=> '</aside><!-- .widget -->',
	) );

	// Add extra main sidebar for this child theme
	register_sidebar( array(
		'name'          => __( 'WebApper Sidebar', 'webapper-theme' ),
		'id'            => 'sidebar-5',
		'description' 	=> __( 'Main sidebar for widgets on WebApper page templates. Hidden on extra small devices (Phones < 768px).', 'webapper-theme' ),
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
		'after_widget' 	=> '</aside><!-- .widget -->',
	) );

	// Add extra main Page Top for this child theme
	register_sidebar( array(
		'name' 			=> __( 'WebApper Page Top', 'webapper-theme' ),
		'id' 			=> 'sidebar-6',
		'description' 	=> __( 'Optional widget section after the header. This is a single column area that spans the full width of the page. Visible only on (extra) small devices (Tablets < 992px).', 'webapper-theme' ),
		'before_widget' => '<aside id="%1$s" class="widget visible-xs-block col-xs-12 clearfix %2$s">', // Corrected html class.
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
		'after_widget' 	=> '</aside><!-- .widget -->',
	) );

	// Add extra Page Bottom sidebar for this child theme
	register_sidebar( array(
		'name' 			=> __( 'WebApper Page Bottom', 'webapper-theme' ),
		'id' 			=> 'sidebar-7',
		'description' 	=> __( 'Optional widget section before the footer. This is a 2 column area on small devices and single column on extra small devices. Visible only on (extra) small devices (Tablets < 992px).', 'webapper-theme' ),
		'before_widget' => '<aside id="%1$s" class="widget visible-xs-block col-sm-6 clearfix %2$s">', // Corrected html class.
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
		'after_widget' 	=> '</aside><!-- .widget -->',
	) );

} //end function
add_action( 'widgets_init', 'xsbf_widgets_init' );


/* Override the parent theme function. Return false since this child theme has
 * containers already
 */
function xsbf_is_fullwidth() {
	return false;
}


/* Remove some of the parent theme page templates.
 */
function webapper_remove_page_template( $pages_templates ) {
    unset( $pages_templates['page-fullwithsubpages.php'] );
    unset( $pages_templates['page-fullwithposts.php'] );
    unset( $pages_templates['page-fullpostsnoheader.php'] );
    return $pages_templates;
}
add_filter( 'theme_page_templates', 'webapper_remove_page_template' );

/**
 * Add custom section to WordPress page / post editor
 */
if ( ! function_exists( 'theme_content_width_add_custom_box' ) ) :
add_action( 'add_meta_boxes', 'theme_content_width_add_custom_box' );
function theme_content_width_add_custom_box() {
    $screens = array( 'page', 'post', 'property' );
    foreach ( $screens as $screen ) {
        add_meta_box(
            'theme_content_width_options',
            __( 'Content Width', 'webapper-theme' ),
            'theme_content_width_inner_custom_box',
            $screen,
            'normal',
            'low'
        );
    }
}
endif; // end ! function_exists

/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current page / post.
 */
if ( ! function_exists( 'theme_content_width_inner_custom_box' ) ) :
function theme_content_width_inner_custom_box( $post ) {

	// Add an nonce field so we can check for it later.
	wp_nonce_field( 'theme_content_width_inner_custom_box', 'theme_content_width_inner_custom_box_nonce' );

    $theme_content_width_options = array(
        'fluid' 	 => 'Fluid width, content will span the width of the browser on all devices.',
        'fixed' 	 => 'Fixed width, content will have a fixed width on medium to large devices.',
        'fixed_wide' => 'Fixed width (wide), content will have a wide fixed width on large devices.',
    );

    foreach ( $theme_content_width_options as $option => $label ) :
        $value = get_post_meta( $post->ID, 'theme_content_width', true );

        if ( $value == $option ) :
             $checked = 'checked ';
        else :
             $checked = '';
        endif;

        echo '<input type="radio" name="theme_content_width" value="' . $option . '" ' . $checked . '/> ';
        echo '<label for="theme_content_width">';
                 _e( $label, 'webapper_theme' );
        echo '</label><br />';
    endforeach;
}
endif; // end ! function_exists

/**
 * When the property is saved, saves our custom data.
 *
 * @todo Finish this function
 * 
 * @param int $theme_content_width_id The ID of the post being saved.
 */
if ( ! function_exists( 'theme_content_width_save_postdata' ) ) :
add_action( 'save_post', 'theme_content_width_save_postdata' );
function theme_content_width_save_postdata( $post_id ) {

	/*
	* We need to verify this came from the our screen and with proper authorization,
	* because save_post can be triggered at other times.
	*/

	// Check if our nonce is set.
	if ( ! isset( $_POST['theme_content_width_inner_custom_box_nonce'] ) )
	return $post_id;

	$nonce = $_POST['theme_content_width_inner_custom_box_nonce'];

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $nonce, 'theme_content_width_inner_custom_box' ) )
	  return $post_id;

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
	  return $post_id;

    update_post_meta( $post_id, 'theme_content_width', $_POST['theme_content_width'] );
}
endif; // end ! function_exists

function webapper_login_script() {
	?>
	<script type="text/javascript">
		jQuery(document).ready(function($){
		   $("#webapper_login_form").submit(function() {
				var data = {
					action: 'webapper_login',
					nonce: '<?php echo wp_create_nonce("webapper-login"); ?>',
					user: $('#webapper_login_form [name="log"]').val(),
					pass: $('#webapper_login_form [name="pwd"]').val()
				};
				$.post("<?php echo admin_url('admin-ajax.php'); ?>", data, function(result) {
					var response = jQuery.parseJSON(result);  // Parse response
					window.location = response.redirect_to;
				});
				return false;
			});
		});
	</script>
	<?php
}
add_action( 'wp_footer', 'webapper_login_script' );

/**
 * Adds ajax callback for the navbar login form
 *
 */
function webapper_login_function() {
	check_ajax_referer( 'webapper-login', 'nonce' );
	$creds = array(
		'user_login' => $_POST['user'],
		'user_password' => $_POST['pass'],
		'remember' => true
	);
	$user = wp_signon( $creds, false);
	if ( is_wp_error($user) ) :
		echo json_encode( array( 'success' => false, 'redirect_to' => home_url() ) );   
	else :
		echo json_encode( array( 'success' => true, 'redirect_to' => home_url() ) );
	endif;  
	die(); // this is required to return a proper result
}
add_action( 'wp_ajax_nopriv_webapper_login', 'webapper_login_function' );

if ( ! function_exists( 'webapper_user_quick_menu' ) ) :
/**
 * Custom function to output html for the User Quick Menu.
 * NOTE: if overriding this function in a child theme, also see the template /inc/ajax_posts/wp_login.php
 */
function webapper_user_quick_menu( $user = null ) {
	global $current_user;
	$notifications = web_apper_get_notifications_for_user( $current_user->ID );
	if ( empty($notifications) ) {
		$notification_list .= '<li><a href="#">No notifications</a></li>';
	} else {
		foreach ( $notifications as $notification ) :
			$notification_list .= '<li><a href="' . $notification->url . '">' . $notification->short_message . '</a></li>';
		endforeach;
	}
	
	//if ( is_god() ) {
		$menu_items = '<li><a href="' . admin_url() . '" title="WP Admin" target="_blank">WP Admin</a></li>';
	//}
	
	$menu_items .= '<li><a href="' . wp_logout_url( site_url() ) . '" title="Logout">Logout</a></li>';
	if( has_filter('webapper_user_quick_menu_items') ) {
		$menu_items = apply_filters( 'webapper_user_quick_menu_items', $menu_items );
	}
	
	return '
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span></a>
        <ul class="dropdown-menu">' . $notification_list . '</ul>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></a>
        <ul class="dropdown-menu">
            ' . $menu_items . '
        </ul>
    </li>
	';
}
endif;