<?php
/**
 * Theme: WebApper Theme
 * 
 * The "sidebar" for the widgetized footer area. If no widgets added AND just preivewing
 * the theme, then display some widgets as samples. Once the theme is actually in use,
 * it will be empty until the user adds some actual widgets.
 *
 * @package WebApper/Theme
 */


// If the sidebar has widgets, then display them
$sidebar_footer = get_dynamic_sidebar( 'Footer' );
if ( $sidebar_footer ) :
	?>
	<div class="sidebar-footer hidden-xs clearfix">
		<div class="container-fluid">
			<div class="row">
				<?php echo $sidebar_footer ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- .sidebar-footer -->
	<?php
endif;
