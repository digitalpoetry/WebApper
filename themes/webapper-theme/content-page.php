<?php
/**
 * Theme: WebApper Theme
 * 
 * The template used for displaying page content in page.php
 * everything after the_content()
 *
 * @package WebApper/Theme
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">

		<?php the_content(); ?>
		
		<?php get_template_part( 'content', 'page-nav' ); ?>

		<?php edit_post_link( __( '<span class="glyphicon glyphicon-edit"></span> Edit', 'webapper-theme' ), '<footer class="entry-meta"><div class="edit-link hidden-xs hidden-sm">', '</div></footer>' ); ?>
		
	</div><!-- .entry-content -->
	
</article><!-- #post-## -->
