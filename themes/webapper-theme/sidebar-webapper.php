<?php
/**
 * Theme: WebApper Theme
 * 
 * The Sidebar for WebApper pages.
 *
 * @package WebApper/Theme
 */


// Display the webapper sidebar
	?>
	<div id="secondary" class="widget-area col-md-3 col-lg-2 hidden-xs hidden-sm" role="complementary">
		<?php do_action( 'before_webapper_sidebar' ); ?>
		<?php dynamic_sidebar( 'WebApper Sidebar' ); ?>
	</div><!-- #secondary -->
