<?php
/**
 * Theme: WebApper Theme
 * 
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package WebApper/Theme
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">

    <?php // do_action( 'before' ); ?>
	
    <header id="masthead" class="site-header" role="banner">
	    <nav id="site-navigation" class="main-navigation" role="navigation">
	        <h1 class="menu-toggle sr-only screen-reader-text"><?php _e( 'Primary Menu', 'webapper-theme' ); ?></h1>
	        <div class="skip-link"><a class="screen-reader-text sr-only" href="#content"><?php _e( 'Skip to content', 'webapper-theme' ); ?></a></div>
	        <div class="navbar navbar-inverse navbar-fixed-top">
        		<div class="container-fluid">
            		<div class="navbar-header">
		              	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
		              	</button>
					    <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						    <?php echo get_bloginfo( 'name' ); ?>
						</a>
				    </div><!-- navbar-header -->
                    <?php if ( !is_user_logged_in() ) : ?>
                        <form id="webapper_login_form" name="webapper_login_form" class="form-inline navbar-form pull-right form-inline">
							<div class="form-group">
	                            <input name="log" class="form-control"type="text" placeholder="Username" />
	                            <input name="pwd" class="form-control" type="password" placeholder="Password" />
	                            <button id="webapper_login" class="btn">Sign in</button>
                            </div>
                        </form>
                    <?php else : ?>
                        <ul id="userQuickMenu" class="nav navbar-nav pull-right">
                            <?php echo webapper_user_quick_menu(); ?>
							<?php echo apply_filters( 'webapper_menu', $menu ); ?>
                        </ul>
                    <?php endif; ?>
				    <?php
				    // Display the desktop navbar
				    wp_nav_menu( array(
						'theme_location' => 'primary',
						'container_class' => 'navbar-collapse collapse', // <div> class
						'menu_class' => 'nav navbar-nav', // <ul> class
						'walker' => new wp_bootstrap_navwalker(),
						'fallback_cb' => 'wp_bootstrap_navwalker::fallback'
					) );
				    ?>
				</div><!-- .container-fluid -->
	        </div><!-- .navbar -->
	    </nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
