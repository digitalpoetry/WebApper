<?php
/**
 * Theme: WebApper Theme
 * 
 * The page bottom sidebar on WebApper templates. The sidebar for the bottom of
 * the page (before the widgetized footer area).
 *
 * @package WebApper/Theme
 */


// If the sidebar has widgets, then display them
$sidebar_pagebottom = get_dynamic_sidebar( 'WebApper Page Bottom' );
if ( $sidebar_pagebottom ) :
	?>
	<div id="sidebar-pagebottom-webapper" class="sidebar-page sidebar-page-bottom hidden-md hidden-lg">
		<div class="container-fluid">
			<div class="row">
				<?php echo $sidebar_pagebottom; ?>
			</div><!-- .row -->
		</div><!-- .container-fluid -->
	</div><!-- .sidebar-pagebottom -->
	<?php
endif;
