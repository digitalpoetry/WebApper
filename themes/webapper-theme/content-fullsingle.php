<?php
/**
 * Theme: WebApper Theme
 * 
 * The template used for displaying a single article (blog post) as full-width
 *
 * @package WebApper/Theme
 */


?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if ( is_single () ) : ?>
	<div class="col-xs-1 centered padding-top">
		<?php get_template_part( 'content', 'post-header' ); ?>
	</div>
	<?php endif; ?>
	
	<div class="entry-content">
		<?php the_content(); ?>
		<?php if ( is_single () ) : ?>
			<?php /* Show the categories and tags */ ?>
			<?php get_template_part( 'content', 'post-footer' ); ?>
		<?php endif; ?>
		<?php get_template_part( 'content', 'page-nav' ); ?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
