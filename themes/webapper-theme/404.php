<?php
/**
 * Theme: WebApper Theme
 * 
 * The template for displaying 404 pages (Not Found).
 *
 * @package WebApper/Theme
 */

get_header(); ?>

	<?php /* Display the header full-width to match our theme */ ?>
	<header class="content-header">
		<div class="container-fluid">
			<section class="error-404 not-found">
				<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'webapper-theme' ); ?></h1>
				<h4 class="page-description"><?php _e( 'Or as techies would say, its a "404 Error"', 'webapper-theme' ); ?></h4>
			</section><!-- .error-404 -->
		</div><!-- .container-fluid -->
	</header><!-- .content-header -->

	<?php /* Now display the main page and sidebar */ ?>
	<div class="container-fluid">
		<div id="main-grid" class="row">

			<div id="primary" class="content-area col-sm-9">
				<main id="main" class="site-main" role="main">

					<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search or one of the links below?', 'webapper-theme' ); ?></p>
					<?php get_template_part( 'content', 'siteindex' ); ?>
				
				</main><!-- #main -->
			</div><!-- #primary -->

			<?php get_sidebar(); // col-sm-3 ?>

		</div><!-- .row -->
	</div><!-- .container-fluid -->

<?php get_sidebar( 'pagebottom' ); ?>
		
<?php get_sidebar( 'footer' ); ?>

<?php get_footer(); ?>
