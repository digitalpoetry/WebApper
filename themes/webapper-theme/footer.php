<?php
/**
 * Theme: WebApper Theme
 * 
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WebApper/Theme
 * 
 * @todo Add Footer Sidebar back if unable to override in functions.php 
 */
?>
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">

		<?php
		// Footer navbar
		if ( function_exists('has_nav_menu') AND has_nav_menu( 'footer' ) ) :
			$nav_menu = wp_nav_menu( 
				array( 'theme_location' => 'footer',
				'container_div' 		=> 'div', //'nav' or 'div'
				'container_class' 		=> '', //class for <nav> or <div>
				'menu_class' 			=> 'list-inline dividers', //class for <ul>
				'walker' 				=> new wp_bootstrap_navwalker(),
				'fallback_cb'			=> '',
				'echo'					=> false, // we'll output the menu later
				'depth'					=> 1,
				) 
			);
		else :
			$nav_menu = '
			<div class="default-menu-footer-container">
				<ul id="default-menu-footer" class="list-inline dividers">
					<li class="menu-item menu-item-type-custom menu-item-object-custom"><a class="smoothscroll" title="'
					.__( 'Back to top of page', 'webapper-theme' )
					.'" href="#page"><span class="fa fa-angle-up"></span> '
					.__( 'Top', 'webapper-theme' )
					.'</a></li>
				</ul>
			</div>';
		endif; //endif has_nav_menu()

		// Site credits
		$site_credits = sprintf( __( '&copy; %1$s %2$s', 'webapper-theme' ), 
			date ( 'Y' ),
			'<a href="' . esc_url( home_url( '/' ) ) . '" rel="home">' . get_bloginfo( 'name' ) . '</a>'
		);
	 	?>

		<div class="after-footer">
			<div class="container-fluid">
		
				<div class="footer-nav-menu pull-left">
					<nav id="footer-navigation" class="secondary-navigation" role="navigation">
						<h1 class="menu-toggle sr-only"><?php _e( 'Footer Menu', 'webapper-theme' ); ?></h1>
						<?php echo $nav_menu; ?>
					</nav>
				</div><!-- .footer-nav-menu -->

				<div id="site-credits" class="site-credits pull-right">
					<?php echo $site_credits; ?>
				</div><!-- .site-credits -->

			</div><!-- .container-fluid -->
		</div><!-- .after-footer -->

	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>