<?php
/**
 * Theme: WebApper Theme
 * 
 * Template Name: WebApper Page
 *
 * Full-width page with Layout Adjusted for WebApper.
 *
 * This is a template for WebApper pages. This template has a slimmer sidebar to
 * show a wider content area and does not have a footer sidebar.
 *
 * @package WebApper/Theme
 */

get_header(); ?>

	<?php get_template_part( 'content', 'header-webapper' ); ?>

	<?php  get_sidebar( 'pagetop-webapper' ); ?>

	<div class="container-fluid">
		<div id="main-grid" class="row">

			<?php get_sidebar( 'webapper' ); // col-md-2 ?>

			<div id="primary" class="content-area col-md-9 col-lg-10">
				<main id="main" class="site-main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'page' ); ?>

						<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || '0' != get_comments_number() ) :
						?>
						<div class="comments-wrap">
							<?php comments_template(); ?>
						</div><!-- .comments-wrap" -->
						<?php endif; ?>

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->
			</div><!-- #primary -->

		</div><!-- .row -->
	</div><!-- .container-fluid -->

<?php get_sidebar( 'pagebottom-webapper' ); ?>

<?php get_footer(); ?>
