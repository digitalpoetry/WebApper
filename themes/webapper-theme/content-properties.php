<?php
/**
 * Theme: WebApper Theme
 * 
 * The template used for displaying page content in page.php
 * everything after the_content()
 *
 * @package WebApper/Theme
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">

		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>

		<?php if ( has_post_thumbnail() ) : ?>

			<div class="post-thumbnail">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php 
				// As of WP 3.9, if we don't have the right image size, use large
				if ( function_exists ( 'has_image_size') AND ! has_image_size ( 'post-thumbnail' ) ) {
					the_post_thumbnail( 'large', $attr = array( 'class'=>'thumbnail img-responsive post-thumbnail' ) ); 
				// Otherwise, use our custom size
				} else {
					the_post_thumbnail( 'post-thumbnail' , $attr = array( 'class'=>'thumbnail img-responsive' ) );
				} //endif has_image_size()
				?>
				</a>
			</div><!-- .post-thumnail -->

			<div class="property_meta">
				<ul>
					<?php
			        $property_details = array(
			            'property_type' 	 => 'Type',
			            'property_sqft' 	 => 'SQFT',
			            'property_bedrooms'  => 'Bedrooms',
			            'property_bathrooms' => 'Bathrooms',
			            'property_address' 	 => 'Address',
			        );
			
					foreach ( $property_details as $key => $label ) :
			            $value = get_post_meta( get_the_ID(), $key, true );
			            
			            if ( !empty( $value ) ) :
			            	echo '<li><strong>' . $label . ':</strong> ' . $value . '</li>';
			            endif;
			        endforeach;
					?>
					<div class="clearfix"></div>
				</ul>
			</div>
		
		<?php endif; ?>

		<div class="entry-summary">
			<?php the_excerpt(); ?>
			<hr>
		</div><!-- .entry-summary -->

	</div><!-- .entry-content -->
	
</article><!-- #post-## -->
