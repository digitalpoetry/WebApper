<?php
/**
 * Theme: WebApper Theme
 * 
 * The "sidebar" for the top of the page (after the nav bar).
 *
 * @package WebApper/Theme
 */


// If the sidebar has widgets, then display them
$sidebar_pagetop = get_dynamic_sidebar( 'Page Top' );
if ( $sidebar_pagetop ) :
	?>
	<div id="sidebar-pagetop" class="sidebar-page sidebar-page-top visible-xs visible-xs-block">
		<div class="container-fluid">
			<div class="row">
				<?php echo $sidebar_pagetop; ?>
			</div><!-- .row -->
		</div><!-- .container-fluid -->
	</div><!-- .sidebar-pagetop -->
	<?php
endif;
